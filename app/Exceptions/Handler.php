<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // If enviroment is not production?
        if ( in_array(config('app.env'), config('common.debugger.disabledInEnvs')) ) {

            // Handle different exceptions.
            switch($exception){

                case ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException):
                    return response()->view('errors.404', ['exception' => $exception], 404);
                    break;

                default:
                    return response()->view('errors.error', ['exception' => $exception], 500);
            }

        } else {
            return parent::render($request, $exception);
        }
    }
}
