<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use Hash;
use App\User;
use App\Models\Setting;
use App\Models\RoleUser;
use Activation;
use App\Models\Addbalancehistory;
use Excel;
use Mail;

class AdminController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function users(Request $request) 
    {
         $users=RoleUser::with('user','parent')->where('role_id',2)->orderBy('created_at', 'DESC')->get();
         return view('admin.users.index',compact('users'));
    }

    public function invRequest(Request $request) 
    {
        $users = User::where('is_delete','1')->orderBy('created_at', 'DESC')->get();
        return view('admin.users.investment',compact('users'));
    }

    public function investmentStatus(Request $request) {
        $user_id = $request->user_id;
        $investmentStatus = $request->investmentStatus;
        $user = User::where('id', $user_id)->first();
        User::where('id', $user_id)->update(['req_invest' => $investmentStatus]);

        return 0;
    }

        public function settingShow(Request $request)
    {
        $key =Setting::where('id','1')->first();
        return view('admin.setting.index',compact('key'));
    }
    public function settingEdit(Request $request,$id)
    {
        $key =Setting::where('id',$id)->first();
        return view('admin.setting.edit',compact('key'));
    }
    public function storeSetting(Request $request,$id)
    {
        // $value = $request->transfer;
        // if (is_null($value)) {

        //     $value = 0;

        // } elseif($value == 0) {

        //     $value = 1;

        // }
        
        $withdrawal = $request->withdrawal;
        if (is_null($withdrawal)) {

            $withdrawal = 0;

        } elseif($withdrawal == 0) {

            $withdrawal = 1;

        }

        $store = Setting::find($id);
        $store->ico_end_date = $request->edate;
        $store->ico_start_date = $request->sdate;
        $store->email = $request->email;
        $store->total_coins = $request->tcoin;
        $store->rate = $request->rate;
        // $store->maxdld = $request->maxdld;
        // $store->user_count = $request->user_count;
        $store->ref_percentage = $request->ref_percentage;
        $store->bonus = $request->bonus;
        $store->public_key = $request->pbkey;
        $store->private_key = $request->prkey;
        // $store->transfer = $value;
        $store->withdrawal = $withdrawal;
        $store->save();
        return redirect('admin-setting')->with(['success' => 'Updated successfully']);

    }

   
}
