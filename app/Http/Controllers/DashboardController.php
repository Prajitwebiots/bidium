<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Setting;
use App\Models\Rate;
use App\Models\CoinDistribution;
use App\CoinPaymentsAPI;

class DashboardController extends Controller {

	public function dashboard() {

		try {

	
			$setting = Setting::first();
			// $cp_helper = new CoinPaymentsAPI();
   //          $setup = $cp_helper->Setup($setting->private_key,$setting->public_key);
   //          $all = 1;
   //          $data =  $cp_helper->api_call('balances',array('all' => $all));
   //          $btcwal = $data['result']['BTC']['balancef'];
   //          $ethwal = $data['result']['ETH']['balancef'];

			$usdofbtc = Setting::find(1)->btc_price; // latest Bitcoin rate in USD
			$usdofeth = Setting::find(1)->eth_price; // latest Etherium rate in USD
			$setting = Setting::find(1);
			$sold = $setting->sold_coins;
			$sold = ceil($sold / 1000000); // to get row from rates table
			if ($sold > 8) {
				$sold = 8;
			}
			$rate = Rate::find($sold);
			$usd_rate = $setting->rate;
			$rates = Rate::orderBy('id')->get();
			$coins = CoinDistribution::get();

			$dt = date('Y-m-d');
			$phase = Rate::whereDate('start_date','<=',$dt)
						->whereDate('end_date','>=',$dt)->first();


            // Update rate in setting table
			 $rate = Rate::whereDate('start_date',date('Y-m-d'))->first();// get rate form date
		        if($rate)
		        {
		          $data = Setting::where('id',1)->update(['rate'=>$rate->usd_price,'bonus'=>$rate->bonus,'ref_percentage'=>$rate->ref_bonus]);
		        // return $data;
		        }

			return view('dashboard.index', compact('usdofbtc', 'usdofeth', 'rate', 'usd_rate', 'setting','rates','coins','phase'));

		} catch (Exception $e) {

			return view('errors.error');

		}

	}

}
