<?php

namespace App\Http\Controllers;

use App\Models\Deposit;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Sentinel;

class DepositController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($coin) {

		try {
			$user_id = Sentinel::getUser()->id;
			$address = Deposit::where('user_id', $user_id)->orderBy('created_at', 'DESC')->get();
			$user_address = User::where('id', $user_id)->orderBy('created_at', 'DESC')->first();
			if ($coin == 'BTC') {
				$coinType = 'BTC';
				$usd_price = Setting::find(1)->btc_price; // latest Bitcoin rate in USD
			} elseif ($coin == 'ETH') {
				$coinType = 'ETH';
				$usd_price = Setting::find(1)->eth_price; // latest Etherium rate in USD
			}
			return view('user.deposit.depositInfo', compact('address', 'user_address', 'coinType', 'usd_price'));

		} catch (Exception $e) {

			return view('errors.error');

		}

		//return view('user.deposit.depositInfo', compact('coinType'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
