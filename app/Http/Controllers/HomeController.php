<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\CoinDistribution;
use App\Models\RoleUser;

class HomeController extends Controller
{
    public function index()
    {
    	$setting = Setting::find(1);

        $user = RoleUser::where('role_id',2)->count();
        $users = $user * 100 / 100000;
        $coindistribution = CoinDistribution::where('slug','sale')->first();
          
        $presaleusd = '1000000';  // 1m 
        $soldpresaleusd =  $coindistribution->sold_coins * 0.1; 
        $presalecoins = $soldpresaleusd * 100 / $presaleusd;

        $saleusd =  '10000000';  // 100m
        $soldsaleusd =  $coindistribution->sold_coins * 0.2; 
        $salecoins = $soldsaleusd * 100 / $saleusd;
    	
       return view('home.index', compact('setting','users','presalecoins','salecoins'));
    }
}
