<?php

namespace App\Http\Controllers;

use App\Models\Ico;
use App\Models\Rate;
use App\Models\Setting;
use App\Models\Wallet;
use App\Models\CoinDistribution;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Mail;
use Sentinel;
use Session;
use Carbon;

class IcoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		try {
			$btc = Setting::find(1)->btc_price;
			$eth = Setting::find(1)->eth_price;
			$usdofbtc = $btc; // latest Bitcoin rate in USD
			$usdofeth = $eth; // latest Etherium rate in USD
			$wallets = Wallet::with('user')->orderBy('created_at', 'DESC')->where('user_id', Sentinel::check()->id)->get();
			$setting = Setting::find(1);
			$sold = $setting->sold_coins;
			$sold = ceil($sold / 1000000); // to get row from rates table
			if ($sold > 8) {
				$sold = 8;
			}
			$rate = Rate::find($sold);
			$usd_rate = $setting->rate;
			$rates = Rate::all();

			


			return view('user.ico.ico-buy', compact('btc', 'eth', 'wallets', 'usdofbtc', 'usdofeth', 'rate', 'usd_rate', 'setting', 'rates'));

		} catch (Exception $e) {

			return view('errors.error');

		}

	}

	public function investRequest(Request $request) {

		$user = user::find(Sentinel::check()->id);
		$user->req_invest = '1'; // request pending
		$user->save();

		return redirect()->back();


	}


	public function storeIco(Request $request) {
	    
	    // Genarate TXID
	    $alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; // genrate txid
		$reset_string = substr(str_shuffle(str_repeat($alpha_numeric, 25)), 0, 25); // genrate txid
        
        // Token Buy with btc wallet
		if ($request->tokenbtc) {

			$tokens = $request->tokenbtc; // token requested
			$rate = Setting::find(1)->rate; // get rate 0.1
			$btc = Setting::find(1)->btc_price; // 10799.8
			$tokenbtc = $tokens * $rate; // e.g 500 * 0.1 = 50
			$total_btc = number_format($tokenbtc / $btc, 8); // total btc 0.0046297153650994
			$myBTC = Sentinel::check()->total_btc_bal; // user btc balance

			$req = Sentinel::check()->req_invest;

			if ($myBTC >= $total_btc || $request->buyall == "1" || $tokens <= '500') {
				
		     if($req != 2) {

				if($tokens >= '10000000') {

					Session::flash('errorCoin', "Maximum Purchase 10000000 tokens");
					return redirect()->back();

				} 

			 } 
			

				// If user Click Buy All 
				if ($request->buyall == "1") {
					$rate = Setting::find(1)->rate;
					$total_usd = $btc * $myBTC; 
					$tokens = $total_usd / $rate;
					$total_btc = $myBTC;
				}

				// Insert data in wallet table
				$wallet = new Wallet;
				$wallet->user_id = Sentinel::check()->id;
				$wallet->tokens = $request->tokenbtc;
				$wallet->type = 1; // BTC
				$wallet->amount = $total_btc;
				$wallet->txid = 'BI'.$reset_string.'DM';
				$wallet->save();
                
                // Update BIDM Token balance in user table
				$user = user::find(Sentinel::check()->id);
				$user->total_btc_bal = $user->total_btc_bal - $total_btc;
				$user->total_bidm_bal = $user->total_bidm_bal + $request->tokenbtc;
				$user->token_bonus = $user->token_bonus + $request->bonus_bidmtkn;
				$user->save();
				
                
                // Update Sold Token In setting Table
                Setting::find(1)->increment('sold_coins', $request->bidmtoken);

                // Update Sold Token In Coin Distribution Table
                CoinDistribution::where('slug','whitelist')->increment('sold_coins', $request->bonus_bidmtkn);

                CoinDistribution::where('slug','sale')->increment('sold_coins', $request->tokenbtc);

                $dt = date('Y-m-d');
				$rate = Rate::whereDate('start_date','<=',$dt)
						->whereDate('end_date','>=',$dt)
						->increment('sold_coins', $request->tokenbtc);

               
                // Check If parent user exits
				if ($user->parent_id) {
					
					// Update BIDM token Bal to parent user
					$parent = User::find($user->parent_id);
					$rbal = $tokens * Setting::find(1)->ref_percentage / 100; // send token to referal 
					$parent->referral_bonus = $parent->referral_bonus + $rbal;
					$parent->save();

					// Update Sold Token In setting Table
                                       Setting::find(1)->increment('sold_coins', $rbal); 
                                       // Update Sold Token In coin distribution table
                                       CoinDistribution::where('slug','whitelist')->increment('sold_coins', $rbal);

				
				}

				// Mail function
				$type = Wallet::where('user_id', Sentinel::check()->id)->first();
				// $this->sendBuyTokenEmail($user, $tokens, $type); //send email

				Session::flash('successCoin', "$request->bidmtoken Token Buy Succssfully.");
				return redirect()->back();
			
			} else {
				
				Session::flash('errorCoin', "Please try again.");
				return redirect()->back();
			
			}
		}
        
        // Token Buy with btc wallet
		if ($request->tokeneth) {

			$tokens = $request->tokeneth; // token requested
			$rate = Setting::find(1)->rate; // get rate 0.1
			$eth = Setting::find(1)->eth_price; // 872.86
			$tokeneth = $tokens * $rate;  // e.g 500 * 0.1 = 50
			$total_eth = number_format($tokeneth / $eth, 8); // e.g 0.0572829548839447
			$myETH = Sentinel::check()->total_eth_bal; 

			$req = Sentinel::check()->req_invest;

			if ($myETH >= $total_eth || $request->buyall == 1 || $tokens <= '500') {

				 if($req != 2) {

				if($tokens >= '10000000') {

					Session::flash('errorCoin', "Maximum Purchase 10000000 tokens");

				} 

			   } 

				
				// If user Click Buy All 
				if ($request->buyall == 1) {
					$rate = Setting::find(1)->rate;
					$total_usd = $eth * $myETH; //
					$tokens = $total_usd / $rate;
					$total_eth = $myETH;
				}
                
                // Insert data in wallet table
				$wallet = new Wallet;
				$wallet->user_id = Sentinel::check()->id;
				$wallet->tokens = $request->tokeneth;
				$wallet->type = 2; // ETH
				$wallet->amount = $total_eth;
				$wallet->txid = 'BI'.$reset_string.'DM';
				$wallet->save();
                
                // Update BIDM Token balance in user table
				$user = user::find(Sentinel::check()->id);
				$user->total_eth_bal = $user->total_eth_bal - $total_eth;
				$user->total_bidm_bal = $user->total_bidm_bal + $request->tokeneth;
				$user->token_bonus = $user->token_bonus + $request->bonus_bidmtkneth;
				$user->save();


                // Update Sold Token In setting Table
                Setting::find(1)->increment('sold_coins', $request->bidmtokeneth);

                // Update Sold Token In Coin Distribution Table
                CoinDistribution::where('slug','whitelist')->increment('sold_coins', $request->bonus_bidmtkneth);

                CoinDistribution::where('slug','sale')->increment('sold_coins', $request->tokeneth);

                $dt = date('Y-m-d');
				$rate = Rate::whereDate('start_date','<=',$dt)
						->whereDate('end_date','>=',$dt)
						->increment('sold_coins', $request->tokeneth);


                 // Check If parent user exits
				if ($user->parent_id) {

                    // Update BIDM token Bal to parent user
					$parent = User::find($user->parent_id);
					$rbal = $tokens * Setting::find(1)->ref_percentage / 100; // send token to referal 
					$parent->referral_bonus = $parent->referral_bonus + $rbal;
					$parent->save();

					// Update Sold Token In setting Table
                    Setting::find(1)->increment('sold_coins', $rbal); 
                    // Update Sold Token In coin distribution table
                    CoinDistribution::where('slug','whitelist')->increment('sold_coins', $rbal);

				
				}
				
				// Mail Function
				$type = Wallet::where('user_id', Sentinel::check()->id)->first();
				//$this->sendBuyTokenEmail($user, $tokens, $type); //send email

				Session::flash('successCoin', "$request->bidmtokeneth Token Buy succssfully.");
				return redirect()->back();

			} else {
				
				Session::flash('errorCoin', "Please try again.");
				return redirect()->back();
			
			}
		}

		Session::flash('error', "Something went wrong !!!");
		return redirect()->back();

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function icoInfo() {
		
		$seting = Setting::get();
		$usdofbtc = Setting::find(1)->btc_price; // latest Bitcoin rate in USD
		$usdofeth = Setting::find(1)->eth_price; // latest Etherium rate in USD
		$setting = Setting::find(1);
		$sold = $setting->sold_coins;
		$sold = ceil($sold / 1000000); // to get row from rates table
		if ($sold > 8) {
			$sold = 8;
		}

		$rate = Rate::find($sold);
		$usd_rate = $setting->rate;
		$rates = Rate::orderBy('id')->get();

		$setting = Setting::first();
		//return $setting;
		return view('user.ico.info', compact('setting', 'usdofbtc', 'usdofeth', 'rate', 'usd_rate', 'setting', 'rates'));
	}

	 private function sendBuyTokenEmail($user,$tokens,$type){
        Mail::send('emails.buytoken',[
            'user' => $user,
            'tokens' => $tokens,
            'type' => $type,
        ],function($message) use ($user, $tokens,$type) {
            $message->to($user->email);
            $message->subject("Hello $user->username, Buy Token");
        });
    }
}
