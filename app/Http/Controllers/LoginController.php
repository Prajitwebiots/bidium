<?php

namespace App\Http\Controllers;

use Activation;
use App\User;
use Illuminate\Http\Request;
use Sentinel;
use View;

//use Socialite;

class LoginController extends Controller {

	public function login() {

		if (Sentinel::check()) {
			return redirect('admin/dashboard');
		}

		return view('auth.login');
	}

	public function doLogin(Request $request) {

		$this->validate($request, [
			'email' => 'required|max:254',
			'password' => 'required|max:254',
		]);
		try {

			$user = User::where('email', $request->email)->first(['id']);
			if ($user) {
				$act = Activation::where('user_id', $user->id)->first(['completed', 'suspend_by']);
				if ($act) {
					if ($act->completed == 1 && $act->suspend_by != 1) {
						return redirect()->back()->with(['error' => "You account is Suspended !"]);
					} else if ($act->completed == 0) {
						return redirect()->back()->with(['error' => "Your account is not activated !!!"]);
					}
				} else {
					return redirect()->back()->with(['error' => "Incorrect email. Please try again."]);
				}

			} else {
				return redirect()->back()->with(['error' => "Incorrect email. Please try again."]);
			}

			if (Sentinel::authenticate($request->all())) {
				if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
					if (Sentinel::getUser()->google2fa_enable == 1) {
						$request->session()->put('2fa:user:id', Sentinel::getUser()->id);
						$this->logout();
						return redirect('2fa/validate');
					} elseif (Sentinel::getUser()->status == 1) {
						$this->logout();
						return redirect('login')->with('success', "Thank You for Mail Confirmation Please wait for Admin Response");
					} elseif (Sentinel::getUser()->status == 2) {
						$this->logout();
						return redirect('login')->with('error', "Please Check Mail and Confirm Account");
					} elseif (Sentinel::getUser()->status == 3) {
                                             if (Sentinel::getUser()->whitelist_status == 1){
						return redirect('user/dashboard');
                                              }else{  
						$this->logout();
						return redirect('login')->with('success', "Please Check Mail and Confirm your Whitelist Account");
                                              }
					}
				} else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1') {
					return redirect('admin/dashboard');
				} else {
					return redirect()->back()->with(['error' => "Incorrect username or password. Please try again."]);
				}
			} else {
				return redirect()->back()->with(['error' => "Incorrect username or password. Please try again."]);

			}
		} catch (ThrottlingException $e) {
			$delay = $e->getDelay();
			return redirect()->back()->with(['error' => "You are Banned with $delay seconds"]);
		} catch (NotActivatedException $e) {
			return redirect()->back()->with(['error' => "You account is not activated!"]);
		} catch (\Exception $e) {
			return redirect()->back()->with(['error' => "Incorrect username or password. Please try again."]);
		}
	}

	public function logout() {
		if (Sentinel::check()) {
			if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '1') {
				Sentinel::logout();
				return redirect('/login');
			} else if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2') {
				Sentinel::logout();
				return redirect('/login');
			} else {
				Sentinel::logout();
				return redirect('/login');
			}
		} else {
			return redirect('/login');
		}

	}
}
