<?php

namespace App\Http\Controllers;

use App\Models\Activation;
use App\Models\CoinDistribution;
use App\Models\Setting;
use App\User;
use Hash;
use Illuminate\Http\Request;
use Mail;
use Sentinel;
use Validator;

class ProfileController extends Controller {

	public function index() {
		return view('auth.profile');
	}

	public function updateProfile(Request $request, $id) {

		$this->validate($request, [
			'first_name' => 'required|max:255',
			'last_name' => 'required|max:255',
		]);

		$id = $request->user_id;
		$user = User::find($id);
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		//$user->erc_20_address = $request->erc_20_address;
		$user->save();
		return redirect()->back()->with(['success' => 'Your Profile updated successfully']);
	}

	public function updateProfilePicture(Request $request, $id) {

		$this->validate($request, [
			'photo' => 'image|mimes:jpg,png,jpeg',
		]);

		$user = User::find($id);
		if (!is_null($request->photo)) {
			$file = $request->file('photo');
			$destinationPath = './assets/images/user/';
			$filename = camel_case(str_random(5) . $file->getClientOriginalName());
			$file->move($destinationPath, $filename);

			$user->profile = $filename;
		}
		$user->save();

		return redirect()->back()->with(['successProfile' => 'Your Profile updated successfully']);

	}

	public function updatePassword(Request $request, $id) {
		if (Sentinel::check()) {
			$id = Sentinel::getUser()->id;

			$validator = Validator::make($request->all(), [
				'old_password' => 'required|string|min:6',
				'new_password' => 'required|string|min:6',
				'confirm_password' => 'required|string|min:6|same:new_password',
			]);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
			}

			$old_password = $request->old_password;
			$current_password = Sentinel::getUser()->password;
			if (Hash::check($old_password, $current_password)) {
				$user = User::find($id);
				$user->password = bcrypt($request->new_password);
				$user->save();
				return redirect()->back()->with(['success' => 'Your Password updated successfully']);
			} else {
				return redirect()->back()->with(['error' => 'Invalid  Old password. Please try again', 'validator' => '1']);
			}
		} else {
			return redirect()->back()->with(['error' => 'login to change password ', 'validator' => '1']);
		}
	}

	public function changeUserStatus(Request $request) {

	    // Basics.
		$user_id = $request->user_id;
		$status = $request->status;

		// Get user.
		$user = User::where('id', $user_id)->first();
		$whitelist_status = $user->whitelist_status;

		// Let's first check if status is valid?
        if (
            !in_array( $status , array( 0, 3 ) )
            || $status == $user->status // Status shouldn't be same.
        ) {
            return "0";
        }

		// Update status.
        User::where('id', $user_id)->update(['status' => $status]);

        // If status was to block?
        if ( $status == 0 ) {

            // Suspend this user.
            Activation::where('user_id', $user_id)->update(['suspend_by' => 0]);

            // Send suspension mail.
            $this->sendSuspendEmail($user); //send email

        } elseif ( $status == 3 ) {

            // Enable activate this user.
            Activation::where('user_id', $user_id)->update(['suspend_by' => 1]);

            // Let's check if user is whitelisted or not?
            if ($whitelist_status == 0) {

                // Handle whitelisting.
                $this->handleUserStatusWhitelisting($user);

            } else {
                // Send mail when status change.
                $this->sendUnblockEmail($user); // this is email sent to register user
            }
        }

        // Return.
        return "1";

        // Old code.
//        if ($whitelist_status == 0) {
//            //return view('emails.activateWhiteList', compact('user', 'status'));
//            User::where('id', $user_id)->update(['status' => $status]);
//            // send mail when status change..
//            $this->sendWhitelistStatus($user, $status); // this is email sent to register user
//        } elseif ($whitelist_status == 1) {
//
//            User::where('id', $user_id)->update(['status' => $status]);
//            Activation::where('user_id', $user_id)->update(['suspend_by' => $status]);
//
//            $this->sendSuspendEmail($user); //send email
//            //return redirect()->back()->with(['success' => 'User Already Whitelisted.']);
//
//            return 0;
//        }
	}

	public function deleteUser(Request $request) {
		$user_id = $request->user_id;
		$deleteStatus = $request->deleteStatus;
		$user = User::where('id', $user_id)->first();
		User::where('id', $user_id)->update(['is_delete' => $deleteStatus]);
		Activation::where('user_id', $user_id)->update(['completed' => $deleteStatus]);

		return 0;
	}

	public function disable2fa(Request $request) {
		$user_id = $request->user_id;
		$status = $request->Status;
		$user = User::where('id', $user_id)->first();
		User::where('id', $user_id)->update(['google2fa_enable' => $status, 'google2fa_secret' => NULL]);

		return 0;
	}

	public function kyc() {

		return view('kyc.index');
	}

	public function updateKyc(Request $request) {

		$user = User::find($request->user_id);

		$this->validate($request, [
			'first_id_proof' => 'required|image|mimes:jpg,png,jpeg',
			'second_id_proof' => 'required|image|mimes:jpg,png,jpeg',
			'third_id_proof' => 'required|image|mimes:jpg,png,jpeg',

//            'username'  => 'required|unique:users,username,'.Sentinel::getuser()->id.'|max:255',
		]);

		if (!is_null($request->first_id_proof)) {

			$file = $request->file('first_id_proof');
			$destinationPath = './assets/images/user/kyc';
			$filename = camel_case(str_random(5) . $file->getClientOriginalName());
			$file->move($destinationPath, $filename);
			$user->kyc_nat_id_front = $filename;

		}

		if (!is_null($request->second_id_proof)) {

			$file_second = $request->file('second_id_proof');
			$destinationPath_second = './assets/images/user/kyc';
			$filename_second = camel_case(str_random(5) . $file_second->getClientOriginalName());
			$file_second->move($destinationPath_second, $filename_second);
			$user->kyc_nat_id_back = $filename_second;

		}

		if (!is_null($request->third_id_proof)) {

			$file_third = $request->file('third_id_proof');
			$destinationPath_third = './assets/images/user/kyc';
			$filename_third = camel_case(str_random(5) . $file_third->getClientOriginalName());
			$file_third->move($destinationPath_third, $filename_third);
			$user->kyc_selfie_id = $filename_third;

		}

		$user->save();
		return redirect('user/profile')->with(['success' => 'Your Kyc uploaded successfully']);
	}

	private function sendSuspendEmail($user) {
		Mail::send('emails.suspend', [
			'user' => $user,
		], function ($message) use ($user) {
			$message->to($user->email);
			$message->subject("Hello $user->username, Account Suspend");
		});
	}

	public function kyclist() {
		$users = User::get();
		return view('admin.kyc.index', compact('users'));
	}

	/**
	 * Kyc Status update
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function statusUpdate($id, $status) {

		$user = Sentinel::getUser();
		$kyc_status = $status;
		if ($kyc_status == 1) {
			$kyc_status = " Approved";
		} else {
			$kyc_status = " Rejected";
		}

		$text = 'Your Kyc status' . $kyc_status . ' .'; // email subjct
		//return view('emails.activateKyc', compact('user', 'kyc_status', 'text'));
		//  $this->sendKycEmail($user,$status,$text); // this is email sent to register user

		$user_data = User::find($id);
		$user_data->kyc_status = $status;
		$user_data->save();
		return redirect()->back()->with(['success' => 'Kyc Status updated successfully']);
	}

	private function sendKycEmail($user, $kyc_status, $text) {
		Mail::send('emails.activation', [
			'user' => $user,
			'kyc_status' => $kyc_status,
			'text' => $text,
		], function ($message) use ($user, $kyc_status) {
			$message->to($user->email);
			$message->subject("Hello $user->username, KYC Status");
		});
	}

    /**
     * This function will handle user being redirected from email.
     *
     * @return {Boolean}
     */
	public function changeUserStatusWhitelist($user_id, $status) {

	    // Get user.
		$user = User::where('id', $user_id)->first();

        // Handle whitelisting.
        $this->handleUserStatusWhitelisting($user);

        // Return.
        return redirect('login')->with(['success' => 'Now you are whitelisted please login and get free token .']);
	}

    /**
     * This function will handle whitelisting of user.
     *
     * @return {Boolean}
     */
    protected function handleUserStatusWhitelisting($user) {

        // If whitelist status is 0.
        if ($user->whitelist_status == 0) {

            // Get count of users who are whitelisted?
            $whitelistUser = User::where('whitelist_status', '=', 1)->count();

            // User's current whitelist bonus.
            $currentWhitelistBonus = $user->whitelist_bonus;

            // Decide whitelist bonus.
            $whitelist_bonus = 0;
            if ($whitelistUser >= 0 && $whitelistUser <= 20000) {
                $whitelist_bonus = 300;
            } elseif ($whitelistUser > 20000 && $whitelistUser <= 50000) {
                $whitelist_bonus = 200;
            } elseif ($whitelistUser > 50000 && $whitelistUser <= 100000) {
                $whitelist_bonus = 100;
            } elseif ($whitelistUser > 100000) {
                $whitelist_bonus = 0;
            }

            // Let's update user with bonus and whitelist_status.
            $user->whitelist_bonus = $whitelist_bonus;
            $user->whitelist_status = 1;
            $user->save();
        
            if ($user->parent_id) {
					
	     // Update BIDM token Bal to parent user
		$parent = User::find($user->parent_id);
		$rbal = $whitelist_bonus * 25 / 100; // send token to referal 
		$parent->referral_bonus = $parent->referral_bonus + $rbal;
		$parent->save();

	    // Update Sold Token In setting Table
                Setting::find(1)->increment('sold_coins', $rbal); 
            // Update Sold Token In coin distribution table
                CoinDistribution::where('slug','whitelist')->increment('sold_coins', $rbal);

	    }



            // If it was 0 previously.
            if ($currentWhitelistBonus == 0) {
                Setting::find(1)->increment('sold_coins', $whitelist_bonus);
                CoinDistribution::where('slug', 'whitelist')->increment('sold_coins', $whitelist_bonus);
            }

            // Send mail.
            $this->sendWhitelistActivate($user); // Whitelist activation mail

            // Return.
            return true;

        } else {
            return false;
        }
    }

	public function sendWhitelistStatus($user, $status) {
		Mail::send('emails.activateWhiteList', [
			'user' => $user,
			'status' => $status,
		], function ($message) use ($user, $status) {
			$message->to($user->email);
			$message->subject("Hello $user->username, Whitelist Status");
		});
	}

    /**
     * Function to send whitelist activation mail.
     *
     * @return {Boolean}
     */
	public function sendWhitelistActivate($user) {
		return Mail::send(
		    'emails.successWhitelistMsg',
            [
                'user' => $user,
            ],
            function ($message) use ($user) {
                $message->to($user->email);
                $message->subject("Hello $user->username, Now you are whitelisted.");
            }
        );
	}

    /**
     * Function to send whitelist activation mail.
     *
     * @return {Boolean}
     */
	public function sendUnblockEmail($user) {
		return Mail::send(
		    'emails.unSuspend',
            [
                'user' => $user,
            ],
            function ($message) use ($user) {
                $message->to($user->email);
                $message->subject("Hello $user->username, Your account is unblocked.");
            }
        );
	}

}
