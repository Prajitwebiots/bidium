<?php

namespace App\Http\Controllers;

use App\Models\Rate;
use Illuminate\Http\Request;

class RatesController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$rate = Rate::get();
		return view('admin.rates.index', compact('rate'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//return $request->all();
		$this->validate($request, [
			'name' => 'required',
			'usd_price' => 'numeric|required',
			'bonus' => 'numeric|required',
			'ref_bonus' => 'numeric|required',
			'days' => 'numeric|required',
			'ico_start_date' => 'required',
			'ico_end_date' => 'required',

		]);

		$ico_start_date = date("Y-m-d h:i:s", strtotime($request->ico_start_date));
		$ico_end_date = date("Y-m-d h:i:s", strtotime($request->ico_end_date));
		$rates = Rate::orderBy('id', 'desc')->first();

		// if($rates->sold <= $request->sold) {
		$rate = new Rate;
		$rate->name = $request->name;
		$rate->usd_price = $request->usd_price;
		$rate->bonus = $request->bonus;
		$rate->ref_bonus = $request->ref_bonus;
		$rate->days = $request->days;
		$rate->start_date = $ico_start_date;
		$rate->end_date = $ico_end_date;

		$rate->save();
		return redirect()->back()->with(['success' => 'Rate Added successfully']);
		// } else {
		//     return redirect()->back()->with(['error' => "Insert Rate must be greater than last rate"]);
		// }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$this->validate($request, [
			'usd_price' => 'numeric|required',
			'name' => 'required',
			'ref_bonus' => 'required',
		]);

		$ico_start_date = date("Y-m-d h:i:s", strtotime($request->ico_start_date));
		$ico_end_date = date("Y-m-d h:i:s", strtotime($request->ico_end_date));

		$rate = Rate::find($id);
		$rate->name = $request->name;
		$rate->usd_price = $request->usd_price;
		$rate->bonus = $request->bonus;
		$rate->ref_bonus = $request->ref_bonus;
		$rate->days = $request->days;
		$rate->start_date = $ico_start_date;
		$rate->end_date = $ico_end_date;
		//$rate->start_date = $request->date;

		$rate->save();
		return redirect()->back()->with(['success' => 'Rate updated successfully']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
