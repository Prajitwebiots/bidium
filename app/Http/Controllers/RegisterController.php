<?php

namespace App\Http\Controllers;

use Activation;
use App\Models\Referral;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Mail;
use Sentinel;
use Validator;

class RegisterController extends Controller {

	public function referral(Request $request, $ref) {
		$request->session()->flash('referral', $ref);
		$request->session()->flash('ref', '1');
		return redirect('register');
	}

	public function showRegister(Request $request) {

	    // Redirect to login if registration is closed.
        if (config('common.timerConfigurations.current.groupId') != 1) {
            return redirect('/login')->with(['error' => "Registrations are closed !!!"]);
        }

		$setting = Setting::find(1);
    	return view('auth.register', compact('setting'));
	}

	public function doRegister(Request $request) {
		// generate bidm token address
		$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; // genrate address
		$bidmaddress = substr(str_shuffle(str_repeat($alpha_numeric, 35)), 0, 35); // genrate address

		$sponser = "";

		try {
			$validator = Validator::make($request->all(), [
				'username' => 'required|unique:users,username|max:255',
				'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
				'sponser' => 'nullable|max:255',
				'email' => 'required|email|unique:users|max:254',
				'password' => 'required',
				'erc_20_address' => 'required|unique:users,erc_20_address',
				'confirm_password' => 'required|same:password',
				'dob' => 'required',
			]);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput()->with(['validator' => '1']);
			}

			// check user exit or not
			if ($request->sponser) {
				$sponser = User::where('username', $request->sponser)->first();
				if (is_null($sponser)) {
					return redirect()->back()->with(['error' => "Please enter Valid Spenser Name !", 'validator' => '1']);
				}

			}

			$profile = 'avatar.png';

			$user = Sentinel::register(array(
				'profile' => $profile,
				'username' => $request->username,
				'first_name' => $request->first_name,
				'last_name' => $request->last_name,
				'email' => $request->email,
				'password' => $request->password,
				'bidm_address' => 'BI' . $bidmaddress . 'DM',
				'dob' => $request->dob,
				'erc_20_address' => $request->erc_20_address,
			));

			$activation = Activation::create($user); // will create activation record of registered user
			$role = Sentinel::findRoleById(2); // role 2 for user
			$role->users()->attach($user); // assign role to registered user

			if ($sponser != "" && $request->sponser == $sponser->username) {
				$parent_id = $sponser->id;
				User::where('username', $request->username)->update(array('parent_id' => $parent_id));
			}

			$link = url('') . '/activate/' . $request->email . '/' . $activation->code; // account activation link
			$text = 'Your Account has been created.'; // email subjct
			//  return view('emails.activation',compact('user','text','link'));
			$this->sendActivationEmail($user, $text, $link); // this is email sent to register user
			return redirect('login')->with(['success' => "Thank you. To complete your registration please check your email."]);
		} catch (ThrottlingException $e) {
			$delay = $e->getDelay();
			return redirect()->back()->with(['error' => "You are Banned with $delay seconds", 'validator' => '1']);
		} catch (NotActivatedException $e) {
			return redirect()->back()->with(['error' => "You account is not activated!", 'validator' => '1']);
		}
	}

	public function activate($email, $activationCode) {

	    // Fetch user.
		$user = User::whereEmail($email)->first();

		// If we have user?
		if ($user) {

			$sentinelUser = Sentinel::findById($user->id);
			if (Activation::complete($sentinelUser, $activationCode)) {
				User::where('id', $user->id)->update(array('status' => 1));
				return redirect('/login')->with(['success' => "Email is verified, please wait for Whitelisting !!!"]);
			} else {
				return redirect('/login')->with(['error' => " This link is expired. please try to login !!!"]);
			}
		}
	}

	private function sendActivationEmail($user, $text, $link) {
		Mail::send('emails.activation', [
			'user' => $user,
			'text' => $text,
			'link' => $link,
		], function ($message) use ($user, $text) {
			$message->to($user->email);
			$message->subject("Hello $user->username, $text");
		});
	}

	/**
	 * Checks if the given string is an address
	 *
	 * @method isAddress
	 * @param {String} $address the given HEX adress
	 * @return {Boolean}
	 */
	public function isAddress($address) {
		if (!preg_match('/^(0x)?[0-9a-f]{40}$/i', $address)) {
			// check if it has the basic requirements of an address
			return 0;
		} elseif (!preg_match('/^(0x)?[0-9a-f]{40}$/', $address) || preg_match('/^(0x)?[0-9a-fA-F]{40}$/', $address)) {
			// If it's all small caps or all all caps, return true
			return 1;
		}
	}

	public function checklstUser($address) {

		$user = User::where('erc_20_address', $address)->where('whitelist_status', 1)->first();
		if ($user) {
			return 1;
		} else {
			return 0;
		}

	}

}
