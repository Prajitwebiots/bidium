<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 1)
            return $next($request);
        else if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == '2')
            return redirect('user/dashboard');
        else
            return redirect('/login');
    }
}
