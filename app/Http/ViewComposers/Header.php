<?php

namespace App\HTTP\ViewComposers;

use Illuminate\View\View;
use Auth;
use Sentinel;
use App\Models\Setting;
use App\Models\Withdraw;
use App\User;


class Header
{
	public function compose(View $view)
		{

         // $btc = json_decode(file_get_contents('https://www.bitstamp.net/api/v2/ticker/btcusd'));
         // $eth = json_decode(file_get_contents('https://www.bitstamp.net/api/v2/ticker/ethusd'));
         //$data = array('BTC' => $btc->last, 'ETH' => $eth->last);
         $data['BTC'] = Setting::find(1)->btc_price;
	     $data['ETH'] = Setting::find(1)->eth_price;
	     $data['rate'] = Setting::find(1)->rate; 
	     $data['sold'] = Setting::find(1)->sold; 
	     $data['transfer'] = Setting::find(1)->transfer; 
	     
             $withdraw=Withdraw::where('admin_status',0)->count();
	     $inv=User::where('req_invest',1)->count();
            
         $data['count'] =  $withdraw;
         $data['inv'] =  $inv;
         
        //    $btc = json_decode(file_get_contents('https://www.bitstamp.net/api/v2/ticker/btcusd'));
        //    $eth = json_decode(file_get_contents('https://www.bitstamp.net/api/v2/ticker/ethusd'));
        //    $btc_24 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/top/pairs?fsym=BTC'));
        //    $eth_24 = json_decode(file_get_contents('https://min-api.cryptocompare.com/data/top/pairs?fsym=ETH'));

        //  $setting = ['btc_volume'=>$btc->volume,'eth_volume'=>$eth->volume,'btc_24'=>$btc_24->Data[1]->volume24h,'etc_24'=>$eth_24->Data[1]->volume24h];

        // $data['setting']=$setting;
	     
         $view->with('header_data',$data);

		}

}
