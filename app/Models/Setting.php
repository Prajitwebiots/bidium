<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Update created_at and updated_at columns.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['ico_start_date', 'ico_end_date'];
}
