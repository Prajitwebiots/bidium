<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;
use App\Models\Rate;
use App\Models\Setting;

class AppServiceProvider extends ServiceProvider
{
    protected $timeZone = 'Asia/Kolkata';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // Update timer related configurations.
        $this->set_timer_configurations();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register timer configurations.
     *
     * @return void
     */
    protected function set_timer_configurations()
    {
        // Basics.
        $allTimings = [];
        $currentTimings = [];
        $now = Carbon::now();
        $lowestTime = null;

        // If we have data?
        if ( !config('common.disableTimerBasedContent') ) {

            // Let's fetch all rates from DB.
            $rates = Rate::whereIn('id', [2, 5, 9, 10])
                ->get();

            // If we have data?
            if ( $rates->isNotEmpty() ) {

                // Iterate.
                foreach ($rates as $rate) {

                    // Format dates.
                    $startDate = $this->format_date($rate->start_date);
                    $endDate = $this->format_date($rate->end_date);

                    // Prepare $row.
                    $rowArray = [
                        'id' => $rate->id,
                        'name' => $rate->name,
                        'bonus' => $rate->bonus,
                        'refBonus' => $rate->ref_bonus,
                        'startDate' => $rate->start_date,
                        'endDate' => $rate->end_date,
                        'timerDate' => $rate->end_date,
                    ];

                    //Let's check whether today falls between this time.
                    if (
                        count($currentTimings) == 0
                        && ( $now->gte($startDate) )
                        && ( $now->lt($endDate) )
                    ) {
                        // Let's add it to $currentTimings array.
                        $currentTimings = $rowArray;
                    }

                    // Add row to $allTimings.
                    $allTimings[] = $rowArray;

                    // Let's update lowest time.
                    $lowestTime = $lowestTime
                        ? ( $lowestTime->lt($startDate)
                            ? $lowestTime
                            : $startDate
                        )
                        : $startDate;
                }
            }

            // Get initial timings.
            $initialTiming = Setting::find(1);

            // Format initial dates.
            $initialTimingStartDate = $this->format_date($initialTiming->ico_start_date);
            $initialTimingEndDate = $this->format_date($initialTiming->ico_end_date);

            // Let's prepare array.
            $initialTimingArray = [
                'id' => 1,
                'startDate' => $now,
                'endDate' => $initialTiming->ico_start_date,
                'timerDate' => $initialTiming->ico_start_date,
            ];

            // Add this to $allTimings.
            $allTimings = array_merge(
                [$initialTimingArray],
                $allTimings
            );

            // Let's check if it can be $currentTimings.
            if (
                count($currentTimings) == 0 // No other profile should be active.
                && (
                    !$lowestTime            // Lowest time if any should be greate than now.
                    || ($lowestTime->gt($now))
                )
                && $now->lt($initialTimingStartDate) // Now should be less than ico_end_date
            ) {
                $currentTimings = $initialTimingArray;
            }
//echo '<pre>';print_r($currentTimings);die;
            // After doing all this, if we have $currentTimings?
            if ( count($currentTimings) > 0 ) {

                // Ierate $allTimings.
                $tempArray = [];
                foreach ($allTimings as $allTiming) {
                    $tempArray[$allTiming['id']] = $allTiming;
                }

                // Prepare config array.
                $configArray = [
                    'common.timerConfigurations.all' => $tempArray,
                    'common.timerConfigurations.current' => [
                        'groupId' => $currentTimings['id'],
                        'data' => $currentTimings
                    ],
                ];

                // Overwrite config values.
                config($configArray);
            }
        }

        // Return.
        return true;
    }

    /**
     * Register timer configurations.
     *
     * @return void
     */
    protected function format_date($date)
    {
        $timestamp = $date;
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, $this->timeZone);
        return $date->setTimezone('UTC');
    }
}
