<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class HeaderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.master','App\Http\ViewComposers\Header');
        View::composer('home.index','App\Http\ViewComposers\Header');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
