<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Website Configurations
    |--------------------------------------------------------------------------
    |
    |
    */

    // Debugger.
    'debugger' => [
        'disabledInEnvs' => array('production'),
    ],

    // Ad codes.
    'adCodes' => [
        'analytics' => [
            'google' => env('ENABLE_GOOGLE_ANALYTICS', true)
        ],
    ],

    // Social.
    'social' => [
        'facebook' => [
            'url' => 'https://www.facebook.com/bidium',
            'mailIcon' => 'facebook-share.png',
        ],
        'twitter' => [
            'url' => 'https://twitter.com/bidiumofficial',
            'mailIcon' => 'twitter-share.png',
        ],
        'telegram' => [
            'url' => 'https://t.me/Bidiumofficialgroup',
            'mailIcon' => 'telegram-share.png',
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Emails List
    |--------------------------------------------------------------------------
    |
    */
    'emails' => [
        'list' => [
            'support' => [
                'email' => 'support@bidium.io',
            ],
        ],
    ],


    /*
    |--------------------------------------------------------------------------
    | Timer related configurations.
    |--------------------------------------------------------------------------
    |
    */
    'disableTimerBasedContent' => env('DISABLE_TIMER_BASED_CONTENT', false),
    'timerConfigurations' => [
        'all' => [],
        'current' => [
            'groupId' => 0,
            'data' => [],
        ],
        'hardCapReached' => false,
    ],
];
