<?php

return [

    /*
    |--------------------------------------------------------------------------
    | App Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'webCopyright' => 'Copyright © 2018. All rights reserved.',
    'mailCopyright' => 'Copyright © ' . config('app.name') . ', 2018. All rights reserved.',
];
