<?php

return [

    /**
     *
     * Mail content.
     *
     */
    'unableToClickText' => 'If you\'re having trouble clicking the button, copy and paste the URL below into your web browser: :actionUrl',
];
