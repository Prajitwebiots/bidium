@extends('layouts.master')

@section('title') Bidium Coin | KYC List @endsection

@section('style')
<style type="text/css">
    button.btn.btn-warning {
        color: #fff;
    }

    .badge-warning {
        color: #fff !important;
    }

    .btndashboard {
        width: 120px;
    }

    @media only screen and (max-width: 768px) {
        .width50 {
            width: 50%;
        }
    }
</style>
@endsection
@section('content')
        <div class="dashboard-body">
        <div class="row">
               <div class="col-sm-12">
         <h4 class="page-title">KYC List</h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}"><i class="fa fa-home" aria-hidden="true"></i></a>
         </li>
         <li class="breadcrumb-item"><a href="{{url('admin-setting')}}"> KYC List</a>
      </li>
   </ol>
</div>
            <div class="col-sm-12">
                @if(session('error'))<br>
                <div class="alert alert-danger">{{ session('error') }}</div><br>@endif
                @if(session('success'))<br>
                <div class="alert alert-success">{{ session('success') }}</div><br>@endif
                <div class="card">
                    <div class="card-body table-responsive">
                        <div class="col-md-12 ">
                            <h4>User KYC List</h4>
                        </div>
                        <div class="mb-4"></div>
                        <!-- <table class="table table-striped data-table"> -->
                        <table id="data-table" class="data-table table table-striped data-table" cellspacing="0" width="100%">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Sr.</th>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th scope="col">kyc national id front </th>
                                <th scope="col">kyc national id back </th>
                                <th scope="col">kyc selfie </th>
                                <th scope="col">Kyc Status</th>
                                <th scope="col">View Kyc</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1;
?>
                            @foreach($users as $user)
                            @if($user->kyc_nat_id_front != '' && $user->kyc_nat_id_back != '' && $user->kyc_selfie_id != '')
                            <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if($user->kyc_nat_id_front != null)
                                            <img src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_nat_id_front }}" class="kyc-doc">
                                        @else
                                            <img style="height:50px"  src="{{url('assets/images/user/kyc/no-image-available.png')}}">
                                        @endif

                                    </td>
                                    <td>
                                        @if($user->kyc_nat_id_back != null)
                                        <img src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_nat_id_back }}" class="kyc-doc">
                                         @else
                                            <img style="height:50px"  src="{{url('assets/images/user/kyc/no-image-available.png')}}">
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->kyc_selfie_id != null)
                                        <img src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_selfie_id }}" class="kyc-doc">
                                         @else
                                            <img style="height:50px"  src="{{url('assets/images/user/kyc/no-image-available.png')}}">
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->kyc_status == 0)
                                            <span class="badge badge-warning mt-2">Pending</span>
                                        @elseif($user->kyc_status == 1)
                                            <span class="badge badge-success mt-2">Accepted</span>
                                        @else
                                            <span class="badge badge-danger mt-2">Rejected</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->kyc_nat_id_front != null && $user->kyc_nat_id_back != null && $user->kyc_selfie_id != null)
                                             <button type="button" class="btn btn-theme" data-toggle="modal" data-target="#info-model{{ $user->id }}">View Kyc</button>
                                        @else
                                            -
                                        @endif

                                    </td>
                                </tr>
                            @endif
								<div class="modal fade bd-example-modal-lg" id="model-kyc-document" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">

                                    <div class="modal-dialog modal-lg">

                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>
                                        <div class="modal-body">

                                            <div class="row">
                                                <div class="col-md-12">

                                                 <div id="largekyc">

                                                 </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="info-model{{$user->id}}" role="dialog">
                                    <div class="modal-dialog">

                                      <!-- Modal content-->
                                      <div class="modal-content">
                                        <div class="modal-header">
                                        	<h5 class="modal-title">Kyc Details</h5>
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>

                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                	<div class="form-group">
                                                		<div class="row">
	                                                        <div class="col-md-5">
	                                                            Username
	                                                        </div>
	                                                        <div class="col-md-4">
	                                                            {{ $user->username }}
	                                                        </div>
                                                    	</div>
                                                	</div>
													<div class="form-group">
														<div class="row">
	                                                        <div class="col-md-5">
	                                                            Email
	                                                        </div>
	                                                        <div class="col-md-4">
	                                                            {{ $user->email }}
	                                                        </div>
                                                    	</div>
													</div>
													<div class="form-group">
	                                                    <div class="row">
	                                                        <div class="col-md-5">
	                                                            kyc national id Front
	                                                        </div>
	                                                        <div class="col-md-4">
	                                                            <img style="height: 100px;" src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_nat_id_front }}">
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="form-group">
	                                                    <div class="row">
	                                                        <div class="col-md-5">
	                                                            kyc national id Back
	                                                        </div>
	                                                        <div class="col-md-4">
	                                                            <img style="height: 100px;" src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_nat_id_back }}"><br><br>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="form-group">
	                                                    <div class="row">
	                                                        <div class="col-md-5">
	                                                            kyc selfie id
	                                                        </div>
	                                                        <div class="col-md-4">
	                                                            <img style="height: 100px;" src="{{url('assets/images/user/kyc')}}/{{ $user->kyc_selfie_id }}"><br><br>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                                <div class="form-group">
	                                                    <div class="row">
	                                                        <div class="col-md-5">
	                                                            Kyc Status
	                                                        </div>
	                                                        <div class="col-md-6">
	                                                         @if($user->kyc_status == 0)
	                                                            <a href="{{ url('admin-kycUpdate',$user->id) }}/1" class="btn btn-info ">
	                                                            	Accept
	                                                            </a>
		                                                        <a href="{{ url('admin-kycUpdate',$user->id) }}/2" class="btn btn-info ">
		                                                            Reject
		                                                        </a>
	                                                        @else
	                                                            Done
	                                                        @endif
	                                                        </div>
	                                                    </div>
	                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                </div>

                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@section('script')

    <script src="{{ url('assets/js/clipboard.min.js') }}"></script>
    <script>
        $(document).ready(function () {

            $('.data-table').DataTable();
        });
    </script>

     <script>
        $(".kyc-doc").click(function () {
            //alert();
            var kycimage = $(this).attr("src");
            $("#largekyc").html("<img src='"+kycimage+"' class='img-fluid'>");
            $("#model-kyc-document").modal("show");
            //$('.data-table').DataTable();
        });
    </script>

@endsection