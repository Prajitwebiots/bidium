@extends('layouts.master')
@section('title') Bidium Coin  | Phase Setting @endsection
@section('style')

<style type="text/css">
button.btn.btn-danger.btn-sm , button.btn.btn-success.btn-sm {
    width: 25%;
}
.badge-warning {
color: #fff !important;
}
a.btn.btn-success.btn-sm {
    color: #fff;
}
</style>


@endsection
@section('content')
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="dashboard-body">
   <div class="row">
      <div class="col-sm-12">
         <h4 class="page-title">Phase Setting</h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i></a>
         </li>
         <li class="breadcrumb-item"><a href="{{url('admin-rates')}}">Phase Setting</a>
      </li>
   </ol>
</div>
</div>
   @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
   @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
    @if (count($errors))
        <div class="alert alert-danger alert-dismissible fade show   m-alert m-alert--air" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
            <ul>
                @foreach($errors->all() as $error)

                <li>{{ $error }}</li><br>
                @endforeach
            </ul>
        </div>
         @endif

<div class="mb-3"></div>
<div class="row">
<div class="col-sm-12">
<a data-toggle="modal" data-target="#addrate" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add New</a>
</div>
</div>
<div class="mb-4"></div>
<div class="row">
<div class="col-sm-12">

   <div class="card">
      <div class="card-body table-responsive">
         <!-- <table class="table table-striped data-table"> -->
         <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr>
                  <th scope="col">Sr.</th>
                  <th scope="col">Name</th>
                  <th scope="col">Bonus</th>
                  <th scope="col">Referral Bonus</th>
                  <th scope="col">Days</th>
                  <th scope="col">Usd price</th>
                  <th scope="col">Start Date</th>
                  <th scope="col">End Date</th>
                   <th scope="col">Sold Coins</th>
                  <th scope="col">Action </th>
               </tr>
            </thead>
            <tbody>

            @foreach($rate as $key)

                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{ $key->name }}</td>
                    <td>{{ $key->bonus }}%</td>
                    <td>{{ $key->ref_bonus }}%</td>
                    <td>{{ $key->days }} Days</td>
                    <td>{{ number_format($key->usd_price,2) }} $</td>
                    <td>{{ $key->start_date }}</td>
                    <td>{{ $key->end_date }}</td>
                    <td>{{ $key->sold_coins }}</td>
                    <td><a data-toggle="modal" data-target="#raet{{$loop->iteration}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a></td>
                </tr>

            @endforeach


       </tbody>
         </table>
          @foreach($rate as $key)
          <div class="modal fade" id="raet{{$loop->iteration}}" role="dialog">
              <div class="modal-dialog">
                  <!-- Modal content-->
                  {!! Form::model($rate, ['method' => 'PATCH','url' => ['admin-rates', $key->id],'files' => true, 'class'=> 'm-form m-form--fit m-form--label-align-right']) !!}
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                  <div class="modal-content">
                          <div class="modal-body">
                              <div class="mb-3"></div>
                              <h3>Edit Phase </h3>
                              <div class="mb-5"></div>
                              <div class="form-group col-md-12">
                                        <div class="form-group col-md-12"></div>
                                          <label for="exampleInputEmail1">Name</label>
                                      <input type="input" class="form-control" name="name" value="{{$key->name}}" >
                                      <div class="form-group col-md-12"></div>

                                  <label for="exampleInputEmail1">Use price </label>
                                      <input type="input" class="form-control" name="usd_price" value="{{$key->usd_price}}" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Bonus</label>
                                      <input type="input" class="form-control" name="bonus" value="{{$key->bonus}}" >
                                      <div class="form-group col-md-12"></div>

                                  <label for="exampleInputEmail1">Referral Bonus</label>
                                      <input type="input" class="form-control" name="ref_bonus" value="{{$key->ref_bonus}}" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Days</label>
                                      <input type="input" class="form-control" name="days" value="{{$key->days}}" >
                                      <div class="form-group col-md-12"></div>
                                  <!-- <label for="exampleInputEmail1">Start Date</label>
                                      <input type="input" class="form-control startDate" name="date" value="{{$key->start_date}}" placeholder="yyyy-mm-dd hh:mm:ss" >
                                      <div class="form-group col-md-12"></div> -->


                                    <label for="exampleInputEmail1">Start Date</label>
                                      <div class="input-group date" id="ico_start_date_edit{{$key->id}}" data-target-input="nearest">
                                      <input type="text" value="{{$key->start_date}}" name="ico_start_date" class="form-control datetimepicker-input" data-target="#ico_start_date_edit{{$key->id}}"/>
                                      <div class="input-group-append" data-target="#ico_start_date_edit{{$key->id}}" data-toggle="datetimepicker">
                                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                      </div>
                                  </div>



                                      <br>

                                    <label for="exampleInputEmail1">End Date</label>
                                      <div class="input-group date" id="ico_end_date_edit{{$key->id}}" data-target-input="nearest">
                                      <input type="text" value="{{$key->end_date}}" name="ico_end_date" class="form-control datetimepicker-input" data-target="#ico_end_date_edit{{$key->id}}"/>
                                      <div class="input-group-append" data-target="#ico_end_date_edit{{$key->id}}" data-toggle="datetimepicker">
                                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-theme mt-4">Update</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
         @endforeach


          <div class="modal fade" id="addrate" role="dialog">
              <div class="modal-dialog">
                  <!-- Modal content-->
                 <form class="m-form m-form--fit m-form--label-align-right" method="post" action="">
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                  <div class="modal-content">
                          <div class="modal-body">
                              <div class="mb-3"></div>
                              <h3>Add New Settings </h3>
                              <div class="mb-5"></div>
                              <div class="form-group col-md-12">
                                  <label for="exampleInputEmail1">Name</label>
                                      <input type="input" class="form-control" name="name" value="" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Use price </label>
                                      <input type="input" class="form-control" name="usd_price" value="" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Bonus</label>
                                      <input type="input" class="form-control" name="bonus" value="" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Referral Bonus</label>
                                      <input type="input" class="form-control" name="ref_bonus" value="" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Days</label>
                                      <input type="input" class="form-control" name="days" value="" >
                                      <div class="form-group col-md-12"></div>
                                  <label for="exampleInputEmail1">Date</label>

                                  <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                                      <input type="text" name="ico_start_date" class="form-control datetimepicker-input" data-target="#datetimepicker"/>
                                      <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                      </div>
                                  </div>
                                  <br>

                                  <div class="input-group date" id="ico_end_date" data-target-input="nearest">
                                      <input type="text" name="ico_end_date" class="form-control datetimepicker-input" data-target="#ico_end_date"/>
                                      <div class="input-group-append" data-target="#ico_end_date" data-toggle="datetimepicker">
                                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                      </div>
                                  </div>


                              </div>
                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-theme mt-4">Submit</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>

      </div>
   </div>
</div>
</div>
</div>
</div>


@endsection
@section('script')

<script>

$(document).ready(function() {
  $('#data-table').DataTable();
    $('#datetimepicker').datetimepicker();
    $('#ico_end_date').datetimepicker();
});




</script>

@endsection