@extends('layouts.master')
@section('title') Bidium Coin  | Manage Users @endsection
@section('style')
<style type="text/css">
.badge-warning {
    background-color: #ffc107;
    color: #fff;
}
</style>
@endsection
@section('content')
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="dashboard-body">
   <div class="row">
      <div class="col-sm-12">
         <h4 class="page-title">Manage User</h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i></a>
         </li>
         <li class="breadcrumb-item"><a href="{{ url('admin/users')}}">Users</a>
      </li>
   </ol>
</div>
</div>
<div class="row">
<div class="col-sm-12">
   @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
   @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
   <div class="card">
      <div class="card-body table-responsive">
           <!-- <table class="table table-striped data-table"> -->
         <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr>
                  <th scope="col">Sr.</th>
                  <th scope="col">Username</th>
                  <!-- <th scope="col">Ref Username</th> -->
                  <th scope="col">First Name</th>
                  <th scope="col">Last Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">DOB</th>
                  <th scope="col">ETH Address</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                  <th scope="col">Disable 2FA</th>
               <!--    <th scope="col">Transaction</th> -->
               </tr>
            </thead>
            <tbody>
               <?php $i = 1;?>
               @foreach($users as $user)
               @if($user->user)
               @if($user->user->is_delete == 1)
               <tr>
                  <th>{{ $i++ }}</th>
                  <td>{{ $user->user->username }}</td>
                  <!-- <td>{{ @$user->parent->username }}</td> -->
                  <td>{{ $user->user->first_name }}</td>
                  <td>{{ $user->user->last_name }}</td>
                  <td>{{ $user->user->email }}</td>
                  <td>{{ $user->user->dob }}</td>
                  <td>{{ $user->user->erc_20_address }}</td>
                  <td>
                    @if($user->user->whitelist_status == 0 || $user->user->status == 0)
                       @if($user->user->status == 0 )
                       <span class="badge badge-danger">Suspended</span>
                       @elseif($user->user->status == 1)
                       <span class="badge badge-success">Email verified</span>
                       @elseif($user->user->status == 2)
                          <span class="badge badge-warning">Pending </span>
                       @elseif($user->user->status == 3)
                          <span class="badge badge-warning">whitelist request </span>
                       @endif
                    @else
                        <span class="badge badge-success">Whitelisted</span>
                    @endif
                  </td>
                  <td>
                      @if(in_array($user->user->status, array(0, 1)))
                          <a href="#"  onclick="userStatus({{$user->user->id}},'3')" class="btn btn-success btn-sm" alt="{{ $user->user->status == 1 ? 'Activate' : 'Un-Block' }}"><i class="fa fa-check"></i></a>
                      @endif

                      @if($user->user->status == 1 || ( $user->user->status == 3 && $user->user->whitelist_status == 1 ))
                         <button onclick="userStatus({{$user->user->id}},'0')"  class="btn btn-danger btn-sm" alt="Block"><i class="fa fa-ban"></i></button>
                      @endif

                     <button class="btn btn-danger btn-sm" onclick="deleteUser({{$user->user->id}},'0')" alt="Delete"><i class="fa fa-trash"></i></button>
                  </td>
                  <td>
                    @if($user->user->google2fa_enable == 1)
                     <button class="btn btn-info btn-sm" onclick="disable2fa({{$user->user->id}},'0')">&nbsp;Disable</button>
                    @else
                     -
                    @endif
                  </td>

               </tr>
               @endif
               @endif
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
    $('#data-table').DataTable();
});

// User Active Or Block
function userStatus(arg1, arg2) {
    var url = '{{ url("changeUserStatus") }}';
    var _token = $("#_token").val();
    if (arg2 == '0') { // user status is 0
        swal({
                title: "Are you sure To Block?",
                text: "You Really Sure Of Block !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Blocked it!",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {
                        '_token': _token,
                        'user_id': arg1,
                        'status': arg2
                    },
                    success: function(result) {
                        if (result == 1) {
                            swal("Blocked!", "Your User is Block.", "success");
                            window.location.reload();
                        } else {
                            $('#inner_msg').html('<strong>User status can not be verified.</strong>');
                        }
                    }
                });
            });
    } else {
        swal({
                title: "Are you sure To Active?",
                text: "You Really Sure Of Active !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Active it!",
                closeOnConfirm: false
            },
            function() {
                $.ajax({
                    url: url,
                    method: 'post',
                    data: {
                        '_token': _token,
                        'user_id': arg1,
                        'status': arg2
                    },
                    success: function(result) {
                        if (result == 1) {
                            swal("Activated!", "Your User is Activated.", "success");
                            window.location.reload();
                        } else {
                            $('#inner_msg').html('<strong>User Status Can not verify.</strong>');
                        }
                    }
                });
            });
    }
}

// Delete User
function deleteUser(arg1,arg2)
    {
         var url = '{{ url("deleteUser") }}';
         var _token=$("#_token").val();

              swal({
              title: "Are you sure To Delete?",
              text: "You Really Sure Of Delete !",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Delete it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                url: url,
                method:'post',
                data: { '_token' : _token,  'user_id':arg1, 'deleteStatus':arg2 },
                success:function(result)
                {
                   if(result==0)
                    {
                         swal("Deleted!", "Your User is Deleted.", "success");
                         window.location.reload();
                    }
                    else
                    {    $('#inner_msg').html('<strong>User Status Can not verify.</strong>');    }
                }
              });
            });
    }


    // disable2fa
function disable2fa(arg1,arg2)
    {
         var url = '{{ url("disable2fa") }}';
         var _token=$("#_token").val();

              swal({
              title: "Are you sure To Disable 2fa?",
              text: "You Really Sure Of Disable 2fa !",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Disable it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                url: url,
                method:'post',
                data: { '_token' : _token,  'user_id':arg1, 'Status':arg2 },
                success:function(result)
                {
                   if(result==0)
                    {
                         swal("Disabled!", "Your User's 2fa is Disabled.", "success");
                         window.location.reload();
                    }
                    else
                    {    $('#inner_msg').html('<strong>User Status Can not verify.</strong>');    }
                }
              });
            });
    }
</script>
@endsection