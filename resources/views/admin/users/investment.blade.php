@extends('layouts.master')
@section('title') Bidium Coin  | Users Investment Request @endsection
@section('style')
<style type="text/css">
.badge-warning {
    background-color: #ffc107;
    color: #fff;
}
</style>
@endsection
@section('content')
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="dashboard-body">
   <div class="row">
      <div class="col-sm-12">
         <h4 class="page-title">User Investment Request</h4>
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i></a>
         </li>
         <li class="breadcrumb-item"><a href="#">Investment Request</a>
      </li>
   </ol>
</div>
</div>
<div class="row">
<div class="col-sm-12">
   @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
   @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
   <div class="card">
      <div class="card-body table-responsive">
           <!-- <table class="table table-striped data-table"> -->
         <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr>
                  <th scope="col">Sr.</th>
                  <th scope="col">Username</th>
                  <th scope="col">First Name</th>
                  <th scope="col">Last Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">DOB</th>

                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
           
         
               </tr>
            </thead>
            <tbody>
               <?php $i = 1;?>
               @foreach($users as $user)
               @if($user)
               @if($user->is_delete == 1)
               @if($user->req_invest == 1 || $user->req_invest == 2)
               <tr>
                  <th>{{ $i++ }}</th>
                  <td>{{ $user->username }}</td>
                  <td>{{ $user->first_name }}</td>
                  <td>{{ $user->last_name }}</td>
                  <td>{{ $user->email }}</td>
                  <td>{{ $user->dob }}</td>

                  <td>
                    @if($user->req_invest == 1)
                          <span class="badge badge-warning">Pending </span>
                       @elseif($user->req_invest == 2)
                          <span class="badge badge-success">Approved </span>
                       @endif
                   
                  </td>
                  <td>
                     @if($user->req_invest == 1)
                     <button onclick="investmentStatus({{$user->id}},'2')"  class="btn btn-success btn-sm">Approve</button>
                     @else
                     -
                     @endif
                      
                  </td>
                  

               </tr>
               @endif
               @endif
               @endif
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
$(document).ready(function() {
    $('#data-table').DataTable();
});


// Delete User
function investmentStatus(arg1,arg2)
    {
         var url = '{{ url("investmentStatus") }}';
         var _token=$("#_token").val();

              swal({
              title: "Are you sure To Approve?",
              text: "You Really Sure Of Approve !",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Approve it!",
              closeOnConfirm: false
            },
            function(){
                $.ajax({
                url: url,
                method:'post',
                data: { '_token' : _token,  'user_id':arg1, 'investmentStatus':arg2 },
                success:function(result)
                {
                   if(result==0)
                    {
                         swal("Approved!", "Your User is Approve.", "success");
                         window.location.reload();
                    }
                    else
                    {    $('#inner_msg').html('<strong>User Status Can not verify.</strong>');    }
                }
              });
            });
    }



</script>
@endsection