<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Decentralend Coin">
    <meta name="keywords" content="Decentralend Coin">
    <meta name="author" content="Decentralend Coin">
    <title>Forgot Password | Bidium Coin</title>
    @include('layouts.head')
  </head>
  <body>
    <style>
    span.help-block {
    color: #fff;
    font-weight: bold;
   }
    </style>
    <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->
    <div class="form-bg login-form-bg">
        <div class="container login-box">
          <div class="row">

          <div class="login-form col-sm-7 pt-4">
            <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">Forgot Password</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
          @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
                  @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
          <form id="register-form" class="tab-content active" action="{{route('forgotPassword.store')}}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Enter Email" id="exampleInputEmail1"  autocomplete="off">
                       @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
              </div>
              
             
              <div class="text-center">
                <button type="submit" class="btn btn-login text-center">Request</button>
              </div>
              
            </form>
          </div>
          <div class="login-info col-sm-5 text-center">
            <a href="/"><img class="img-fluid mt-4 mb-4 main-logo-login" src="assets/images/logo.png" /></a>
            <div class="d-flex-center login-left-div">
              <div class="row">
                <div class="col-sm-12 text-left">
                     
                <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="assets/images/icons/daily-transaction.png"></span>
                          <span class="font-15">Get bonus plus heavy discount on Pre-ICO.</span>
                    </div>
                     <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/online-business.png"></span>
                        <span class="font-15">Refer and get flat 25% referral bonus, Simply login copy your referral link and share it. </span>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Sign in Signup Ends -->
    @include('layouts.footerScript')
  </body>
</html>