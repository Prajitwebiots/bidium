<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="coin">
  <meta name="keywords" content="coin">
  <meta name="author" content="coin">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <title>Login | Bidium Coin</title>

  @include('layouts.head')
  @if (config('common.adCodes.analytics.google'))
    @include('layouts.ad-codes.google-analytics')
  @endif
  <style type="text/css">
    .alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    line-height: 20px;
}
  </style>
  </head>
  <body>
<?php   $ref = Session::get('ref', 'default'); Session::forget('ref');   ?>
  <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->

       <div class="form-bg login-form-bg login-new-bg">
        <div class="container login-box">
          <div class="row">

          <div class="login-form col-sm-7 pt-4">
            <div class="container">
            <div class="text-center">
               <h2 class="title">Sign in</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
          @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
          @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
          <form id="register-form"  action="{{ url('doLogin') }}" class="tab-content active" method="post">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" >
                 @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" >
                 @if ($errors->has('password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
              </div>
             
              <div class="text-center">
                <button type="submit" class="btn btn-login text-center">Login</button>
              </div>
              <div class="row mt-2">
                @if (config('common.timerConfigurations.current.groupId') == 1)
                    <div class="col-sm-7 text-left mt-3">
                      Not whitelisted yet?  <a href="/register" class="p-0 m-0">Join Whitelist</a>
                    </div>
                @endif

                <div class="col-sm-5 forgotpasstext mt-3">
                  <a href="/forgotPassword">Forgot your password?</a>
                </div>
              </div>
            </form>
          </div>
          <div class="login-info col-sm-5 text-center">
            <a href="/"><img class="img-fluid mt-4 mb-4 main-logo-login" src="assets/images/logo.png" /></a>
            <div class="d-flex-center login-left-div">
              <div class="container">
                <div class="col-sm-12 text-left">
                
                <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="assets/images/icons/daily-transaction.png"></span>
                          <span class="font-15">Get bonus plus heavy discount on Pre-ICO.</span>
                    </div>
                     <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/online-business.png"></span>
                        <span class="font-15">Refer and get flat 25% referral bonus, Simply login copy your referral link and share it. </span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span>&nbsp;</span>
                        <span class="font-15">&nbsp;</span>
                    </div>
                <!--    <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="assets/images/icons/ent.png"></span>
                          <span class="font-15">Create your own digital currency wallet in minutes.</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/ent.png"></span>
                        <span class="font-15">Setting up a wallet is always free.</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/online-business.png"></span>
                        <span class="font-15">Trusted by over 55,428 users.</span>
                    </div>
                -->
                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        
    <!-- Sign in Signup Ends -->
    @include('layouts.footerScript')
  </body>
</html>