<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="coin">
  <meta name="keywords" content="coin">
  <meta name="author" content="coin">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <title>Login | Bidium Coin</title>

  @include('layouts.head')
  <style type="text/css">
    .alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    line-height: 20px;
}
  </style>
  </head>
  <body>
  <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->

    <div class="form-bg">
      <div class="container">
        <div class="row">
          <div class="offset-sm-3 col-sm-6">
            <div class="row auth-logo mb-4">
              <a href="/"><img src="assets/images/logo-white.png"></a>
            </div>


            <div class="tab signin-up" role="tabpanel">
              <!-- Nav tabs -->
              <?php
                      $ref = Session::get('ref', 'default');
                      Session::forget('ref');                     
              ?>
              
                        <!-- Tab panes -->
              <div class="row register-step">
                <ul id="section-tabs">
                  <li class="current active"><span>1.</span>Basic Details</li>
                  <li><span>2.</span>Upload Documents</li>
                </ul>
                  <form class="form-horizontal theme-form" action="{{url('doRegister')}}" method="post">
                  <fieldset class="current">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                      <label for="exampleInputEmail1">User Name</label>
                      <input type="text" name="username" value="{{old('username')}}" class="form-control" id="exampleInputEmail1" autocomplete="off">
                      @if ($errors->has('username'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('username') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">First Name</label>
                      <input type="text" name="first_name" value="{{old('first_name')}}"  class="form-control" id="exampleInputEmail1" autocomplete="off">
                      @if ($errors->has('first_name'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('first_name') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Last Name</label>
                      <input type="text" name="last_name" value="{{old('last_name')}}"class="form-control" id="exampleInputEmail1" autocomplete="off">
                        @if ($errors->has('last_name'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('last_name') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Sponser</label>
                      <input type="text" name="sponser" class="form-control" value="{{old('sponser')}}@if(Session::get('referral')){{ Session::get('referral') }}@endif" id="exampleInputEmail1" autocomplete="off">
                        @if ($errors->has('sponser'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('sponser') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" name="email" value="{{old('email')}}" class="form-control" id="exampleInputEmail1" autocomplete="off">
                        @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                       @if ($errors->has('password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword2">
                         @if ($errors->has('confirm_password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                      </span>
                      @endif
                    </div>
                  </fieldset>
                  <fieldset class="next">
                    <div class="row m-b-1">
    <div class="col-sm-12">
      <button type="button" class="btn btn-primary btn-block" onclick="document.getElementById('inputFile').click()">Add Image</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile">File Upload</label>
        <input type="file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this); readimage(this);" data-title="Drag and drop a file">
      </div>
      <div>
        <img src="" id="blah" height="200" class="img-fluid">
      </div>
    </div>
  </div>
   <div class="row m-b-1">
    <div class="col-sm-12">
      <button type="button" class="btn btn-success btn-block" onclick="document.getElementById('inputFile').click()">Add Image</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile">File Upload</label>
        <input type="file" class="form-control-file text-success font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this); readimage(this);" data-title="Drag and drop a file">

      </div>
    </div>
    <div>
        <img src="" id="blah" height="200" class="img-fluid">
      </div>
  </div>
    <div class="row m-b-1">
    <div class="col-sm-12">
      <button type="button" class="btn btn-warning btn-block" onclick="document.getElementById('inputFile').click()">Add Image</button>
      <div class="form-group inputDnD">
        <label class="sr-only" for="inputFile">File Upload</label>
        <input type="file" class="form-control-file text-warning font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this); readimage(this);" data-title="Drag and drop a file">
      </div>
      <div>
        <img src="" id="blah" height="200" class="img-fluid">
      </div>
    </div>
  </div>
  
                  </fieldset>
                   <div class="form-group">
                      <a class="btn btn-theme text-white float-right" id="next">Next</a>
                      <input type="submit" class="btn btn-theme btn-block" value="Sign up" />

                    </div>
                  </form>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Sign in Signup Ends -->
    @include('layouts.footerScript')
    <script>
      function readUrl(input) {
  
  if (input.files && input.files[0]) {
    let reader = new FileReader();
    reader.onload = (e) => {
      let imgData = e.target.result;
      let imgName = input.files[0].name;
      let imgsrc = input.files[0].src;
      input.setAttribute("data-title", imgName);
      console.log(e.target.result);
      input.setAttribute("data-src", imgsrc);
    }
    reader.readAsDataURL(input.files[0]);
  }

}

 function readimage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
      $("body").on("keyup", "form", function(e){
      if (e.which == 13){
      if ($("#next").is(":visible") && $("fieldset.current").find("input, textarea").valid() ){
      e.preventDefault();
      nextSection();
      return false;
    }
  }
});
 
 
$("#next").on("click", function(e){
  console.log(e.target);
  nextSection();
});
 
$("form").on("submit", function(e){
  if ($("#next").is(":visible") || $("fieldset.current").index() < 1){
    e.preventDefault();
  }
});
 
function goToSection(i){
  $("fieldset:gt("+i+")").removeClass("current").addClass("next");
  $("fieldset:lt("+i+")").removeClass("current");
  $("li").eq(i).addClass("current").siblings().removeClass("current");
  setTimeout(function(){
    $("fieldset").eq(i).removeClass("next").addClass("current active");
      if ($("fieldset.current").index() == 1){
        $("#next").hide();
        $("input[type=submit]").show();
      } else {
        $("#next").show();
        $("input[type=submit]").hide();
      }
  }, 80);
 
}
 
function nextSection(){
  var i = $("fieldset.current").index();
  if (i < 1){
    $("li").eq(i+1).addClass("active");
    goToSection(i+1);
  }
}
 
$("li").on("click", function(e){
  var i = $(this).index();
  if ($(this).hasClass("active")){
    goToSection(i);
  } else {
    alert("Please complete previous sections first.");
  }
});
    </script>
  </body>
</html>