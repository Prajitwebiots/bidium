<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="title" content="Referal link" />
  <meta name="description" content="I have whitelisted, why you left behind get heavy bonuses.Hurry only for limited users." />

  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <title>Register | Bidium Coin</title>

  @include('layouts.head')
  @if (config('common.adCodes.analytics.google'))
    @include('layouts.ad-codes.google-analytics')
  @endif
  <style type="text/css">
    .alert-success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
        line-height: 20px;
    }
  </style>
  </head>
  <body>
  <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->
 <?php
                      $ref = Session::get('ref', 'default');
                      Session::forget('ref');                     
              ?>
    <div class="form-bg login-form-bg rg-form-bg">
        <div class="container login-box">
          <div class="row">
          <div class="login-info col-sm-5 text-center">
            <a href="/"><img class="img-fluid mt-4 main-logo-login" src="assets/images/logo.png" /></a>

            @if (config('common.timerConfigurations.current.groupId') == 1)
            <div class="col-sm-12 text-center">
                <h4 class="main-token-title-login">Hurry Up Token Sale will start in</h4>
                  <div class="timer-wrapper mt-3">
                      <p id="login-timer" style="min-height: 80px;"></p>
                     <!--<p id="login-timer"><span>9<span class="timer-cal">days</span></span><span>4<span class="timer-cal">hours</span></span> <span>40<span class="timer-cal">minutes</span></span> <span>0<span class="timer-cal">second</span></span> </p>-->
                  </div>
            </div>
            @endif
           
            <div class="d-flex-center register-left-div">
              <div class="row">
                <div class="col-sm-12 text-left">
                <div class="iconed-text mtop-80 mbt-30">
                       <span>&nbsp;</span>
                          <span class="font-15">First  20000 users will get 300 tokens free</span>
                    </div>
                     <div class="iconed-text mtop-80 mbt-30">
                        <span>&nbsp;</span>
                          <span class="font-15">Next 30000 users will get 200 tokens free</span>
                    </div>
                     <div class="iconed-text mtop-80 mbt-30">
                        <span>&nbsp;</span>
                          <span class="font-15">Next 50000 users will get 100 tokens free</span>
                    </div>
                     
                     <div class="iconed-text mtop-80 mbt-30">
                           <span>&nbsp;</span>
                          <span class="font-15">Get 25% Referral bonus</span>
                    </div>
                 <div class="iconed-text mtop-80 mbt-30">
                        <span>&nbsp;</span>
                          <span class="font-15">Please fill all the details correctly</span>
                    </div>
                    <div class="iconed-text mbt-30">
                      <span>&nbsp;</span>
                        <span class="font-15">Please enter your ETH address ( eg MEW and MetaMask wallet address)</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span>&nbsp;</span>
                        <span class="font-15">How to create and get ETH address. To know more <a href="/info">Click Here</a></span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span>&nbsp;</span>
                        <span class="font-15">&nbsp;<br/>&nbsp;</span>
                    </div>
<!--                     <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="assets/images/icons/daily-transaction.png"></span>
                          <span class="font-15">Create your own digital currency wallet in minutes.</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/online-business.png"></span>
                        <span class="font-15">Trusted by over 55,428 users.</span>
                    </div> -->
                  </div>
              </div>
            </div>
          </div>
          <div class="login-form col-sm-7">
            <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">Sign Up</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
          @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
          @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
          <form id="register-form" class="tab-content active" action="{{url('doRegister')}}" method="post"> 
 <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group form-row">
                <div class="col-sm-6">
                   <input type="text" name="first_name" value="{{old('first_name')}}" placeholder="First Name" class="form-control" id="first_name" autocomplete="off">
                  @if ($errors->has('first_name'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('first_name') }}</strong>
                      </span>
                      @endif
                </div>
                <div class="col-sm-6">
                   <input type="text" name="last_name" value="{{old('last_name')}}" placeholder="Last Name" class="form-control" id="last_name" autocomplete="off">
                        @if ($errors->has('last_name'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('last_name') }}</strong>
                      </span>
                      @endif
                </div>
              </div>
              <div class="form-group form-row">
                <div class="col-sm-6">
                 <input type="text" name="username" value="{{old('username')}}" placeholder="User Name" class="form-control" id="username" autocomplete="off">
                      @if ($errors->has('username'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('username') }}</strong>
                      </span>
                      @endif
                </div>
                <div class="col-sm-6">
                   <input type="text" name="sponser" class="form-control" value="@if(Session::get('referral')){{ Session::get('referral') }}@endif" id="sponser" placeholder="Sponser Name" autocomplete="off">
                        @if ($errors->has('sponser'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('sponser') }}</strong>
                      </span>
                      @endif
                </div>
              </div>
              <div class="form-group form-row">
                <input type="email" name="email" value="{{old('email')}}" placeholder="Email" class="form-control" id="email" autocomplete="off">
                        @if ($errors->has('email'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group form-row">
                <input type="text" onfocus="(this.type='date')" name="dob" value="{{old('dob')}}"  class="form-control" id="dob" placeholder="Date Of Birth" autocomplete="off">
                      @if ($errors->has('dob'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('dob') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group form-row">
                  <input type="text" name="erc_20_address" value="{{old('erc_20_address')}}" placeholder="ETH Address" onchange="checkaddress(this.value)" class="form-control" id="erc_20_address" autocomplete="off">
                        @if ($errors->has('erc_20_address'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('erc_20_address') }}</strong>
                      </span>
                      @endif
                      <span class="help-block text-danger" id="address"></span>
              </div>
              <div class="form-group form-row">
                <div class="col-sm-6">
                   <input type="password" name="password" value="{{old('password')}}"  placeholder="Password" class="form-control" id="password" autocomplete="off">
                      @if ($errors->has('password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
                </div>
                <div class="col-sm-6">
                 <input type="password" name="confirm_password" value="{{old('confirm_password')}}" placeholder="Confirm Password" class="form-control" id="confirm_password" autocomplete="off">
                        @if ($errors->has('confirm_password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                      </span>
                      @endif
                </div>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-login text-center disabled" id="register">Register</button>
              </div>
              <div class="row mt-5 text-center">
                <div class="col-sm-12">
                 Already have an account?  <a href="/login" class="p-0 m-0">Log In</a>
                </div>
                
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

@include('layouts.footerScript')
<script>
    function checkaddress(address)
    {
        $('#address').empty();
         $.get("check-address/"+address, function(data, status){
           if(data == 0)
           { 
              $('#address').removeClass();
              $('#address').addClass('help-block text-danger');
              $('#address').append('<strong> Please Enter Valid Address </strong>');
              $('#register').addClass('disabled');
           }else{
              $('#address').removeClass();
              $('#address').addClass('help-block text-success');
              $('#address').append('<strong> ERC20 Address Valid !! </strong>');
              $('#register').removeClass('disabled');
           }
        });  
    }
</script>
@if (config('common.timerConfigurations.current.groupId') == 1)
<script>
   /* _____________________________________
   
        Count down
        _____________________________________ */

   var countDownDate = moment("{{ config('common.timerConfigurations.current.data.timerDate') }}", "YYYY-MM-DD HH:mm:ss").valueOf();
   //var countDownDate = new Date("{{ $setting->ico_start_date }}").getTime();

   // Update the count down every 1 second
   var countdownfunction = setInterval(function() {
   
       // Get todays date and time
       var now = new Date().getTime();
       
       // Find the distance between now an the count down date
       var distance = countDownDate - now;
       
       // Time calculations for days, hours, minutes and seconds
       var days = Math.floor(distance / (1000 * 60 * 60 * 24));
       var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
       var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
       var seconds = Math.floor((distance % (1000 * 60)) / 1000);
       
       // Output the result in an element with id="timer"
       document.getElementById("login-timer").innerHTML = "<span>" + days + "<span class='timer-cal'>days</span></span>" + "<span>" + hours + "<span class='timer-cal'>hours</span></span> "
       + "<span>" + minutes + "<span class='timer-cal'>minutes</span></span> " + "<span>" + seconds + "<span class='timer-cal'>second</span></span> ";
      
       
       // If the count down is over, write some text 
       if (distance < 0) {
         clearInterval(countdownfunction);
         document.getElementById("timer").innerHTML = "EXPIRED";
      }
   }, 1000);
</script>
@endif

<script>
   $(document).ready(function(){
       // Add smooth scrolling to all links
       $("nav a").on('click', function(event) {

           // Make sure this.hash has a value before overriding default behavior
           if (this.hash !== "") {
               // Prevent default anchor click behavior
               event.preventDefault();

               // Store hash
               var hash = this.hash;

               // Using jQuery's animate() method to add smooth page scroll
               // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
             /*  $('html, body').animate({
               scrollTop: $(hash).offset().top
               }, 1, function(){

               // Add hash (#) to URL when done scrolling (default click behavior)
               window.location.hash = hash;
               });*/
           } // End if
       });
   });
</script>
  <!-- Sign in Signup Ends -->
  </body>
</html>