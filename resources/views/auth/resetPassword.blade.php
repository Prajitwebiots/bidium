<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="coin">
  <meta name="keywords" content="coin">
  <meta name="author" content="coin">
  <link rel="icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon"/>
  <title>Reset Password | Bidium Coin</title>

  @include('layouts.head')
  <style type="text/css">
    .alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    line-height: 20px;
}
  </style>
  </head>
  <body>
<?php   $ref = Session::get('ref', 'default'); Session::forget('ref');   ?>
  <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->

       <div class="form-bg login-form-bg">
        <div class="container login-box">
          <div class="row">

          <div class="login-form col-sm-7 pt-4">
            <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">Reset Password</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
          @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
          @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
     
{!! Form::model($user, ['method' => 'PATCH','route' => ['forgotPassword.update', $user->id], 'class'=> 'tab-content active']) !!}
 <input class="form-control" id="email" type="hidden" name="email" value="{{$user->email}}">
              <input class="form-control" id="code" type="hidden" name="code" value="{{$code}}">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input type="password" name="password" placeholder="Password" class="form-control" id="password">
                 @if ($errors->has('password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group">
                <input type="password" name="confirm_password" placeholder="Confirm Password"  class="form-control" id="confirm-pwd">
                  @if ($errors->has('confirm_password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                      </span>
                  @endif
              </div>
             
              <div class="text-center">
                <button type="submit" class="btn btn-login text-center">Login</button>
              </div>
              <div class="row mt-5">
                <div class="col-sm-7 text-left">
                  Do not whitelisted yet?  <a href="/register" class="p-0 m-0">Join Whitelist</a>
                </div>
                <div class="col-sm-5 text-right">
                  <a href="/forgotPassword">Forgot your password?</a>
                </div>
              </div>
           {!! Form::close(); !!}
          </div>
          <div class="login-info col-sm-5 text-center">
            <img class="img-fluid mt-4 main-logo-login" src="{{ url('assets/images/logo.png') }}" />
            <div class="d-flex-center login-left-div">
              <div class="row">
                <div class="col-sm-12 text-left">
                      <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="assets/images/icons/daily-transaction.png"></span>
                          <span class="font-15">Get bonus plus heavy discount on Pre-ICO.</span>
                    </div>
                     <div class="iconed-text mbt-30">
                        <span><img src="assets/images/icons/online-business.png"></span>
                        <span class="font-15">Refer and get flat 25% referral bonus, Simply login copy your referral link and share it. </span>
                    </div>
                    
                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        
    <!-- Sign in Signup Ends -->
    @include('layouts.footerScript')
  </body>
</html>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Decentralend Coin">
    <meta name="keywords" content="Decentralend Coin">
    <meta name="author" content="Decentralend Coin">
    <title>Reset Password | Bidium Coin</title>
    @include('layouts.head')
  </head>
  <body>
    <style>
    span.help-block {
    color: #fff;
    font-weight: bold;
    }
    </style>
    <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <div class="form-bg">
      <div class="container">
        <div class="row">
          <div class="offset-sm-3 col-sm-6">
            <div class="tab signin-up" role="tabpanel">

            
                  <!-- Nav tabs -->
 <ul class="nav nav-tabs mb-5" role="tablist">
              <li role="presentation"><a href="#signin" class="active" aria-selected="true" aria-controls="home" role="tab" data-toggle="tab">Reset Pasword</a></li>
              <li class="navbar-brand-2">
                <a href=""><img src="{{ url('assets/images/logo-white.png') }}"></a>
              </li>
            </ul>
            <!-- Tab panes -->

              <!-- Tab panes -->
              {!! Form::model($user, ['method' => 'PATCH','route' => ['forgotPassword.update', $user->id], 'class'=> 'form-horizontal theme-form']) !!}
                 
                 @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
                 @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
              
              <input class="form-control" id="email" type="hidden" name="email" value="{{$user->email}}">
              <input class="form-control" id="code" type="hidden" name="code" value="{{$code}}">

              <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password">
                 @if ($errors->has('password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group">
                <label for="confirm-pwd">Confirm Password</label>
                <input type="password" name="confirm_password"  class="form-control" id="confirm-pwd">
                  @if ($errors->has('confirm_password'))
                      <span class="help-block text-danger">
                        <strong>{{ $errors->first('confirm_password') }}</strong>
                      </span>
                      @endif
              </div>
              <div class="form-group">
                    <button type="submit" class="btn btn-theme btn-block">Confirm</button>
              </div>
              {!! Form::close(); !!}
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Sign in Signup Ends -->
@include('layouts.footerScript')
  </body>
</html>