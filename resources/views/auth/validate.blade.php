<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Decentralend Coin">
    <meta name="keywords" content="Decentralend Coin">
    <meta name="author" content="Decentralend Coin">
    <title>Validator | Bidium Coin</title>
    @include('layouts.head')
  </head>
  <body>
    <style>
    span.help-block {
    color: #fff;
    font-weight: bold;
   }
    </style>
    <!-- Loader Start-->
    <div class="spinner-wrapper">
      <div class="spin"></div>
    </div>
    <!-- Loader Ends-->
    <!-- Sign in Signup start -->
    <div class="form-bg login-form-bg">
        <div class="container login-box">
          <div class="row">

          <div class="login-form col-sm-7 pt-4">
            <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">OTP</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
         @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
          @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
          @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
          <form id="register-form" class="tab-content active" method="post" action="{{ url('2fa/validate') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group">
                <input class="form-control placeholder-no-fix" type="number" placeholder="Enter OTP"  name="totp" />
                @if ($errors->has('totp'))
                    <span class="help-block">
                        <strong>{{ $errors->first('totp') }}</strong>
                    </span>
                @endif
              </div>
              
             
              <div class="text-center">
                <button type="submit" class="btn btn-login text-center">Validate</button>
              </div>
              
            </form>
          </div>
          <div class="login-info col-sm-5 text-center">
             <a href="/"><img class="img-fluid mt-4 mb-4 main-logo-login" src="../assets/images/logo.png" /></a>
            <div class="d-flex-center login-left-div">
              <div class="row">
                <div class="col-sm-12 text-left">
                    <div class="iconed-text mtop-80 mbt-30">
                        <span><img src="../assets/images/icons/daily-transaction.png"></span>
                          <span class="font-15">Create your own digital currency wallet in minutes.</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span><img src="../assets/images/icons/ent.png"></span>
                        <span class="font-15">Setting up a wallet is always free.</span>
                    </div>
                    <div class="iconed-text mbt-30">
                        <span><img src="../assets/images/icons/online-business.png"></span>
                        <span class="font-15">Trusted by over 55,428 users.</span>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Sign in Signup Ends -->
    @include('layouts.footerScript')
  </body>
</html>

