@extends('layouts.master')
@section('title') Dashboard | Bidium Coin @endsection
@section('style')
<style type="text/css">
a.btn.btn-success.btn-sm {
    color: #fff;
}
</style>
@endsection
@section('content')
<?php
$slug = Sentinel::getUser()->roles()->first()->slug;
?>
<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
<div class="dashboard-body">
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">Dashborad</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
            </li>
            <li class="breadcrumb-item"><a href="#!">Dashborad</a>
            </li>
        </ol>
    </div>
    @if($slug == 2)
    <div class="col-md-6">
        <form class="form-copy theme-form">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" id="post-shortlink" readonly="" class="form-control" value="{{url('/ref')}}/{{Sentinel::getUser()->username}}" aria-label="Search for...">
                    <span class="input-group-btn">
<button class="btn btn-theme" id="copy-button" type="button" data-clipboard-target="#post-shortlink" data-copytarget="#lets_copy" onclick="copytext()">copy</button>
</span>
                </div>
            </div>
            <p class="bouns-note">Get bonus by referring new members </p>
            <div class="col-sm-12">
                <h5 class="text-center mb-2">Share Referal link on Social media</h5>
                <ul class="text-center referal-link-share p-0">
                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url('/ref')}}/{{Sentinel::getUser()->username}}" target="_blank"><img src="{{ url('assets/images/small-icon/facebook-share.png') }}"></a></li>
                    <li><a href="http://twitter.com/share?text=Join+us+today+and+get+heavy+bonus.&url={{url('/ref')}}/{{Sentinel::getUser()->username}}" target="_blank"><img src="{{ url('assets/images/small-icon/twitter-share.png') }}"></a></li>
                    <li><a href="https://telegram.me/share/url?url={{url('/ref')}}/{{Sentinel::getUser()->username}}&text=Join us today and get heavy bonus." target="_blank"><img src="{{ url('assets/images/small-icon/telegram-share.png') }}"></a></li>
                    <li><a href="https://api.whatsapp.com/send?text=Join us today and get heavy bonus. {{url('/ref')}}/{{Sentinel::getUser()->username}}" target="_blank"><img src="{{ url('assets/images/small-icon/whatsapp.png') }}"></a></li>
                </ul>
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <div class="card icon-timer">
            <div class="card-header">
                @if (config('common.timerConfigurations.current.groupId') == 1 || config('common.timerConfigurations.current.groupId') == 9)
                    <h4 class="text-center"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Pre-ICO will start in:</h4>
                @endif
                @if (config('common.timerConfigurations.current.groupId') == 2)
                    <h4 class="text-center"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Pre-ICO started, 25% Bonus will End in:</h4>
                @endif
                @if (config('common.timerConfigurations.current.groupId') == 5)
                    <h4 class="text-center"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Get 15% Bonus , Pre-ICO will End in:</h4>
                @endif
            </div>
            <div class="card-body">
                <div class="timer-wrapper">
                    <p id="timer-modal">
                    </p>
                </div>
            </div>
        </div>
    </div>
        <!-- <div class="col-lg-6">
        <form class="form-copy theme-form">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" id="post-shortlink2" readonly="" class="form-control" value="{{Sentinel::getUser()->bidm_address}}" aria-label="Search for...">
                    <span class="input-group-btn">
<button class="btn btn-theme" id="copy-button" type="button" data-clipboard-target="#post-shortlink2" data-copytarget="#lets_copy" onclick="copytext2()">copy</button>
</span>
                </div>
            </div>
            <p class="bouns-note">BIDM Token address (You can use this address for token transfer)</p>
        </form>
    </div> -->
    @endif
    <div class="col-lg-12">
        <div class="card coin-value">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="media">
                            <img class="mr-3" src="{{ url('assets/images/coin/btc.png') }}" alt="bitcoin">
                            <div class="media-body">
                                <h5 class="mt-2"> BTC Balance</h5>
                                <h4 class="blue-font mt-0"><span class="counter">@if($slug == "2") {{number_format(Sentinel::getUser()->total_btc_bal,8)}} @elseif($slug == 1) 0.00000000 @endif</span></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="media">
                            <img class="mr-3" src="{{ url('assets/images/coin/eth.png') }}" alt="ETH">
                            <div class="media-body">
                                <h5 class="mt-2">ETH Balance</h5>
                                <h4 class="blue-font mt-0"><span class="counter">@if($slug == "2") {{number_format(Sentinel::getUser()->total_eth_bal,8)}} @elseif($slug == 1) 0.00000000 @endif</span></h4>
                            </div>
                        </div>
                    </div>
             <!--        <div class="col-lg-3">
                        <div class="media">
                            <img class="mr-3" src="{{ url('assets/images/coin/bidium.png') }}" alt="ETH">
                            <div class="media-body">
                                <h5 class="mt-2">BIDIUM</h5>
                                <h4 class="blue-font mt-0"><span class="counter">{{number_format(Sentinel::getUser()->total_bidm_bal)}}</span></h4>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-lg-4">
                        <div class="media">
                            <img class="mr-3" src="{{ url('assets/images/$.png') }}" alt="dollar">
                            <div class="media-body">
                                <h5 class="mt-2">USD Rate</h5>
                                <h4 class="blue-font mt-0"><span class="counter">{{ $setting->rate }}</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  @if($slug == 2)
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">BIDM Token Balance</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->total_bidm_bal)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Token Bonus Balance</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->token_bonus)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Referral Bonus Balance</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->referral_bonus)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Whitelist Bonus Balance</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->whitelist_bonus)}}</h2>
            </div>
        </div>
</div>
@endif


 @if($slug == 1)
 
    <div class="col-sm-4">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Total BIDM Tokens</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format($setting->total_coins)}}</h2>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Sold BIDM Tokens</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format($setting->sold_coins)}}</h2>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">ICO End at</h4>
            </div>
            <div class="card-body">
                <div class="timer-wrapper">
                    <p id="timer">
                    </p>
                </div>
            </div>
        </div>
    </div>



<div class="col-sm-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <div class="col-md-12 ">
                                <h4>Coin Distribution</h4>
                            </div>
                            <div class="mb-4"></div>
                            <!-- <table class="table table-striped data-table"> -->
                               <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr>

                  <th scope="col">Sr.</th>
                  <th scope="col">Name</th>
                  <th scope="col">Percentage</th>
                  <th scope="col">Total Coins</th>
                  <th scope="col">Sold Coins</th>

               </tr>
            </thead>
            <tbody>

            @foreach($coins as $key)

                <tr>
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $key->name }}</td>
                    <td>{{ $key->percentage }} %</td>
                    <td>{{ $key->total_coins }}</td>
                    <td>{{ $key->sold_coins }}</td>
                </tr>

            @endforeach


       </tbody>
         </table>
                        </div>
                    </div>
                </div>




                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body table-responsive">
                            <div class="col-md-12 ">
                                <h4>ICO Calender</h4>
                            </div>
                            <div class="mb-4"></div>
                            <!-- <table class="table table-striped data-table"> -->
                               <table id="data-table" class="table " cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr >

                  <th scope="col">Sr.</th>
                  <th scope="col">Name</th>
                  <th scope="col">Bonus</th>
                  <th scope="col">Referral Bonus</th>
                  <th scope="col">Days</th>
                  <th scope="col">Usd price</th>
                  <th scope="col">Start Date</th>
                  <th scope="col">End Date</th>
                  <th scope="col">Sold Coins</th>

               </tr>
            </thead>
            <tbody>







            <?php
$current_date = strtotime(date('Y-m-d h:i:s'));

?>
                @foreach($rates as $key)

<?php
$start_date = strtotime($key->start_date);
$end_date = strtotime($key->end_date);
if (($current_date >= $start_date) && ($current_date <= $end_date)) {
	$table_active = 'table-custom';
} else {
	$table_active = '';
}

?>
                <tr class="{{$table_active}}">
                    <td>{{ $loop->iteration}}</td>
                    <td>{{ $key->name }}</td>
                    <td>{{ $key->bonus }}%</td>
                    <td>{{ $key->ref_bonus }}%</td>
                    <td>{{ $key->days }} Days</td>
                    <td>{{ number_format($key->usd_price,2) }} $</td>
                    <td>{{ $key->start_date }}</td>
                    <td>{{ $key->end_date }}</td>
                    <td>{{ $key->sold_coins }}</td>
                </tr>

            @endforeach


       </tbody>
         </table>
                        </div>
                    </div>
                </div>
 @endif

</div>

</div>
@endsection @section('script')
<script>
function copytext() {
var copyText = document.getElementById("post-shortlink");
copyText.select();
document.execCommand("Copy");
}

function copytext2() {
var copyText = document.getElementById("post-shortlink2");
copyText.select();
document.execCommand("Copy");
}

</script>
<script type="text/javascript">
         /* ___________________________________

             Count down
             ___________________________________ */
         var countDownDate = moment("{{ config('common.timerConfigurations.current.data.timerDate') }}", "YYYY-MM-DD HH:mm:ss").valueOf();

             //var countDownDate = new Date("Mar 5 2018 15:37:25").getTime();
             //var countDownDate = new Date(new_date).getTime();

        // Update the count down every 1 second
        var countdownfunction = setInterval(function() {

            // Get todays date and timer
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="timer"
            document.getElementById("timer-modal").innerHTML = "<span>" + days + "<span class='timer-cal'>days</span></span> :" + "<span>" + hours + "<span class='timer-cal'>hours</span></span> :"
            + "<span>" + minutes + "<span class='timer-cal'>minutes</span></span> :" + "<span>" + seconds + "<span class='timer-cal'>seconds</span></span> ";

            // If the count down is over, write some text
            if (distance < 0) {
              clearInterval(countdownfunction);
              document.getElementById("timer-modal").innerHTML = "EXPIRED";
              location.reload();
            }
          }, 1000);
      </script>
@endsection