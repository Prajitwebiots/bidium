<!DOCTYPE html>
<html lang="en" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>BIDIUM</title>
<style type="text/css">
  .email {
  display: inline-block;
  text-align: center;
  margin: 0 auto;
  background-color: #F0F4F7;
  border-top: 3px solid #27A9E0;
  border-bottom: 3px solid #27A9E0;
  width: 600px;
  margin: 0 auto;
  display: block;
}

.logo {
  margin: 0 auto;
  text-align: center;
  display: block;
  margin-top: 30px;
  margin-bottom: 30px;
}

.white-container {
  background: #ffffff;
  width: 560px;
  text-align: center;
  margin: 0 auto;
  padding: 30px 0;
  margin-top: -10px; height: 225px;

}

h3 {
  font-weight: light;
}


.button {
  padding: 18px 25px;
  background-color: #27A9E0;
  border-radius: 30px;
  text-decoration: none;
  color: white;
  text-transform: uppercase;
  font-size: 13px;
  letter-spacing: 1px;
}

.link {
  margin-top: 30px;
  text-decoration: none;
  color: #303141;
  display: inline-block;
}

.links {
  margin: 0 auto;
  text-align: center;
  display: block;
}

.sm {
  max-width: 25px;
  display: inline-block;
  margin: 30px 5px;
}

h3 {
  width: 400px;
  margin: 0 auto;
  margin-bottom: 50px;
}
.enquiry{
  margin-top: 30px;
}
@media screen and (max-width: 768px){
.email {
  width: 100%;
}
.white-container{
  width: 100%;
}
h3{
  width: 100%;
}

}
</style>
</head>
<body>
<div style="font-family:Tondo; margin: 20px;">
<div class="email">
<img class="logo" src="{{url('assets/images/logo.png')}}" width="200">
<!-- <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/615414/Image.jpg"> -->
  <div class="white-container">
    <h2>Welcome To Bidium</h2>

    <!--h3>You’re almost there! Click the button below to verify your email address.</h3-->
    <h3>Now You are successfully whitelisted. Please login and you can contribute on Bidium.</h3>

    <a href="{{url('/login')}}" class="button">Login To Account</a> 
  </div>

    <div class="enquiry">
        For enquiries, please email us at <strong>{{ config('common.emails.list.support.email') }}</strong>
    </div>

    <div class="social-media">
        <a href="{{ config('common.social.facebook.url') }}"><img class="sm" src="{{ url( 'assets/images/small-icon/' . config('common.social.facebook.mailIcon') ) }}" ></a>
        <a href="{{ config('common.social.twitter.url') }}"><img class="sm" src="{{ url( 'assets/images/small-icon/' . config('common.social.twitter.mailIcon') ) }}" ></a>
        <a href="{{ config('common.social.telegram.url') }}"><img class="sm" src="{{ url( 'assets/images/small-icon/' . config('common.social.telegram.mailIcon') ) }}" ></a>
    </div>
    <div class="copyright">
        <h4 class="text-center">{!! __('app.mailCopyright') !!}</h4>
    </div>
</div>
</div>
</body>
</html>
