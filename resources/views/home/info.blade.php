@extends('layouts.masterHome')
@section('title') Bidium | An advance solution of trading,auction and freelance hiring @endsection
@section('style')
<style>


.manual{font-size:16px;line-height:32px;padding:40px 0 27px 0;}
.manual .max-all{margin:0 auto;max-width:980px;    margin-top: 100px;}
.manual p{margin:16px 0 0 0;    line-height: 30px;
    font-family: sans-serif;}
.manual h1{font-size:64px;line-height:80px;letter-spacing:-0.71px;margin:0 0 50px 0;text-align:center;}
.manual h2{font-size:24px;letter-spacing:2px;line-height:2;text-transform:uppercase;margin:0 0 13px 0;font-weight:600;}
.manual h2 + *{margin-top:0;}
.manual .text{padding:30px 10px 24px 10px;margin:0 -10px;}
.manual .text .max{margin:0 auto;max-width:580px;}
.manual .top{padding-top:25px;padding-bottom:55px;font-size:20px;background:rgba(10, 60, 87, 0.03);border-radius:4px;}
.manual .image{margin:0 -5000px;padding:0 5000px;}
.manual .image img{width:100%;height:auto;}

@media only screen and (max-width: 1030px){
.container{padding-left:10px;padding-right:10px;}
h1,.manual h1{letter-spacing:-0.78px;line-height:1.142;font-size:70px;margin-bottom:27px;}
}

@media only screen and (max-width: 750px){
.container{padding-left:10px;padding-right:10px;}
h1,.manual h1{letter-spacing:-0.44px;line-height:1.2;font-size:40px;margin-bottom:18px;}
h2{font-size:32px;letter-spacing:-0.5px;}
}

.virtual-anchor{height:0;margin-top:-150px;margin-bottom:150px;}

</style>
@endsection
@section('content')
<div class="section manual">
		<div class="container">
			<div class="max-all">
                                 <div class="mt-10"></div>
				<h1>How to create  wallets on MyEtherWallet and MetaMask</h1>
				<div class="text top">
					<div class="max">
						<p></p><p>To participate in the distribution of BIDM tokens, you can  send Ethers (ETH) to the address of the Token Sale smart contract. This process requires an ERC-20 wallet.</p><p>We recommend using <a href="#myetherwallet">MyEtherWallet</a> or <a href="#metamask">MetaMask</a> to buy BIDM tokens.</p><p></p>
					</div>
					 
				</div>
				
				<div class="text">
					<div class="virtual-anchor" id="myetherwallet"></div>
					<div class="max">
						<h2>Creating your MyEtherWallet</h2>
						<p>1. Please go to: <a href="https://myetherwallet.com/" target="_blank">https://myetherwallet.com/</a></p><p>Create a complicated password, consisting of numbers, letters and punctuation signs. After that, click <q>Create New Wallet</q> button.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-1.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>2. Click on <q>Download Keystore File</q>. Store it safely on your computer.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-2.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>3. Click on <q>I understand. Continue</q>.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-3.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>4. Save your private key. You will be able to access your wallet using this key.</p><p><strong>It is very important to keep your private key safe, because if you lose it, unless the Keystore File is used, you will not be able to login to your wallet.</strong></p><p><strong>Make sure to save your public and private keys. Do not give them away!</strong></p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-4.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>5. Click on <q>Save Your Address</q>.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-5.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>6. First choose <q>Private Key</q> to access your wallet. Then enter your private key in the blank and click <q>unlock</q>.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-6.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>7. Go to Send Ether &amp; Tokens tab. Enter your private key.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-7.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						
						<p>8. You will see the tab <q>Wallet successfully decrypted</q> and your address.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/1-8.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p><strong>Attention</strong>: Please carry out all the steps with caution. If your private key and Keystore file are lost - you will no longer have access to your wallet and all the assets stored in it.</p><p>Only you have control over your assets. The team of Bankex is not able to restore the access to your wallet under any circumstances. Blockchain Ethereum ensures that no one, including the creators of the system, can gain control over your assets without having a private key from your wallet.</p><p>The process of creating a public and private key for your wallet can only be carried out on your own computer. Neither do we have access to your wallet, nor do we store your private key. All further operations that require your private key, such as transactions can only be carried out on your computer. Unlike regular passwords, a private key can not be altered.</p><p>If you have a private key, you can use it to get the public key, but it does not work vice versa!</p><p><strong>Great! You have created your own Ethereum wallet on MyEtherWallet!</strong></p>
					</div>
					
				</div>
				
				<div class="text ind2">
					<div class="virtual-anchor" id="metamask"></div>
					<div class="max">
						<h2>Creating your MetaMask wallet</h2><p>MetaMask is more than just an Ether wallet. It's an Ethereum Browser, like Mist! It allows you all the same functions, features and ease of access from regular Ethereum Wallets, and it allows you to interact with Dapps and Smart Contracts, and all without the need to download the blockchain or install any software, you can just install it as a Google Chrome Extension! Plus, it has a cool fox logo that follows your mouse around, which made us smile. So, let's get started with MetaMask!</p><p>You need to use Google Chrome browser.</p>
						<p>1. Please follow the link and install <a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en" target="_blank">MetaMask plugin to your Google Chrome browser</a>.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-1.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>2. Click on the <q>+ADD TO CHROME</q> button to add the plugin to chrome</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-2.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>Confirm by clicking Add extension</p><p>3. A new tab will open and the MetaMask plugin is displayed at the right top of your Chrome browser.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-3.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>4. Scroll down and click <q>Continue</q></p> to agree with MetaMasks' terms of use<p></p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-4.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>5. Fill in a strong password to use MetaMask and click <q>Create</q></p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-5.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p><strong>Do NOT lose your password!</strong></p><p>6. Then you will see 12 words which you should save in a secure place! These 12 words can be used to gain access to your wallet.</p><p>Safe your secret 12 word recovery key somewhere safe and press <q>I'VE COPIED IT SOMEWHERE SAFE</q> or <q>SAVE SEED WORDS AS FILE</q>.</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-6.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p>You're ready to fund your account! Click the copy icon to copy your wallet address</p>
					</div>
					
				</div>
				
				<div class="image"><img src="{{ url('assets/images/wallets/2-7.png') }}" width="1000" height="532" alt=""></div>
				
				<div class="text">
					<div class="max">
						<p><strong>Attention</strong>: Please carry out all the steps with caution.</p><p>Only you have control over your assets. The team of Bidium is not able to restore the access to your wallet under any circumstances. Blockchain Ethereum ensures that no one, including the creators of the system, can gain control over your assets without having a private key from your wallet.</p><p><strong>You have created your own Ethereum wallet on MetaMask!</strong></p>
					</div>
					
				</div>
				
			</div>
			 
		</div>
		 
	</div>
@endsection