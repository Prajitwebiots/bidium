<footer>
   <div class="container footer-wrapper">
      <div class="row">
         <div class="col-sm-12">
            <h2 class="title">
               Contact Us On Social Media
            </h2>
            <div>
               <ul class="social-links-list mt-5 text-center">
                  
                  <li>
                     <a target="_blank" href="https://web.telegram.org/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/3.png" alt="" class="img-responsive">
                        </div>
                        <span>Telegram</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://www.instagram.com/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/4.png" alt="" class="img-responsive">
                        </div>
                        <span>Instagram</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://twitter.com/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/5.png" alt="" class="img-responsive">
                        </div>
                        <span>Twitter</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://www.facebook.com/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/8.png" alt="" class="img-responsive">
                        </div>
                        <span>Facebook</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://www.youtube.com/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/1.png" alt="" class="img-responsive">
                        </div>
                        <span>Youtube</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://bitcointalk.org/" class="abs-link">
                        <div class="icon-holder">
                           <img src="assets/images/social/6.png" alt="" class="img-responsive">
                        </div>
                        <span>Bitcointalk</span>
                     </a>
                  </li>
                  <li>
                     <a target="_blank" href="https://www.reddit.com/" class="abs-link">
                        <div class="icon-holder">
                          <img src="assets/images/social/2.png " alt="" class="img-responsive">
                        </div>
                        <span>reddit</span>
                     </a>
                  </li>
               </ul>

               <ul class="footer-menu text-center mt-5">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">about</a></li>
                  <li><a href="#">Terms</a></li>
                  <li><a href="#">Privacy</a></li>
                  <li><a href="#">White paper</a></li>                 
               </ul>
                <p class="footer-mail text-center"><a class="text-white" href="mailto:support@bidium.io">support@bidium.io</a> </p>
            </div>
         </div>
      </div>
   </div>
   <div class="copyright">
      <h4 class="text-center">{!! __('app.webCopyright') !!}</h4>
   </div>
</footer>
<!-- Footer ends -->