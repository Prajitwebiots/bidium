<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="BIDIUM coin">
  <meta name="keywords" content="BIDIUM coin">
  <meta name="author" content="BIDIUM coin">
  <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon"/>
  <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon"/>
  <title>@yield('title')</title>
    @include('layouts.head')
  </head>
  <body>
    @include('layouts.header')
    <div class="dashboard-wrapper" style="margin-top: 50px !important;">
      @include('layouts.sidebar')
      @yield('content')
    </div>
    @include('layouts.footerScript')
  </body>
  
</html>

