<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Advance combination of auction platform , freelancing platform and crypto-fiat-crypto trading/exchange platform.">
    <meta name="keywords" content="Exchange platform, Bidium, freelancing Platform, Best ICO ">
    <meta name="author" content="Bidium ">
    <link rel="icon" href="{{ url('assets/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ url('assets/images/favicon.png') }}" type="image/x-icon"/>
    <title>@yield('title')</title>
    @include('layouts.head')
    @if (config('common.adCodes.analytics.google'))
      @include('layouts.ad-codes.google-analytics')
    @endif
  </head>
  <body>
    <!-- Loader Start-->
     
    <div class="loader-wrapper">
      <div class="loader">
        
      </div>
  </div> 
    <!-- Loader Ends-->
    @include('layouts.headerHome')
    @yield('content')
    @include('layouts.footerScript')
    @include('layouts.footerHome')
    @stack('footer-scripts')
  </body>
</html>
<script type="text/javascript">
  $(window).on('load', function() {
      $('.loader-wrapper').fadeOut('slow');
      $('.loader-wrapper').remove('slow');
    });

</script>