'<?php
// check admin or user
$slug = Sentinel::getUser()->roles()->first()->slug;
?>
<div class="nice-nav" style="top: 64px !important;">
   <div class="user-info">
       <div class="media">
           <img class="mr-3 align-self-center" src="{{ url('/assets/images/user')}}/{{Sentinel::getUser()->profile}}" alt="Generic placeholder image">
           <div class="media-body">
                  <h5>{{Sentinel::getUser()->first_name}}</h5>
                 <h6>{{Sentinel::getUser()->last_name}}</h6>
           </div>
       </div>
   </div>
  @if($slug == 1)
   <ul>
       <li>
           <a href="{{ url('admin/dashboard') }}" class="{{{ (Request::is('admin/dashboard') ? 'active ' : '') }}}">Dashboard</a>
       </li>
        <li>
           <a href="{{ url('admin/profile') }}" class="{{{ (Request::is('admin/profile') ? 'active ' : '') }}}">Profile</a>
       </li>
       <li class="child-menu">
               <a href="#"><span>Users</span> <span class="fa fa-angle-right toggle-right"></span>
               </a>
               <ul class="child-menu-ul">
                  <li>
                    <a href="{{ url('admin/users') }}" >Manage Users</a>
                  </li>
                  <li>
                    <a href="{{ url('admin/invRequest') }}" >Investment Request  @if($header_data['inv'] != 0) <span class="badge badge-warning" style="float:right;font-size: 18px;border-radius: 16px;margin: -6px 0px 0px 0px;color:  #fff;padding-top: 3px;width:  28px;">{{ $header_data['inv'] }}
               </span> @endif</a>
                  </li>
                  <li>
                      <a href="{{ url('admin/kyclist') }}">User KYC List</a>
                  </li>
                  
               </ul>
       </li>


       <li class="child-menu">
               <a href="#"><span>Users Transaction</span> <span class="fa fa-angle-right toggle-right"></span>
               </a>
               <ul class="child-menu-ul">
                  <li>
                     <a href="{{url('admin-buytokentransaction')}}">Buy Token  </a>
                  </li>
                  <li>
                     <a href="{{url('admin-deposittransaction')}}">Deposit</a>
                  </li>
                  <li>
                     <a href="{{url('admin-withdrawaltransaction')}}">Withdrawal</a>
                  </li>
       <!--            <li>
                     <a href="{{url('admin-tokentransaction')}}">Transfer Token </a>
                  </li> -->
               </ul>
       </li>
              <li>
               <a href="{{url('admin-withdrawal')}}" class="{{{ (Request::is('admin-withdrawal') ? 'active ' : '') }}}">Withdrawal Request 
                @if($header_data['count'] != 0) <span class="badge badge-warning" style="float:right;font-size: 18px;border-radius: 16px;margin: -6px 0px 0px 0px;color:  #fff;padding-top: 3px;width:  28px;">{{ $header_data['count'] }}
               </span> @endif
               </a>
       </li>
       <li>
         <a href="{{url('admin-setting')}}" class="{{{ (Request::is('admin-setting') ? 'active ' : '') }}}">ICO Settings</a>
       </li>
       <li>
         <a href="{{url('admin-rates')}}" class="{{ (Request::is('admin-rates') ? 'active ' : '') }}">Phase Settings</a>
       </li>


   </ul>
  @else
   <ul>
       <li>
           <a href="{{ url('user/dashboard') }}" class="{{ (Request::is('user/dashboard') ? 'active ' : '') }}">Dashboard</a>
       </li>
       <li>
           <a href="{{ url('user/profile') }}" class="{{ (Request::is('user/profile') ? 'active ' : '') }}">Profile</a>
       </li>

       @if (config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5)
       <li class="child-menu">
          <a href="{{ url('user-walletInfo') }}" class="{{ (Request::is('user-walletInfo') ? 'active ' : '') }}"><span>Deposit</span>
          </a>
       </li>
       @endif

       @if (config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5)
       <li class="child-menu">
          <a href="{{ url('user-ico') }}" class="{{ (Request::is('user-ico') ? 'active ' : '') }}"><span>Buy Token</span>
              <!--<span class="fa fa-angle-right toggle-right"></span>-->
          </a>
          <!--<ul class="child-menu-ul">

            <li>
              <a href="{{ url('user-ico') }}">Buy Token</a>
            </li>
                 <li>
              <a href="{{ url('ico-info') }}" class="">ICO information</a>
            </li>
          </ul>-->
       </li>
       @endif

       @if (config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5 || config('common.timerConfigurations.current.groupId') == 0)
       <li class="child-menu">
           <a href="#"><span>Transaction</span> <span class="fa fa-angle-right toggle-right"></span>
           </a>
           <ul class="child-menu-ul">
              <li>
                 <a href="{{url('buytokentransaction')}}">Buy Token History </a>
              </li>
              <li>
                 <a href="{{url('deposittransaction')}}">Deposit History</a>
              </li>
              <!--<li>
                 <a href="{{url('withdrawaltransaction')}}">Withdrawal</a>
              </li>-->
             <!--  <li>
                 <a href="{{url('tokentransaction')}}">Transfer Token </a>
              </li> -->

           </ul>
       </li>
       @endif

        <li>
           <a href="{{url('user-referral')}}" class="{{{ (Request::is('user-referral') ? 'active ' : '') }}}">Referral List</a>
        </li>
        <li class="d-md-none d-lg-block">
            <a href="{{ url('logout') }}">Signout</a>
        </li>
   </ul>
  @endif
</div>