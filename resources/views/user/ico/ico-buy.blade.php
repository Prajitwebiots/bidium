@extends('layouts.master')
@section('title') Bidium Coin | Buy Token @endsection
@section('style')
<style type="text/css">
  
  .btn.disabled,
.btn:disabled {
    opacity: .65;
    cursor: not-allowed !important;
}

.badge-warning {
    color: #fafcff;
    background-color: #ffc107;
}

.alert-success {
    color: #155724;
    background-color: #d4edda;
    border: 1px solid #155724;
}
.alert-danger {
    color: #721c24;
    background-color: #f8d7da;
    border-color: #721c24;
}

</style>

@endsection
@section('content')
 <div class="dashboard-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">Buy BIDIUM token</h4>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
            </li>
            <li class="breadcrumb-item"><a href="#!">ICO</a>
            <li class="breadcrumb-item"><a href="#!">Buy Token</a>
            </li>
          </ol>
        </div>

<div class="col-lg-12">
                  <div class="card">
                    <div class="card-header">
                      <h4>Balance</h4>
                    </div>
                     <div class="card-body coin-value">
                        <div class="row">
        <div class="col-lg-4">
          <div class="media">
            <img class="mr-3" src="{{ url('assets/images/coin/btc.png') }}" alt="bitcoin">
            <div class="media-body">
              <h5 class="mt-2">
              BTC Balance</h5>
              <h4 class="blue-font mt-0 ">
                {{number_format(Sentinel::getUser()->total_btc_bal,8)}}
              </h4>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="media">
            <img class="mr-3" src="{{ url('assets/images/coin/eth.png') }}" alt="ETH">
            <div class="media-body">
              <h5 class="mt-2">
              ETH Balance</h5>
              <h4 class="blue-font mt-0 ">
              {{number_format(Sentinel::getUser()->total_eth_bal,8)}}
              </h4>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
                        <div class="media">
                        <img class="mr-3" src="{{ url('assets/images/$.png') }}" alt="dollar">
                            <div class="media-body">
                                <h5 class="mt-2">BIDM USD Rate</h5>
                                <h4 class="blue-font mt-0"><span class="counter">{{ $setting->rate }}</span></h4>
                            </div>
                        </div>
                    </div>

      </div>
                     </div>
                  </div>
               </div>



<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">BIDM Token</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->total_bidm_bal)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Token Bonus</h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->token_bonus)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Referral Bonus </h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->referral_bonus)}}</h2>
            </div>
        </div>
</div>
<div class="col-sm-3">
        <div class="card icon-timer">
            <div class="card-header">
                <h4 class="text-center">Whitelist Bonus </h4>
            </div>
            <div class="card-body p-5">
                <h2 class="counter text-center ico-font">{{number_format(Sentinel::getUser()->whitelist_bonus)}}</h2>
            </div>
        </div>
</div>


<div class="col-sm-12 mb-3">
  @if(Sentinel::getUser()->req_invest == 0)
  <form method="post" action="{{url('invest-request')}}">
  <input type="hidden" name="_token" value="{{csrf_token()}}">
  <div class="alert alert-custom" role="alert">
    <div class="row">
    <div class="col-sm-8">
      <h5 class="alert-heading">User Minimum investment 50 USD and Maximum Investment 10000 USD ,  For more than 10000 USD please contact us. </h5>
    </div>
    <div class="col-sm-4 text-right">
      <button class="btn btn-theme" type="submit" id="btnBuyAllBTC">Request</button>
    </div>
  </div>
  </div>
</form>
  @elseif(Sentinel::getUser()->req_invest == 1)
   <div class="alert alert-custom" role="alert">
    <div class="row">
    <div class="col-sm-8">
      <h5 class="alert-heading">Request Placed Successfully waiting for admin approval. </h5>
    </div>
  </div>
  </div>
   @elseif(Sentinel::getUser()->req_invest == 2)

   <div class="alert alert-custom alert-dismissible fade show" role="alert">
    <div class="row">
    <div class="col-sm-8">
      <h5 class="alert-heading">You Can invest more than 10000 USD. </h5>
    </div>
  </div>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

<!--    <div class="alert alert-custom" role="alert">
    <div class="row">
    <div class="col-sm-8">
      <h5 class="alert-heading">You Can invest more than 10000 USD. </h5>
    </div>
  </div>
  </div> -->
  @endif
  

</div>

<div class="col-sm-12">
@if(session('errorCoin'))<br><div class="alert alert-danger">{{ session('errorCoin') }}</div><br>@endif
@if(session('successCoin'))<br><div class="alert alert-success">{{ session('successCoin') }}</div><br>@endif
</div>
        <div class="col-sm-4">
        <div class="card">
          <div class="card-header">

       <div class="row">
          <div class="col-md-2">
            <h4>
                <img class="mr-3" src="{{ url('assets/images/coin/btc.png') }}" alt="bitcoin" style="width: 36px;background: #152e4a;border-radius: 5px;padding: 8px;">
            </h4>
          </div>
           <div class="col-md-5">
               <h4>Balance:</h4>
               {{number_format(Sentinel::getUser()->total_btc_bal,8)}}
           </div>
          <div class="col-md-5 mt-2 text-right">
            <button class="btn btn-theme" type="button" id="btnBuyAllBTC" @if(Sentinel::getUser()->total_btc_bal <= 0) disabled="" @endif>Buy All</button>
             <input type="hidden" name="buyall" value="0" id="buyallbtc">
          </div>
        </div>
           
          </div>
          <div class="card-body">
            <form class="form-horizontal theme-form" method="post" action="{{url('storeIco')}}">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group token_error">
                <label for="address">Units</label>
                <input type="text" class="form-control" autocomplete="off" name="tokenbtc" required="" id="NumberCoinBTC" onkeyup="return buywithBTC(this)" placeholder="0.00000000">
                <input type="hidden" class="form-control" name="bidmtoken" id="bidmtoken">
                <span id="errorBtc"></span>
              </div>

              <div class="form-group">
               <label for="address">Price</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                 <input type="text" class="form-control" autocomplete="off" placeholder="0.00000000" id="AmountBTC" type="text" value="" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">BTC</div>
              </div>
            </div>
             
             
              <div class="form-group">
               <label for="address">Bonus Tokens</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                 <input type="text" class="form-control" autocomplete="off" placeholder="0.00" id="bonus_bidm" type="text" value="0.00" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">BIDM</div>
              </div>
              <input type="hidden" class="form-control" name="bonus_bidmtkn" id="bonus_bidmtkn">
            </div>

             <div class="form-group">
               <label for="address">Total Tokens</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                 <input type="text" class="form-control" autocomplete="off" placeholder="0.00" id="total_bidm" type="text" value="0.00" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">BIDM</div>
              </div>
            </div>

        
          <div class="form-group text-left">
            <button name="type" type="submit" id="btnBuyCoinBtcBeh"  value="BTC" class="submit btn btn-theme"> Buy Coin </button>           
          </div>

        </form>
          </div>
        </div>
      </div>


      <div class="col-sm-4">
        <div class="card">
          <div class="card-header">
              <div class="row">
                  <div class="col-md-2">
                      <h4>
                          <img class="mr-3" src="{{ url('assets/images/coin/eth.png') }}" alt="bitcoin" style="width: 36px;background: #152e4a;border-radius: 5px;padding: 8px;">
                      </h4>
                  </div>
                  <div class="col-md-5">
                      <h4>Balance:</h4>
                      {{number_format(Sentinel::getUser()->total_eth_bal,8)}}
                  </div>
                  <div class="col-md-5 mt-2 text-right">
                      <button class="btn btn-theme" type="button" id="btnBuyAllETH" @if(Sentinel::getUser()->total_eth_bal <= 0) disabled="" @endif>Buy All</button>
                      <input type="hidden" name="buyall" value="0" id="buyalleth">
                  </div>
              </div>
          </div>
          <div class="card-body">
            <form class="form-horizontal theme-form" method="post" action="{{url('storeIco')}}">
             <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <label for="address">Units</label>
                 <input type="text" class="form-control"  autocomplete="off" name="tokeneth" id="NumberCoinETH" required="" onkeyup="return buywithETH(this)" placeholder="0.00000000">
             <input type="hidden" class="form-control" name="bidmtokeneth" id="bidmtokeneth">
             <span id="errorETH"></span>
              </div>

              <div class="form-group">
               <label for="address">Price</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
               <input type="text" class="form-control" autocomplete="off" id="AmountEth" placeholder="0.00000000" type="text" value="" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">ETH</div>
              </div>
            </div>

               <div class="form-group">
               <label for="address">Bonus Tokens</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                 <input type="text" class="form-control" autocomplete="off" placeholder="0.00" id="bonus_bidm_eth" type="text" value="0.00" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">BIDM</div>
              </div>
               <input type="hidden" class="form-control" name="bonus_bidmtkneth" id="bonus_bidmtkneth">
            </div>

             <div class="form-group">
               <label for="address">Total Tokens</label>
               <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                 <input type="text" class="form-control" autocomplete="off" placeholder="0.00" id="total_bidm_eth" type="text" value="0.00" disabled readonly>
                <div class="btn-theme lbl-info" disabled="disabled">BIDM</div>
              </div>
            </div>
          
          <div class="form-group text-left">
            <button name="type" id="btnBuyCoinEthBeh" type="submit"  value="ETH" class="submit btn btn-theme"> Buy Coin </button>
          </div>
        </form>
          </div>
        </div>
      </div>


      <div class="col-md-4">
  <div class="card cardheight394">
    <div class="card-header">
       <div class="col-md-12 ">
        <h4>Current Rate</h4>
      </div>
    </div>
    <div class="card-body theme-form table-responsive">
      <table class="table table-responsive">
        <tbody>
          <tr>
            <td><b><i class="fa fa-usd" aria-hidden="true"></i></b></td>
            <td>USD</td>
            <td>{{$usd_rate}}</td>
          </tr>
          <tr>
            <td><b><i class="fa fa-btc" aria-hidden="true"></i></b></td>
            <td>BTC</td>
            <td>{{number_format($usd_rate/$usdofbtc,8)}}</td>
          </tr>
          <tr>
            <td><b><img class="mr-3" src="{{ url('assets/images/icon-eth.png') }}" alt="ETH" style="
              height: 18px;
              margin-left: -5px;
            "></b></td>
            <td>ETH</td>
            <td>{{number_format($usd_rate/$usdofeth,8)}}</td>
          </tr>
          <tr>
            <td>1 ETH </td>
            <td>=</td>
            <td>{{$usdofeth}} USD </td>
          </tr>
          <tr>
            <td>1 BTC </td>
            <td>=</td>
            <td>{{$usdofbtc}} USD </td>
          </tr>
        </tbody>
    
    </table>

  </div>
</div>
</div>


</div>


<div class="row pt-4">
<div class="col-sm-12">
@if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
@if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
<div class="card">
  <div class="card-body table-responsive">
    <div class="col-md-12 ">
      <h4>Transaction History</h4>
    </div>
    <div class="mb-4"></div>
    <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
      <thead class="thead-dark">
        <tr>
          <th scope="col">Sr.</th>
          <th scope="col">User Name</th>
          <th scope="col">Amount</th>
          <th scope="col">Token</th>
          <th scope="col">Type</th>
          <th scope="col">Txid</th>
         <th scope="col">Status</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1;?>
        @foreach($wallets as $wallet)
        <tr>
          <td> {{$i++}} </td>
          <td> {{$wallet->user->username}} </td>
          <td> @if($wallet->amount){{ number_format($wallet->amount,8)}} @elseif($wallet->amount1) {{ number_format($wallet->amount1,2) }} @endif </td>
          <td> {{$wallet->tokens}} </td>
          <td>
            @if($wallet->type == 1) <span class="badge badge-warning"> BTC </span>
            @elseif($wallet->type == 2) <span class="badge badge-info"> ETH </span>
            @endif
          </td>
          <td> {{$wallet->txid}} </td>
          <td> <span class="badge badge-success"> Confirm </span> </td>
        </tr>
        @endforeach

      </tbody>
    </table>

  </div>
</div>
</div>
</div>





</div>


@endsection
@section('script')
<script type="text/javascript">
       var btc = 0;
       var eth = 0;
       var rate = 0;
       var buyALLBTC = 1;
       var buyALLETH = 1;
         btc = {{$btc}};
         eth = {{$eth}};
         rate = {{$usd_rate}};

      //setInterval("check_reserveprice()", 5000);
      function check_reserveprice() {
          $.ajax({
            type:"GET",
            url:"{{url('getPrice')}}",
                 success:function(res){
            if(res){
               btc = res['BTC'];
               eth = res['ETH'];
               rate = res['rate'];
               (buyALLBTC) ? $("#NumberCoinBTC").trigger('keyup') : $("#btnBuyAllBTC").trigger('click') ;
               (buyALLETH) ? $("#NumberCoinETH").trigger('keyup') : $("#btnBuyAllETH").trigger('click') ;
            } else {
               console.log('error');
           }
         }
       });
      }

    var btc_amount = {{Sentinel::getUser()->total_btc_bal}};
    var eth_amount = {{Sentinel::getUser()->total_eth_bal}};
    
    function buywithBTC(elem){
      $("#buyallbtc").val(0);
      buyALLBTC= 1;
      $(".token_error").removeClass('has-error');
            $("#errorBtc").html('');
      var val = $(elem).val();
      var bonus = $("#NumberCoinBTC").val()*{{$setting->bonus}}/100;
      $("#total_bidm").val( ($("#NumberCoinBTC").val()*1 + bonus).toFixed(2) );
      $("#bidmtoken").val( ($("#NumberCoinBTC").val()*1 + bonus).toFixed(0) );
      $("#bonus_bidm").val(bonus);
      $("#bonus_bidmtkn").val(bonus);
      $("#btnBuyCoinBtcBeh").prop('disabled',true);
      //console.log(val);
            if(isNaN(val)){
                 val = val.replace(/[^0-9\.]/g,'');
                 if(val.split('.').length>2)
                     val = val.replace(/\.+$/,"");
                 $(elem).val(val);
            }
            else{
              if($("#NumberCoinBTC").val() > 0){
                //console.log(btc_amount);
                if(btc_amount==0 ){
                  //console.log("0")
                  $(".token_error").addClass('has-error');
                      $("#errorBtc").html('<div class="text-danger"><b>You dont have sufficient BTC balance to buy !!</b></div>');
                  return false;
                }
                if($("#NumberCoinBTC").val() > 0){

                  var NumberCoinBTC = $("#NumberCoinBTC").val();
             
                  if(NumberCoinBTC <= 500 ){
                  //console.log("0")
                  $(".token_error").addClass('has-error');
                      $("#errorBtc").html('<div class="text-danger"><b>Minimum Investments 500 Tokens !!</b></div>');
                  return false;
                 }
                
                if({{Sentinel::getUser()->req_invest }} == 0 || {{Sentinel::getUser()->req_invest }} == 1) {
                if(NumberCoinBTC >= 10000000 ){
                  //console.log("0")
                  $(".token_error").addClass('has-error');
                      $("#errorBtc").html('<div class="text-danger"><b>Maximum Investments 10000000 Tokens !!</b></div>');
                  return false;
                }
              }

                 // console.log(NumberCoinBTC)
                  NumberCoinBTC = NumberCoinBTC * rate;
                 // console.log(rate)
                  var btc_total = (NumberCoinBTC/btc).toFixed(8);
                  $("#priceBTCToBEH").val((1/btc).toFixed(8));
                  $("#AmountBTC").val(btc_total);
                  if(btc_total>btc_amount){
                      $(".token_error").addClass('has-error');
                        $("#errorBtc").html('<div class="text-danger"><b>You dont have sufficient BTC balance to buy !!</b></div>');
                         $("#btnBuyCoinBtcBeh").prop('disabled',true);
                        return false;
                  }else{
                      $("#btnBuyCoinBtcBeh").prop('disabled',false);
                      //$("errorBtc").html('');
                  }
                }
                else{
                  $("#btnBuyCoinBtcBeh").prop('disabled',true);
                    $("#AmountBTC").val('0.00000000');
                    //$("errorBtc").html('');
                }
            }
            else{
                
            }
          }
        }

    function buywithETH(elem){
      $("#buyalleth").val(0);
      buyALLETH= 1;
      var val = $(elem).val();
      var bonus = $("#NumberCoinETH").val()*{{$setting->bonus}}/100;
      $("#total_bidm_eth").val( ($("#NumberCoinETH").val()*1 + bonus).toFixed(2) );
      $("#bidmtokeneth").val( ($("#NumberCoinETH").val()*1 + bonus).toFixed(0) );
      $("#bonus_bidm_eth").val(bonus);
      $("#bonus_bidmtkneth").val(bonus);
      $("#btnBuyCoinBtcBeh").prop('disabled',true);

      $(".token_errorETH").removeClass('has-error');
            $("#errorETH").html('');
      $("#btnBuyCoinEthBeh").prop('disabled',true);
            if(isNaN(val)){
                 val = val.replace(/[^0-9\.]/g,'');
                 if(val.split('.').length>2)
                     val = val.replace(/\.+$/,"");
                 $(elem).val(val);
            }
            else{
              if($("#NumberCoinETH").val() > 0){
                if(eth_amount==0){
                  console.log("1")
                  $(".token_errorETH").addClass('has-error');
                      $("#errorETH").html('<div class="text-danger"><b>You dont have sufficient ETH balance to buy !!</b></div>');
                  return false;
                }
                if($("#NumberCoinETH").val() > 0){
                  var NumberCoinETH = $("#NumberCoinETH").val();

                  if(NumberCoinETH <= 500 ){
                  $(".token_error").addClass('has-error');
                      $("#errorETH").html('<div class="text-danger"><b>Minimum Investments 500 Tokens !!</b></div>');
                  return false;
                  }
                  
                  if({{Sentinel::getUser()->req_invest }} == 0 || {{Sentinel::getUser()->req_invest }} == 1) {
                  if(NumberCoinETH >= 10000000 ){
                  $(".token_error").addClass('has-error');
                      $("#errorETH").html('<div class="text-danger"><b>Maximum Investments 10000000 Tokens !!</b></div>');
                  return false;
                  }
                }

                  NumberCoinETH = NumberCoinETH * rate;
                  var eth_total = (NumberCoinETH/eth).toFixed(8);
                  $("#priceEHToBEH").val((1/eth).toFixed(8));
                  $("#AmountEth").val(eth_total);
                  if(eth_total>eth_amount){
                      $(".token_errorETH").addClass('has-error');
                        $("#errorETH").html('<div class="text-danger"><b>You dont have sufficient ETH balance to buy !!</b></div>');
                        return false;
                  }else{
                      $("#btnBuyCoinEthBeh").prop('disabled',false);
                  }
                }
                else{
                  $("#btnBuyCoinEthBeh").prop('disabled',true);
                    $("#AmountEth").val('0.00000000');
                }
            }
          }
        }

        $("#btnBuyCoinBtcBeh").prop('disabled',true);
        $("#btnBuyCoinEthBeh").prop('disabled',true);

        $("#btnBuyAllBTC").click(function(){
          buyALLBTC = 0;
          var total_usd = btc * btc_amount;
          var tokens = (total_usd/rate);
          var bonustokens = tokens*{{$setting->bonus}}/100;
          var totaltok = tokens*1 + bonustokens;
        $("#priceBTCToBEH").val((1/btc).toFixed(8));
        $("#AmountBTC").val((btc_amount).toFixed(8));
        $("#bonus_bidm").val((bonustokens).toFixed(2));
        $("#bonus_bidmtkn").val((bonustokens).toFixed(2));
        $("#total_bidm").val((totaltok).toFixed(2));
        $("#bidmtoken").val((totaltok).toFixed(2));
        $("#NumberCoinBTC").val((tokens).toFixed(2));
        $("#buyallbtc").val(1);
        $("#btnBuyCoinBtcBeh").prop('disabled',false);

        });



        $("#btnBuyAllETH").click(function(){
          buyALLETH = 0;
          var total_usd = eth * eth_amount;
          var tokens = (total_usd/rate);
          var bonustokens = tokens*{{$setting->bonus}}/100;
          var totaltok = tokens*1 + bonustokens;
        $("#priceETHToBEH").val((1/eth).toFixed(8));
        $("#AmountEth").val((eth_amount).toFixed(8));
        $("#NumberCoinETH").val((tokens).toFixed(2));
        $("#bonus_bidm_eth").val((bonustokens).toFixed(2));
        $("#bonus_bidmtkneth").val((bonustokens).toFixed(2));
        $("#total_bidm_eth").val((totaltok).toFixed(2));
        $("#bidmtokeneth").val((totaltok).toFixed(2));
        $("#buyalleth").val(1);
        $("#btnBuyCoinEthBeh").prop('disabled',false);
        });

$(document).ready(function() {
$('#data-table').DataTable();
});

    </script>
	
@endsection