@extends('layouts.master')
@section('title') Bidium Coin | Ico Information @endsection
@section('style')
<style type="text/css">
a.btn.btn-success.btn-sm {
    color: #fff;
}
</style>
@endsection
@section('content')
<div class="dashboard-body">
      <div class="row">
        <div class="col-sm-12">
          <h4 class="page-title">ICO information</h4>
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fa fa-home" aria-hidden="true"></i></a>
            </li>
            <li class="breadcrumb-item"><a href="#">ICO</a>
              <li class="breadcrumb-item"><a href="#">ICO information</a>
              </li>
            </ol>
          </div>

          <div class="col-sm-6">
                  <div class="card icon-timer">
        <div class="card-header">
          <h4 class="text-center">ICO Start at:</h4>
        </div>
        <div class="card-body">
          <div class="timer-wrapper">
            <p id="timer-modal">
             
            </p>
          </div>
        </div>
      </div>

      
            <div class="card icon-timer">
             <div class="card-header">
              <h4 class="text-center">Total BIDM Token</h4>
            </div>
            <div class="card-body p-5">
             <h2 class=" text-center ico-font">
             	{{ number_format($setting->total_coins) }}
             </h2>
           </div>
         </div>

     </div>

     <div class="col-sm-6">
      <div class="card icon-timer">
        <div class="card-header">
          <h4 class="text-center">ICO End at</h4>
        </div>
        <div class="card-body">
          <div class="timer-wrapper">
            <p id="timer">

            </p>
          </div>
        </div>
      </div>

          <div class="card icon-timer">
           <div class="card-header">
            <h4 class="text-center">Sold BIDM</h4>
          </div>
          <div class="card-body p-5">
           <h2 class="counter text-center ico-font">
              {{ number_format($setting->sold_coins) }}
           </h2>
         </div>
       </div>



    </div>



</div>

</div>


@endsection
@section('script')

<script type="text/javascript">
        /* _____________________________________

             Count down
             _____________________________________ */
         	 var new_date = '{{ $setting->ico_end_date }}';
         	 var start_date = '{{ $setting->ico_start_date }}';

             //var countDownDate = new Date("Mar 5 2018 15:37:25").getTime();
             var countDownDate = new Date(new_date).getTime();
             var countDownDate1 = new Date(start_date).getTime();
        // Update the count down every 1 second
        var countdownfunction = setInterval(function() {

            // Get todays date and timer
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="timer"
            document.getElementById("timer").innerHTML = "<span>" + days + "<span class='timer-cal'>days</span></span> :" + "<span>" + hours + "<span class='timer-cal'>hours</span></span> :"
            + "<span>" + minutes + "<span class='timer-cal'>minutes</span></span> :" + "<span>" + seconds + "<span class='timer-cal'>seconds</span></span> ";

            // If the count down is over, write some text
            if (distance < 0) {
              clearInterval(countdownfunction);
              document.getElementById("timer").innerHTML = "EXPIRED";
            }
          }, 1000);

          var countdownfunction1 = setInterval(function() {

            // Get todays date and timer
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate1 - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);


            document.getElementById("timer-modal").innerHTML = "<span>" + days + "<span class='timer-cal'>days</span></span> :" + "<span>" + hours + "<span class='timer-cal'>hours</span></span> :"
            + "<span>" + minutes + "<span class='timer-cal'>minutes</span></span> :" + "<span>" + seconds + "<span class='timer-cal'>seconds</span></span> ";

            // If the count down is over, write some text
            if (distance < 0) {
              clearInterval(countdownfunction);
              document.getElementById("timer").innerHTML = "EXPIRED";
            }
          }, 1000);


      </script>

@endsection