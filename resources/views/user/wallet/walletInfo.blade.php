@extends('layouts.master')
@section('title') Bidium Coin | Wallet Information @endsection
@section('style')
<style type="text/css">
.btn-dark.disabled, .btn-dark:disabled {
    background-color: #343a40;
    border-color: #343a40;
    cursor: not-allowed;
}
</style>

@endsection
@section('content')
 <div class="dashboard-body">

  <div class="row">
    <div class="col-sm-12">
    <h4 class="page-title">Deposits</h4>
    <ol class="breadcrumb">
     <li class="breadcrumb-item"><a href="{{ url('user/dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i></a>
     </li>
     <li class="breadcrumb-item"><a href="#!">wallet</a>
     <li class="breadcrumb-item"><a href="#!">Deposits</a>
     </li>
   </ol>
 </div>
   <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h4>Deposits</h4>
      
      </div>
      <div class="card-body table-responsive">

       <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
         <thead class="thead-dark">
          <tr>
           <th scope="col">Currency</th>
           <th scope="col">Name</th>
           <th scope="col">Total Balance</th>
           <th scope="col">Action</th>
         </tr>
       </thead>
       <tbody>
        <tr>
         <th scope="row">
         	<img style="background: #152e4a;border-radius: 5px;padding: 8px;height: 50px;" src="{{ url('assets/images/coin/btc.png') }}" alt="BTC">
         </th>
         <td>BTC</td>
         <td>{{ $userData->total_btc_bal }} BTC</td>

         <td>
          <a href="{{url('user-Deposit/BTC')}}"> <button type="button" class="btn btn-outline-dark  btn-sm">Deposit</button></a>
         <!--<a href="{{url('user-Withdraw/BTC')}}" alt="fswdfswffdsrw"><button type="button" class="btn btn-dark  btn-sm" @if($withdrawal == 0) disabled  @endif>Withdraw</button></a>-->
         </td>
        </tr>

             <tr>
          <th scope="row">
            <img style="background: #152e4a;border-radius: 5px;padding: 8px;height: 50px;" src="{{ url('assets/images/coin/eth.png') }}" alt="ETH"></th>
            <td>ETH</td>
         <td>{{ $userData->total_eth_bal }} ETH</td>

         <td>
             <a href="{{url('user-Deposit/ETH')}}"> <button type="button" class="btn btn-outline-dark  btn-sm">Deposit</button></a>
             <!--<a href="{{url('user-Withdraw/ETH')}}"><button type="button"  class="btn btn-dark  btn-sm" @if($withdrawal == 0) disabled  @endif>Withdraw</button></a>-->
         </td>
       </tr>


       <!--<tr>
           <th scope="row">
           	<img style="background: #152e4a;border-radius: 5px;padding: 8px;height: 50px;"  src="{{ url('assets/images/coin/bidium.png') }}" alt="BIDM"></th>
           	<td>BIDIUM</td>
            <td>{{ $userData->total_bidm_bal }} BIDM</td>

          <td>
          <a href="{{url('user-Deposit/BIDIUM')}}""> <button type="button" class="btn btn-outline-dark  btn-sm ">Deposite</button></a>
          <a href="{{url('user-Withdraw/BIDM')}}"><button  type="button" class="btn btn-dark  btn-sm" @if($withdrawal == 0) disabled  @endif>Withdraw</button></a></td>
       
</td>
       </tr>-->
 
     </tbody>
   </table>

 </div>
</div>
</div>
</div>

</div>
@endsection
@section('script')

<script>
	$(document).ready(function() {
		$('#data-table').DataTable();
           $('#data-table-1').DataTable();
	});
</script>

@endsection