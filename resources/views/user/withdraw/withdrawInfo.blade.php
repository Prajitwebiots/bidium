@extends('layouts.master')
@section('title') Bidium Coin | User Withdrawal @endsection
@section('style')
<style type="text/css">
  .badge-warning {
    color: #fafcff;
    background-color: #ffc107;
}
</style>

@endsection
@section('content')
 <div class="dashboard-body">

  <div class="row">
    <div class="col-sm-12">
      <h4 class="page-title">Withdrawals</h4>
      <ol class="breadcrumb">
       <li class="breadcrumb-item"><a href="dashboard.html"><i class="fa fa-home" aria-hidden="true"></i></a>
       </li>
       <li class="breadcrumb-item"><a href="#!">wallet</a>
         <li class="breadcrumb-item"><a href="#!">Deposits & Withdrawals</a>
         <li class="breadcrumb-item"><a href="#!">Withdrawals</a>
         </li>
       </ol>
     </div>

<div class="col-sm-4">
  <div class="card">
    <div class="card-header">
      <h4>Withdrawal (@if($coin=='ETH')  {{number_format(Sentinel::getUser()->total_eth_bal,8)}} @elseif($coin=='BTC')   {{number_format(Sentinel::getUser()->total_btc_bal,8)}} @elseif($coin=='BIDM')   {{number_format(Sentinel::getUser()->total_bidm_bal,0)}} @endif{{$coin}})</h4>
    </div>
    <div class="card-body">

         <form class="form-horizontal theme-form" action="{{url('user-withdraw')}}" method="post">
             @if(session('error'))<br><div class="alert alert-danger">{{ session('error') }}</div><br>@endif
            @if(session('success'))<br><div class="alert alert-success">{{ session('success') }}</div><br>@endif
            <div id="alert-msg"></div>
            <input type="hidden" name="_token" value="{{csrf_token()}}" >
            <input type="hidden" name="coin_name" value="{{$coin}}" >
        <div class="form-group">
          <label for="address">wallet Address</label>
          <input type="text" @if($coin=='BIDM') value="{{Sentinel::getUser()->erc_20_address}}" @endif class="form-control" id="address" name="address_withdraw">
        </div>
        <div class="form-group">
          <label for="Quantity-wisdom coin">Quantity coin</label>
         <input type="number" class="form-control" id="amount-withdraw" name="amount_withdraw" placeholder="0.00" step="any" onkeyup="checkBalance()" >
        </div>
      <div class="form-group text-right">
        <button type="submit" class="btn btn-theme">Withdraw</button>
      </div>
    </form>
  </div>
</div>
</div>


<div class="col-lg-8">
                  <div class="card">
                    <div class="card-header">
                      <h4>Balance</h4>
                    </div>
                     <div class="card-body coin-value">
                        <div class="row">
        <div class="col-lg-4">
          <div class="media">
            <img class="mr-3" src="{{ url('assets/images/coin/btc.png') }}" alt="bitcoin">
            <div class="media-body">
              <h5 class="mt-2">
              BTC Balance</h5>
              <h4 class="blue-font mt-0 ">
                {{number_format(Sentinel::getUser()->total_btc_bal,8)}}
              </h4>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="media">
            <img class="mr-3" src="{{ url('assets/images/coin/eth.png') }}" alt="ETH">
            <div class="media-body">
              <h5 class="mt-2">
              ETH Balance</h5>
              <h4 class="blue-font mt-0 ">
              {{number_format(Sentinel::getUser()->total_eth_bal,8)}}
              </h4>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="media">
            <img class="mr-3" src="{{ url('assets/images/coin/bidium.png') }}" alt="ETH">
            <div class="media-body">
              <h5 class="mt-2">
              BIDM Balance</h5>
              <h4 class="blue-font mt-0">
             {{number_format(Sentinel::getUser()->total_bidm_bal)}}
              </h4>
            </div>
          </div>
        </div>


      </div>
                     </div>
                  </div>
               </div>

                  <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h4>List Withdrawal</h4>
        </div>
        <div class="card-body">
          <table id="data-table" class="table table-striped data-table" cellspacing="0" width="100%">
            <thead class="thead-dark">
              <tr>
                 <th scope="col">Id</th>
                 <th scope="col">Address</th>
                 <th scope="col">Txid</th>
                 <th scope="col">Status</th>
                 <th scope="col">Amount</th>
                 <th scope="col">Coin Type</th>
                
              </tr>
            </thead>
            <tbody>
            <?php $i=1; ?>
            @foreach($withdraws as $withdraw)
            
             <tr>
               <th scope="row">{{$i}}</th>
               <td>{{$withdraw->address}}</td>
               <td>{{$withdraw->txid}}</td>
               <td>@if($withdraw->admin_status == 0) 
               <span class="badge badge-warning"> Pending </span>
               @elseif($withdraw->admin_status == 1)
                <span class="badge badge-success"> Approved </span>
               @elseif($withdraw->admin_status == 2)
               <span class="badge badge-danger"> Rejected </span>
               @endif </td>
               <td>@if($withdraw->coin == 'BIDM') {{number_format($withdraw->amount,0)}} @else {{number_format($withdraw->amount,8)}} @endif </td>
               <td>@if($withdraw->coin == 'BTC')
               <span class="badge badge-warning"> BTC </span>
               @elseif($withdraw->coin == 'ETH')
               <span class="badge badge-info"> ETH </span>
                @elseif($withdraw->coin == 'BIDM')
               <span class="badge badge-success"> BIDM </span>
               @endif
               </td>
           
             </tr>
             
             <?php $i++; ?>
             @endforeach
            
            </tbody>
          </table>
        </div>
      </div>
    </div>








</div>

</div>

@endsection
@section('script')
<script>
$(document).ready(function() {
$('#data-table').DataTable();
 $("#Withdraw-btn").prop('disabled', true); 
});

function checkBalance()
{
  var coin='<?php echo $coin ?>';
  if(coin=='ETH'){ var balance= <?php echo Sentinel::getUser()->total_eth_bal ?>; }
  else if(coin=='BTC'){ var balance= <?php echo Sentinel::getUser()->total_btc_bal ?>; }
  else if(coin=='BIDM'){ var balance= <?php echo Sentinel::getUser()->total_bidm_bal ?>; }
  else{ var balance=0; }
  var amount= $("#amount-withdraw").val();
  if(amount<=balance && amount>0)
  {
     $("#Withdraw-btn").prop('disabled', false); 
      $("#alert-msg").html("");
  }
  else
  {
    $("#Withdraw-btn").prop('disabled', true); 
    $("#alert-msg").html("<p class='alert alert-danger'>Insuficient balance.</p>")
  }
}
</script>
<script>
	$(document).ready(function() {
		$('#data-table').DataTable();
           $('#data-table-1').DataTable();
	});
</script>

@endsection