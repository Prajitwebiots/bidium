<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
// Force SSL 
//Route::group(['middleware'=>'ForceSSL'], function(){

//Google 2fa authentication
Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
Route::post('/2fa/save', 'Google2FAController@saveSecretKey');
Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor');
Route::get('/2fa/validate', 'Auth\AuthController@getValidateToken');
Route::post('/2fa/validate', ['uses' => 'Auth\AuthController@postValidateToken']);
Route::post('/2fa/validate-disabletime', ['uses' => 'Auth\AuthController@postValidateTokenDesable']);
Route::post('2fa/validate-enabletime', ['uses' => 'Auth\AuthController@postValidateTokenenable']);

Route::get('check-address/{address}', 'RegisterController@isAddress');

/** Guest **/
Route::group(['middleware' => 'guest'], function () {

	Route::get('/', 'HomeController@index');

	Route::get('/info', function () {
	 	return view('home.info');
	 });

	Route::get('/register', 'RegisterController@showRegister');

	/* Login & register */
	Route::get('/login', 'LoginController@login');
	Route::post('doLogin', 'LoginController@doLogin');
	/* Register */
	Route::post('doRegister', 'RegisterController@doRegister');
	/* Account Activation */
	Route::get("activate/{email}/{activationCode}", 'RegisterController@activate');
	/* Forgot Password */
	Route::resource('forgotPassword', 'ForgotPasswordController');
	/* Reset Password */
	Route::get('reset/{email}/{code}', 'ForgotPasswordController@show');
	/* Referral set refer member */
	Route::get('ref/{refid}', 'RegisterController@referral');

	// whitelist activate by mail
	Route::get('changeUserStatusWhitelist/{id}/{status}', 'ProfileController@changeUserStatusWhitelist');
    
    // Check whitelist user
	Route::get('checklstUser/{address}', 'RegisterController@checklstUser');

});

/** Admin **/

Route::group(['middleware' => 'admin'], function () {

	/* Admin Manage profile */
	Route::get('admin/dashboard', 'DashboardController@dashboard');
	Route::get('admin/profile', 'AdminProfileController@index');
	Route::get('admin/profile', 'ProfileController@index');
	Route::post('admin/profile/{id}', 'ProfileController@updateProfile');
	Route::post('admin/profilePicture/{id}', 'ProfileController@updateProfilePicture');
	Route::post('admin/changePassword/{id}', 'ProfileController@updatePassword');
	Route::get('admin/kyclist', 'ProfileController@kyclist');
	Route::get('admin-kycUpdate/{id}/{status}', 'ProfileController@statusUpdate');

	/* Admin Manage User */
	Route::get('admin/users', 'AdminController@users');
	Route::post('changeUserStatus', 'ProfileController@changeUserStatus');
	Route::post('deleteUser', 'ProfileController@deleteUser');
	

	Route::get('admin/invRequest', 'AdminController@invRequest');
	Route::post('investmentStatus', 'AdminController@investmentStatus');


	/* Admin Manage User Transaction */
	Route::get('admin-transaction/{id}', 'AdminTransactionController@userTransaction');
	Route::get('admin-transaction/{id}', 'AdminTransactionController@userTransaction');
	Route::get('admin-buytokentransaction', 'AdminTransactionController@buyTokenTransaction');
	Route::get('admin-deposittransaction', 'AdminTransactionController@depositTransaction');
	Route::get('admin-withdrawaltransaction', 'AdminTransactionController@withdrawalTransaction');
	Route::get('admin-tokentransaction', 'AdminTransactionController@transferTokenTransaction');

	/* Admin Manage User Withdrawal */
	Route::get('admin-withdrawal', 'WithdrawController@requestWithdraw');
	Route::post('confirmStatus', 'WithdrawController@confirmStatus');
	Route::post('rejectStatus', 'WithdrawController@rejectStatus');

	/* Admin Manage User 2fa*/
	Route::post('disable2fa', 'ProfileController@disable2fa');

	/* Admin Manage Setting */
	Route::get('admin-setting', 'AdminController@settingShow');
	Route::get('admin-settingEdit/{id}', 'AdminController@settingEdit');
	Route::post('admin-storeSetting/{id}', 'AdminController@storeSetting');

	/* Admin Manage rate */
	Route::resource('admin-rates', 'RatesController');
	Route::PATCH('admin-rates/{id}', 'RatesController@update');

});

/** User **/
Route::group(['middleware' => 'user'], function () {

	/* User Manage profile */
	Route::get('user/dashboard', 'DashboardController@dashboard');
	Route::get('user/profile', 'ProfileController@index');
	Route::post('user/profile/{id}', 'ProfileController@updateProfile');
	Route::post('user/profilePicture/{id}', 'ProfileController@updateProfilePicture');
	Route::post('user/changePassword/{id}', 'ProfileController@updatePassword');
	Route::get('user/kyc', 'ProfileController@kyc');
	Route::post('kyc-form-upload', 'ProfileController@updateKyc');

	/* Refferal System */
	Route::get('user-referral', 'ReferralController@index');

	/* USer Ico */
	Route::resource('user-ico', 'IcoController');
	Route::get('ico-info', 'IcoController@icoInfo');
	Route::post('storeIco', 'IcoController@storeIco');
	Route::post('invest-request', 'IcoController@investRequest');

	/* User Wallet */
	Route::resource('user-walletInfo', 'WalletController');

	/* User Deposit */
	Route::get('user-Deposit/{coin}', 'DepositController@index');

	/* User Withdrawal */
	Route::get('user-Withdraw/{coin}', 'WithdrawController@index');
	Route::post('user-withdraw', 'WithdrawController@postWithdraw');

	/* User Transaction */
	Route::get('buytokentransaction', 'UserController@buyTokenTransaction');
	Route::get('deposittransaction', 'UserController@depositTransaction');
	Route::get('withdrawaltransaction', 'UserController@withdrawalTransaction');
	Route::get('tokentransaction', 'UserController@transferTokenTransaction');

});

/* Payment Coinpayment Api */
Route::get('payment', 'paymentController@index');
Route::post('checkstatus', 'PaymentController@check_status');
Route::post('ipn-handler', 'PaymentController@IpnHandler');
Route::post('getDepositAddress', 'PaymentController@depositAddress');

/* Logout session */
Route::get('logout', 'LoginController@logout');

//});