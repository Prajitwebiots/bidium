
 <!--Google font-->
 <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
 <!-- Font awesome -->
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
 <!-- Parallax.css-->
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/jarallax.css')); ?>">
 <!-- Bootstrap css -->
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/bootstrap.css')); ?>">
 <!-- BIDIUM coin css -->
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/style.css')); ?>">
 
 <!--responsive -->
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/admin-responsive.css')); ?>">
 
 <!--admin responsive -->
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/admin-responsive.css')); ?>">
 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/dataTables.bootstrap4.min.css')); ?>">
 <link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('assets/css/sweetalert.min.css')); ?>">

 <!-- Datepicker style -->
<link href="<?php echo e(url('assets/css')); ?>/tempusdominus-bootstrap-4.css" rel="stylesheet">
<!-- Datepicker style -->
 <?php echo $__env->yieldContent('style'); ?>

