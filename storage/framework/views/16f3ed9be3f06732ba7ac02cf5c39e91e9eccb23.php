<?php $__env->startSection('title'); ?> Bidium | An advance solution of trading,auction and freelance hiring <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style>
.home h3 {
    line-height: 35px;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<!-- Home slider -->
<div class="home">
  <!--  <video autoplay muted loop id="myVideo">
<source src="assets/images/backgroundICO.mp4" type="video/mp4">
</video> -->

   <div class="Section-Bg-Mask Section-Bg-Mask-Light">
      <div class="Section-Bg-R2 Section-Bg-R2-Light"></div>
      <div class="Section-Bg-R1 Section-Bg-R1-Light"></div>
      <div class="CircuitBoardLeft"></div>
      <div class="CircuitBoardRight"></div>
      <div class="main-rotatingimage"></div>
   </div>
   <div class="container home-contain">
      <div class="row">
         <div class="col-sm-12">
            <h1 class="main-theme-font mb-0">BIDIUM IS THE SOLUTION AT ONE PLATFORM</h1>
            <h3 class="main-theme-font main-sub-f mt-0">Combining the power of Advance Auction + Freelance hiring + Crypto Exchange / Trading on Blockchain Technology</h3>
         </div>
         <?php if(config('common.timerConfigurations.current.groupId') == 1): ?>
         <div class="col-md-12">
            <h3 class="freetoken mb-0">GET FREE TOKENS TO WHITELIST</h3>
            <h5 class="mt-0">(Refer and get 25% flat bonus on Referral's all purchased Tokens and Free Tokens)</h5>
         </div>
         <?php endif; ?>
         
            
               
               
            
         
         
            
               
               
            
         
         <?php if(config('common.timerConfigurations.current.groupId') == 0): ?>
            <div class="col-md-12">
               <?php if(config('common.timerConfigurations.hardCapReached')): ?>
                    <h3 class="freetoken mb-0">Thanks for your Contribution, Successfully 1 million USD Hardcap reached.  Stay in touch on telegram , facebook and twitter for updated Information.</h3>
               <?php else: ?>
                    <h3 class="freetoken mb-0">Thanks for your Contribution, Successfully 10 million USD Hardcap reached.  Stay in touch on telegram , facebook and twitter for updated Information.</h3>
               <?php endif; ?>
            </div>
         <?php endif; ?>
         <div class="col-xl-8 offset-xl-2 col-md-12">
            <div class="mt-3 icon-timer">
               <div class="">
                  <?php if(config('common.timerConfigurations.current.groupId') == 1 || config('common.timerConfigurations.current.groupId') == 9): ?>
                     <h4 class="card-title freetoken"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Pre-ICO will start in:</h4>
                  <?php endif; ?>
                  <?php if(config('common.timerConfigurations.current.groupId') == 2): ?>
                     <h4 class="card-title freetoken"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Pre-ICO started, 25% Bonus will End in:</h4>
                  <?php endif; ?>
                  <?php if(config('common.timerConfigurations.current.groupId') == 5): ?>
                     <h4 class="card-title freetoken"><!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->Get 15% Bonus , Pre-ICO will End in:</h4>
                  <?php endif; ?>
                  <div class="timer-wrapper mt-3">
                     <?php if(count(config('common.timerConfigurations.current.data')) > 0): ?>
                     <p id="timer" style="min-height: 80px;"></p>
                     <?php endif; ?>
                     <div>
                        <ul class="accepted-coins p-0 m-0">
                          <li>
                              <strong>Accept : </strong>
                          </li>
                          <li>
                              <img src="assets/images/icon/btc.png" alt="BTC">BTC ,
                          </li>
                          <li>
                              <img src="assets/images/icon/eth.png" alt="ETH"> ETH
                          </li>
                          <li>
                             &nbsp; &nbsp; (No USA and CHINA citizen)
                          </li>
                        </ul>
                     </div>
                     <div class="banner-rates text-center">
                        <ul class="rating-info p-0 m-0">
                           <li><img src="assets/images/icon/btc.png" alt="BTC">1 BTC = $ <?php echo e($setting->btc_price); ?></li>
                           <li><img src="assets/images/icon/eth.png" alt="ETH">1 ETH = $ <?php echo e($setting->eth_price); ?></li>
                           <li>
                           <li><img src="assets/images/icon/bidium.png"/>1 BIDM = $ <?php echo e($setting->rate); ?></li>
                           </li>
                        </ul>
                        
                     <!--   <ul class="rating-info p-0 m-0">
                           <li>Whitelist will end on 14th March.</li>
                          
                        </ul>-->
                         <?php if(config('common.timerConfigurations.current.groupId') == 1): ?>
                             <ul class="rating-info p-0 m-0">
                               <li>Whitelist will end on 14th March.</li>
                             </ul>
                         <?php endif; ?>
                         <?php if(config('common.timerConfigurations.current.groupId') == 9): ?>
                             <ul class="rating-info p-0 m-0">
                                 <li>Whitelist process is closed.</li>
                             </ul>
                         <?php endif; ?>
                     </div>

                  </div>
               </div>
            
               <!-- <div class="col-md-12 mt-4">
                 <div class="container ">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0" style="width:1%">
                        </div>
                    </div>
                    <div class="row progress-row">
                       <div class="col-6 text-left">$0</div>
                       <div class="col-6 text-right">$650,000,000</div>
                    </div>
                </div>
               </div> -->

               <!--Whitelist button-->
               <?php if(config('common.timerConfigurations.current.groupId') == 1): ?>
                  <div class="col-md-12 pb-3 text-center home-btn mt-3">
                     <a href="/register" class="btn btn-join mt-2">Join Whitelist</a>
                     <a href="#" class="btn btn-join  mt-2" data-toggle="modal" data-target="#whitelistmodal">Check Whitelist</a>
                  </div>
               <?php endif; ?>
               <?php if(config('common.timerConfigurations.current.groupId') == 2): ?>
                  <div class="col-md-12 pb-3 text-center home-btn mt-3">
                     <a href="/login" class="btn btn-join mt-2" style="width: auto !important;">Contribute Now with 25% Bonus</a>
                      <a href="#" class="btn btn-join  mt-2" data-toggle="modal" data-target="#whitelistmodal">Check Whitelist</a>
                  </div>
               <?php endif; ?>
               <?php if(config('common.timerConfigurations.current.groupId') == 5): ?>
                  <div class="col-md-12 pb-3 text-center home-btn mt-3">
                     <a href="/login" class="btn btn-join mt-2" style="width: auto !important;">Contribute Now with 15% Bonus</a>
                      <a href="#" class="btn btn-join  mt-2" data-toggle="modal" data-target="#whitelistmodal">Check Whitelist</a>
                  </div>
               <?php endif; ?>

               <div class="col-md-12 text-center home-btn">
               <?php $dt = date('Y-m-d'); if($dt >= '2018-03-15'){ ?>
                  <a href="#" class="btn btn-join">Buy token</a>
               <?php } ?>
               </div>


              <!-- progress bar for whitelist time -->
            <?php if(config('common.timerConfigurations.current.groupId') == 1): ?>
            <div class="mt-2">
                <div class="col-sm-12">
                  <h4 class="card-title freetoken font-weight-bold">FREE TOKENS DISTRIBUTION</h4>
                </div>
              <div class="col-sm-12 d-flex mb-1">
                <div class="first-step-ico" style="width:16.66%; ">
                  <span class="progress-total-coin">300 BIDM</span>
                </div>
                <div class="second-step-ico" style="width:25%; ">
                  <span class="progress-total-coin">200 BIDM</span>
                </div>
                <div class="third-step-ico" style="width:43.67%;">
                  <span class="progress-total-coin">100 BIDM</span>
                </div>
              </div>
              <div class="progress" id="progress1">
                
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($users * 100 / 100000); ?>%;"> 

                </div>
                <span class="progress-type" style="width:20%;"></span>
                <span class="progress-type" style="left: 25%; width: 25%;"> </span>
                <span class="progress-type" style="left: 57%; width:41.67%;"></span>
               
              </div>
              <div class="col-sm-12 d-flex mt-3 p-0">
                <div class="first-step-ico" style="width:16.66%; text-align: right;">
                  <span class="progress-total-text"><b>20000</b> Users</span>
                </div>
              <div class="second-step-ico" style="width:25%; text-align: right;">
                <span class="progress-total-text"><b>50000</b> Users</span>
              </div>
              <div class="third-step-ico" style="width:43.67%; text-align: right;">
                <span class="progress-total-text"><b>100000</b> Users</span>
              </div>
              </div>
            </div>
            <?php endif; ?>
            <!-- progress bar for whitelist time end-->


            <!-- progress bar for pre-sale time -->

             <?php if(config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5 || config('common.timerConfigurations.current.groupId') == 10): ?>
           
            <div class="mt-2">
                <div class="col-sm-12">
                </div>
              <div class="col-sm-12 d-flex mb-1">
                <div class="first-step-ico text-right" style="width:50%; ">
                  <span class="progress-total-coin">5,00,000 USD</span>
                </div>
                <div class="second-step-ico text-right" style="width:50%; ">
                  <span class="progress-total-coin">1 Million USD</span>
                </div>
                
              </div>
              <div class="progress" id="progress1">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($presalecoins); ?>%;"> 

                </div>
                <span class="progress-type" style="width:50%;"></span>
                <span class="progress-type" style="left: 48%; width: 50%;"> </span>
              </div>
              <div class="col-sm-12 d-flex mt-3 p-0">
                <div class="first-step-ico" style="width:50%; text-align: right;">
                  <span class="progress-total-text">SOFTCAP</span>
                </div>
              <div class="second-step-ico" style="width:50%; text-align: right;">
                <span class="progress-total-text">HARDCAP</span>
              </div>
              </div>
            </div>
           
            <!-- progress bar for pre-sale time end-->


            <!-- progress bar for ICO time -->
           <div class="mt-5">
                <div class="col-sm-12">
                </div>
              <div class="col-sm-12 d-flex mb-1">
                <div class="first-step-ico text-right" style="width:10%; ">
                  <span class="progress-total-coin">1M</span>
                </div>
                <div class="second-step-ico text-right" style="width:20%; ">
                  <span class="progress-total-coin">3M</span>
                </div>
                <div class="second-step-ico text-right" style="width:70%; ">
                  <span class="progress-total-coin">10M</span>
                </div>
                
              </div>
              <div class="progress" id="progress1">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo e($salecoins); ?>%;"> 

                </div>
                <span class="progress-type" style="width:21%;"></span>
                <span class="progress-type" style="left: 26%; width: 20%;"> </span>
                <span class="progress-type" style="left: 28%; width: 70%;"> </span>
              </div>
              <div class="col-sm-12 d-flex mt-3 p-0">
                <div class="first-step-ico" style="width:10%; text-align: right;">
                  <span class="progress-total-text"></span>
                </div>
              <div class="second-step-ico" style="width:20%; text-align: right;">
                <span class="progress-total-text">SOFTCAP</span>
              </div>
              <div class="second-step-ico" style="width:70%; text-align: right;">
                <span class="progress-total-text">HARDCAP</span>
              </div>
              </div>
            </div> 
            <!-- progress bar for ICO time end-->
 <?php endif; ?>


<br /> &nbsp; <br />&nbsp; <br />&nbsp; <br />&nbsp; <br />&nbsp; <br />

            </div>
         </div>
      </div>
   </div>
</div>

<!-- What is BIDIUM coin start-->
<section id="about-us" class="what-is ">

   <div class="container reveal-0 ">
      <div class="row token-details">
         <div class="col-lg-12 what-right pb-4">
            <div class="text-center">
               <h2 class="title text-white">What is <span>BIDIUM </span>?</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
         <div class="col-lg-5">
            <div class="about-img card">
            <!--   <iframe height="315" src="https://www.youtube.com/embed/ehjOL_bqzSc?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
            
               <video loop width="400" controls="" id="abtvideo">
                  <source src="assets/images/bidiumICO.mp4" type="video/mp4" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen>
                                    Your browser does not support HTML5 video.
                  </video>
            </div>
         </div>
         <div class="col-lg-7 ml-10">
            <p >Bidium, a decentralized bidding marketplace where buyers can purchase only by bidding with the aid of cyptocurrency exchange and a freelance platform where employers can hire freelancers with the aid of more than 10 coins which includes bidium, bitcoin, ethereum, litecoin, and others.</p>
            <p >In summary, our market structure is made up of networks of various technical devices that will give investors the opportunity of benefiting from a marketplace without centralized location.</p>
            <p >Bidium is sharing its profit with Bidium holders to give more benefits to its holders. Bidium token can be used for selling , purchasing, hiring, trading at 0 transaction fee</p>
            <h4 class="text-white p-2 m-1">TOKEN DETAILS</h4>
            <ul class="text-white BDM-about">
                <li> <strong>Name :</strong> Bidium</li>
                <li> <strong>Symbol :</strong> BIDM</li>
                <li> <strong>Decimal - 2</strong></li>
                <li> <strong>Total Supply :</strong> 1,000,000,000 BIDM</li>
                <li> <strong>Presale :</strong> 0.01 USD</li>
                <li> <strong>ICO : </strong> 0.02 USD </li>
                <li> <strong>Accept BTC and Eth</strong></li>
                <li> <strong>Min Purchase 50 USD and Max 10000 USD</strong></li>


            </ul>
         </div>
      </div>

         <!-- <div class="row justify-content-md-center mt-5">
         <div class="col-lg-3">
            <div class="card text-center">
               <div class="token-details-title">
                  <h2>1,000,000,000</h2>
                  <span>Total Supply</span>
               </div>
            </div>
         </div>
          <div class="col-lg-3">
            <div class="card text-center">
               <div class="token-details-title">
               <h2>0.01 USD</h2>
               <span>Presale</span>
            </div>
            </div>
         </div>
         <div class="col-lg-3">
            <div class="card text-center">
            <div class="token-details-title">               
               <h2>0.02USD</h2>
               <span>ICO</span>
            </div>
            </div>
         </div>
         </div> -->
   </div>
</section>
<!-- What is BIDIUM coin end-->

<!-- About us start-->
<!--<section class="about-us background-main">
  <div class="container">
      <div class="row">
         <div class="col-lg-12 reveal-left">
            <div class="text-center">
               <h2 class="title text-center text-white">About <span>Us </span>?</h2>
               <span class="titleline"><em></em></span>
               <!--<h3 class="text-center ">WE ARE THE LEADERS IN THE CRYPTO INDUSTRY!</h3>-/->
            </div>
         </div>
         <div class="col-lg-12 reveal-0 text-center">
            <h3 class="text-center text-white mb-2">What is BIDIUM coin (BIDM)?</h3>
            <p class="text-white mb-4">BIDIUM coin  is a revolutionary crypto currency platform and BIDIUM coins are the main currency of this platform. It is all based of the Ethereum ecosystem and just like Ethereum and many other crypto currencies, we have developed this new coin on the BlockChain with the aim to create a new era of banking. BIDIUM coin  allows for a self managed, decentralised financial ecosystem with peer to peer BlockChain transactions.</p>
            <h3 class="text-center text-white mb-2">What can I do with BIDIUM coin (BIDM)?</h3>
            <p class="text-white mb-4">With our platform you can send money in the form of BIDIUM coins to anywhere in the world within a matter of seconds, provided there is a internet connection. BIDIUM coin  platform also offers lending programs, BIDM/BTC exchange, BIDM/ETH exchange, BIDM mining (PoW), and staking (PoS).
            </p>
         </div>
      </div>
   </div>
</section>-->
<!-- About us end-->
<!-- Why BIDIUM coin start-->
<section  class="services">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">Why BIDIUM</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
      </div>
      <div class="row service-wrapper">
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/fast.png">
               <h3>Decentralization</h3>
               <h6>BIDIUM plans to be completely decentralized by utilizing already existing blockchain application. All data and multimedia uploads will be stored on the blockchain. Cost Effective ServiceAs compared to other payment services that require huge procedures and huge pocket cut, the BIDIUM token is cost effective and minimizes time wastage. This is because the platform is created in a very fast and efficient blockchain network. You will only be charged little or no amount thereby making you deploy a payment service of your own.</h6>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/down.png"> 
               <h3>Functionality</h3>
               <h6>Transactions will be done by the customer at their preferred time in the marketplace; Cryptocurrency can also be trading or exchanged on Bidium Exchange. The BIDIUM token can be transferred to two other parties as payment for goods and services after bids for freelance service. The amount of user's balance will calculated at BIDM equivalent to their local currency . The total system is produced and can be controlled at one place.</h6>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/private.png">
               <h3>Cost Effective Service</h3>
               <h6>As compared to other payment services that require huge procedures and huge pocket cut, the BIDIUM token is cost effective and minimizes time wastage. This is because the platform is created in a very fast and efficient blockchain network. You will be charged 0 Transaction fee between the Bidium Platforms and unbeatable service fee to use the outstanding Bidium service.</h6>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/secure.png">
               <h3>Immediatate Payment</h3>
               <h6>This is another unique  feature that is associated with BIDIUM platform. It enables the transfer of payment to the sellers , freelancers and traders without any chargeback issue and ontime  with the aid of smart contracts that are regulated. There are certain procedures that will aid you to keep the address of the receiver in mind. The BIDIUM ensures the transfer without any problem. </h6>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/simple.png">
               <h3>Wallet</h3>
               <h6>BIDIUM uses integrated mobile wallet which is basically for the receiving and sending of payments. This shows that you will be able to transfer funds for payment of goods and services in the bid marketplace in a secured and improved manner without requiring any document or possessing a bank account. BIDIUM token account wallet will be able to hold standard tokens. BIDIUM also provides offline PaperWallet where token can be stored securely.</h6>
            </div>
         </div>
         <div class="col-lg-4 col-md-6 reveal-0">
            <div class="text-center service-block">
               <img src="assets/images/service/reliable.png">
               <h3>High Scope</h3>
               <h6>More than 10 million auction sellers, 1 million traders, 53 million Freelancers across the globe and they are paying higher fees to get the outstanding services. Bidium brings a platform to increase more opportunity without any more hassle.</h6>
            </div>
         </div>
      </div>
   </div>
</section>

<!-- how work starts -->

<section id="opportunity" class="opportunity reveal-0">
    <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="">
                  <div class="text-center">
                     <h2 class="title">How It Works ?</h2>
                     <span class="titleline"><em></em></span>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="text-center">
                  <div class="tab-content" id="pills-tabContent">
                      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                           <div class="opportunity-wrapper">
                               <div id="cntr-img-cntnt-display">
                                   <img src="assets/images/Wallet.ico" alt="#" class="center-img" />
                                   <h2 class="mt-2">Wallet</h2>
                                   <h5>Web, Mobile and Offline PaperWallet</h5>
                               </div>
                               <div class="how-work-content">
                               </div>
                           </div>
                           <div class="">
                               <ul class="investment-service">
                                   <li>
                                       <div class="how-w-circle rounded-circle">
                                          <img src="assets/images/how/auction.png" alt="#"/>
                                          <h5>Auction</h5>
                                          <div class="exchng-list">
                                             <p class="exchng-list-items">Max 24 hours turn around.</p>
                                          </div>
                                          <div class="work-cont">
                                              <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Max 24 hours term around.</p>
                                              <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Genuine Byuers, No fake bidding</p>
                                              <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Ads Revenue sharing 50% to jeller and 30% to buyer</p>
                                          </div>
                                       </div>
                                   </li>
                                   <li>
                                       <h5 class="zero-tran-fee">Zero transaction fee</h5>
                                   </li>
                                   <li>
                                       <div class="how-w-circle rounded-circle" id="exchange">
                                          <img src="assets/images/how/bitcoin.png" alt="#"/>
                                          <h5>Exchange</h5>
                                          <div class="exchng-list">
                                             <p class="exchng-list-items">Lowest trading for at 0.01% Only</p>
                                          </div>
                                          <div class="work-cont">
                                             <p class="exchng-list-items"> <i class="fa fa-check" aria-hidden="true"></i> Lowest trading for at 0.01% Only</p>
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> BIDM Holders will get 50% distuilation of trading for profit sharing</p>
                                               <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> prices and money excitements</p>
                                               <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Web / mobile apps</p>
                                          </div>
                                       </div>
                                   </li>
                                   <li>
                                       <h5 class="zero-tran-fee">Zero transaction fee</h5>
                                   </li>
                                   <li>
                                       <div class="how-w-circle rounded-circle" id="freelance">
                                           <img src="assets/images/how/freelanc.png" alt="#"/>
                                          <h5>Freelance</h5>
                                          <div class="exchng-list">
                                             <p class="exchng-list-items">Service Fee is 0% For employee and 0.1%...</p>
                                          </div>
                                          <div class="work-cont">
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Service fee is 0% for employee and only 0.1% for freelancers.</p>
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Escrow</p>
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Reviews and ratings</p>
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Freelancers public Profiles</p>
                                             <p class="exchng-list-items"><i class="fa fa-check" aria-hidden="true"></i> Web / Mobile apss.</p>
                                          </div>
                                       </div>
                                   </li>
                                   <li>
                                       <h5 class="zero-tran-fee">Zero transaction fee</h5>
                                   </li>
                               </ul>
                          </div>
                      </div>
                  </div>
               </div>
            </div>
         </div>
    </div>
</section>

<!-- how work ends -->
<!-- 

<section class="">
   <div class="container reveal-0">
      <div class="row">
         <div class="col-lg-12 what-right pb-4">
            <div class="text-center">
               <h2 class="title">How it <span>Works </span>?</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
         <div class="col-lg-12 text-center">
            <img src="assets/images/home/how-it-works.png" class="img-fluid">
         </div>
         
      </div>
   </div>
</section> -->

<section class="faq">
   <div class="container reveal-0">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">FAQ</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>How to contribute in Bidium?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <div class="card-body">
               <ul>
               
              <li> Step-1 : Register and get Whitelist at Bidium </li>
   <li>Step 1: Login and Deposit BTC or ETH into your Bidium Wallet</li>
    <li>Step 3: Click on Buy Token to buy token</li>
                 
               </ul>
            </div>
         </div>
      </div>
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>Is there any limit on the number purchase?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
            
            <li> The contribution is limited to min 50USD and max 10000USD. </li>
            <li>However you can purchase as many times as 

you want but total sum of purchase amount should not exceed more than 10000USD. </li>
<li>To purchase more than 

10000USD , you need to request approval.</li>
               
            </ul>
         </div>
      </div>
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>May I have multiple account with same ETH address?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
               <li>No, One ETH address can be used only in one account.
</li>
              
            </ul>
         </div>
      </div>
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>Is there any country restriction to contribute in Pre-ICO or in ICO?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
               <li>Yes, USA and China Citizen can't participate in Bidium Pre-ICO and ICO Phase. </li>
               <li>But they can purchase 

token from exchange after launch.</li>
            </ul>
         </div>
      </div>
      
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>How many free tokens I will get at the time of whitelist?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
            
            <li> First 20000 users will get 300 tokens , next 30000 users will get 200 tokens and next 50000 users 

will get 100 tokens</li>
           
               
            </ul>
         </div>
      </div><div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>Does ICO Price increase after each phase?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
            
            <li>Yes, Token Price in Pre-ICO phase is only 0.01USD and in ICO phase is 0.02USD 
  </li>
            <li>However after every week of sale bonus will get decrease <br />
Pre-ICO 1st week - 25% bonus <br />
Pre-ICO 2nd week - 15% bonus <br/>
ICO 1st week - 10% bonus <br />
ICO 2nd week - 5% bonus </li>

               
            </ul>
         </div>
      </div>
      
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>After contibuting in Pre-ICO and ICO phase when can I trade the BIDM Token?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
            
            <li> Bidium Token can be tradable right after ICO END on Bidium Exchange.
 </li>
            <li>However whitelist free tokens will be released only user do first succesful trade on Bidium Exchange. </li>

               
            </ul>
         </div>
      </div>
      
      <div class="col-sm-12 col-xs-12 panel">
         <div class="question">
            <span>How to get ETH Address?</span>
            <span class="material-icons"><i class="fa fa-angle-down"></i></span>
         </div>
         <div class="answer">
            <ul>
            
            <li> You can create account in MEW (MyEtherWallet) or in MetaMask to get ETH address. </li>
            <li>For details <a href="#" target="_blank">Check 

here</a>.</li>

               
            </ul>
         </div>
      </div>
      
   </div>
</section>


<section class="ico-token" id="ICO">
   <div class="container reveal-0">
      <div class="row">
         <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="text-center">
               <h2 class="title">TOKENS Summary</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
         <div class="col-md-6 col-sm-12 col-xs-12 text-center mt-5">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="text-center">
                     <h2 class="title">TOKENS ALLOCATION</h2>
                  </div>
               </div>
            </div>
           <div class="row">
              <div class="responsiveChart col-md-12 col-sm-12 col-xs-12" style="max-width: 100% !important;overflow-x: hidden !important;">
                 <main class="mainChart">
                    <section class="secChart p-0">
                       <div class="pieID2 pie"></div>
                       <ul class="chartM pieID2 legend mbox-shadow">
                          <li>
                             <em>Crowdsale</em>
                             <span>80</span>
                          </li>
                          <li>
                             <em>Bounty</em>
                             <span>5</span>
                          </li>
                          <li>
                             <em>Advisor </em>
                             <span>2</span>
                          </li>
                          <li>
                             <em>Founder (Locked for 1 year)</em>
                             <span>3</span>
                          </li>
                          <li>
                             <em>Reserve (Every year 2% will dilute)</em>
                             <span>10</span>
                          </li>
                       </ul>
                    </section>
                 </main>
              </div>
            </div>
         </div>

         <div class="col-md-6 col-sm-12 col-xs-12 text-center mt-5">
            <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="text-center">
                     <h2 class="title">FUNDS ALLOCATION</h2>
                  </div>
               </div>
            </div>
           <div class="row">
              <div class="responsiveChart col-md-12 col-sm-12 col-xs-12" style="max-width: 100% !important;overflow-x: hidden !important;">
                 <main class="mainChart">
                    <section class="secChart p-0">
                       <div class="pieID3 pie"></div>
                       <ul class="chartM pieID3 legend mbox-shadow">
                          <li data-name="Development">
                             <em>Development</em>
                             <span>45</span>
                          </li>
                          <li>
                             <em>Marketing</em>
                             <span>30</span>
                          </li>
                          <li>
                             <em>Legal</em>
                             <span>5</span>
                          </li>
                          <li>
                             <em>Reserve &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em>
                             <span>15</span>
                          </li>
                          <li>
                             <em>Management</em>
                             <span>5</span>
                          </li>
                       </ul>
                    </section>
                 </main>
              </div>
            </div>
         </div>
      </div>
   </div>
</section>


<section id="road-map" class="road-map ">
   <div class="container reveal-0">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">Road <span>Map</span></h2>
            </div>
         </div>
         <div class="col-md-12 mt-4">
            <div class="main-timeline">
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">01</span></div>
                  <div class="timeline-content">
                     <h3 class="title">DEC 2017</h3>
                     <p class="description">
                        STARTED WORKING
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">02</span></div>
                  <div class="timeline-content">
                     <h3 class="title">JAN 2018</h3>
                     <p class="description">
                        CREATED DESIGN &<br>
                        STARTED DEVELOPMENT
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">03</span></div>
                  <div class="timeline-content">
                     <h3 class="title">FEB 2018</h3>
                     <p class="description">
                        WHITELIST
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">04</span></div>
                  <div class="timeline-content">
                     <h3 class="title">MARCH 2018</h3>
                     <p class="description">
                        PRE-SALE
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">05</span></div>
                  <div class="timeline-content">
                     <h3 class="title">APRIL 2018</h3>
                     <p class="description">
                        SALES
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">06</span></div>
                  <div class="timeline-content">
                     <h3 class="title">MAY 2018</h3>
                     <p class="description">
                        EXCHANGE LAUNCH
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">07</span></div>
                  <div class="timeline-content">
                     <h3 class="title">JUNE 2018</h3>
                     <p class="description">
                        EXCHANGE APP LAUNCH
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">08</span></div>
                  <div class="timeline-content">
                     <h3 class="title">AUGUST 2018</h3>
                     <p class="description">
                        FREELANCE PLATFORM LAUNCH
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">09</span></div>
                  <div class="timeline-content">
                     <h3 class="title">SEPTEMBER 2018</h3>
                     <p class="description">
                        FREELANCE APP LAUNCH
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">10</span></div>
                  <div class="timeline-content">
                     <h3 class="title">Q4 2018</h3>
                     <p class="description">
                        E-BID MARKETPLACE LAUNCH
                     </p>
                  </div>
               </div>
               <div class="timeline">
                  <div class="timeline-icon"><span class="year">11</span></div>
                  <div class="timeline-content">
                     <h3 class="title">Q4 2018</h3>
                     <p class="description">
                        E-BID MARKETPLACE APP LAUNCH
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- echo system -->


<section class="documents" id="documents">
   <div class="container reveal-0">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">DOCUMENTS</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
       
            <div class="col-lg-4 col-md-6">
               <a href="<?php echo e(URL::asset('assets/documents/whitepaper.pdf')); ?>" target="blank" class="document-bdm">
                  <div class="box_feat">
                     <img src="assets/images/icons/whitepaper.png" class="img-fluid" alt="">
                     <h3>WHITEPAPER</h3>
                  </div>
               </a>
            </div>
            <div class="col-lg-4 col-md-6">
               <a href="#" target="blank" class="document-bdm">
                  <div class="box_feat">
                     <img src="assets/images/icons/token-sale-agreement.png" class="img-fluid" alt="">
                     <h3>AGREEMENT</h3>
                  </div>
               </a>
            </div>
            <div class="col-lg-4 col-md-6">
               <a href="#" target="blank" class="document-bdm">
                  <div class="box_feat">
                     <img src="assets/images/icons/disclaimer.png" class="img-fluid" alt="">
                     <h3>DISCLAIMER</h3>
                  </div>
               </a>
            </div>
            
         

      </div>
   </div>
</section>


<section class="team section" id="team">
    <div class="container reveal-0">
        <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">OUR TEAM</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
        <div class="row">
            <div class="col-12 col-md-4 col-sm-6">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/member1.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                        <h3>Nausheen Ahmad</h3>
                        <span>CEO</span>
                        <p>Bachelor in Computer Science Engineering. Has more than 8 years of work experience in IT field, web development and managing team.</p>                       
                        <a href="https://www.linkedin.com/in/nausheen-ahmad/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
                </div>
            </div>
         
            <div class="col-12 col-md-4 col-sm-6">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/member2.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                           <h3>John Shipmen</h3>
                        <span>Blockchain Developer</span>
                        <p>He has more than 9 years of work experience in IT field and have free hand on blockchain technology to fly any project.</p>
                        <a href="https://www.linkedin.com/in/john-shipmen-89419613b"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
             <div class="col-12 col-md-4 col-sm-6">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/member3.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                           <h3>Walo Boni</h3>
                        <span>Developer</span>
                        <p>Bachelor of Applied Science. Has more than 7 years of experience in web and mobile application, blockchain and cryptocurrency technologies. </p>
                        <a href="https://www.linkedin.com/in/walo-boni-08a8a1146"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
           
        </div>
    </div>
</section>


<section class="team section" id="team">
    <div class="container reveal-0">
        <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">OUR ADVISOR</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
        <div class="row">
            <div class="col-12 col-md-3 col-sm-3">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/team1.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                        <h3>Vladimir Nikitin</h3>
                        <span>Head of Advisory Board</span>
                        <p>Master of Law, Master of Economics (Finance and Credit). Has experience in the field of Civil law, finance, Internet technologies for more than 10 years. For more than 2 years he is active member of the crypto community and is an active supporter of the promotion of blockchain, he has an extensive network of contacts in the crypto community (over 30 000 in LinkedIn) . Advisor more than 10 ICO-projects, Expert on ICObench.</p>                       
                        <a href="https://www.linkedin.com/in/cryptoexpertnikitin/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 col-sm-3">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/team2.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                         <h3>Nikolay Shkilev</h3>
                        <span>Advisor</span>
                        <p>Crypto enthusiast and mentor.

Has 20 years of experience in large-scale transaction projects. He has many awards and titles in the IT business. Self-Made Russia award. Tech guru. Super TOP award etc. Founder and CEO of Private Business Club. His Holding received "Enterprise of the Year" award in the Kremlin. Has a business in various directions. Crypto enthusiast and mentor.</p>
                        <a href="https://www.linkedin.com/in/icoadvisor/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-3 col-sm-3">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/team3.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                           <h3>Anna Shkilyova</h3>
                        <span>Marketing Advisor</span>
                        <p>ICO advisor and blockchain enthusiast. 
Crypto investor. Always searching for new and innovative ways to help businesses meet their goals.
TroubleShooter. 
Linkedin Expert.
Has experience in promoting brands, launching marketing companies. 
"Top ICO Advisors" expert</p>
                        <a href="https://www.linkedin.com/in/anna-shkilyova-478a54158/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
            <div class="col-12 col-md-3 col-sm-3">
                <div class="team-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid" src="assets/images/team/team4.jpg" alt="team people">
                    </div>
                     <div class="member-content">
                         <h3>Erickvand Tampilang</h3>
                        <span>Bounty Advisor</span>
                        <p>An Advisor who is very professional and plays an important role in this project. He is also being an Advisor on some startup projects and has a huge influence on them. He also became an Ambassador on one of the largest startup projects that are now growing rapidly.
He also has experience as a highly professional campaign manager, with a distribution of over 8000+ participants.</p>
                        <a href="https://www.linkedin.com/in/erickvand-tampilang-6b9524149/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
           
        </div>
    </div>
</section>

<!--
<section class="team section " id="team">
    <div class="container reveal-0">
        <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title">OUR ADVISORS</h2>
               <span class="titleline"><em></em></span>
            </div>
         </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="expert-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid rounded-circle" src="assets/images/team/member-1.jpg" alt="team people">
                    </div>
                     <div class="member-content txt-blck">
                        <h3>Vladimir Nikitin</h3>
                        <span>Head of Advisory Board</span>
                        <a href="https://www.linkedin.com/in/cryptoexpertnikitin/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="expert-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid rounded-circle" src="assets/images/team/member-2.jpg" alt="team people">
                    </div>
                     <div class="member-content txt-blck">
                        <h3>Nikolay Shkilev</h3>
                        <span>Advisor</span>
                        <a href="https://www.linkedin.com/in/icoadvisor/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="expert-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid rounded-circle" src="assets/images/team/member-3.jpg" alt="team people">
                    </div>
                     <div class="member-content txt-blck">
                        <h3>Anna Shkilyova</h3>
                        <span>Marketing Advisor</span>
                        <a href="https://www.linkedin.com/in/anna-shkilyova-478a54158/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
            <div class="col-sm-3">
                <div class="expert-member text-center">
                    <div class="member-photo">
                        <img class="img-fluid rounded-circle" src="assets/images/team/member-4.jpg" alt="team people">
                    </div>
                     <div class="member-content txt-blck">
                        <h3>Erickvand Tampilang</h3>
                        <span>Bounty Advisor</span>
                      <a href="https://www.linkedin.com/in/erickvand-tampilang-6b9524149/"><i class="fa fa-linkedin-square"></i></a>
                    </div>
               </div>
            </div>
           
        </div>
    </div>
</section>
-->


<!-- Investment opportunity Start-->
<!--<section class="invest-opprt gray-bg background-main">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title text-white">BIDIUM coin <span>Investment Opportunity</span></h2>
            </div>
         </div>
      </div>
      <div class="row mt-4">
         <div class="col-sm-12">
            <div class="tabs">
               <div class="container">
                  <div class="row">
                     <div class="col-xl-4">
                        <ul class="nav nav-pills nav-stacked flex-column tabnav-invest">
                           <li class="active tabli1"><a href="#tab_a" data-toggle="pill">BIDIUM coin Lending</a></li>
                           <li class="tabli1"><a href="#tab_b" data-toggle="pill">BIDIUM coin Trading</a></li>
                           <li class="tabli1"><a href="#tab_c" data-toggle="pill">BIDIUM coin Mining</a></li>
                        </ul>
                     </div>
                     <div class="col-xl-6">
                        <div class="tab-content">
                           <div class="tab-pane active" id="tab_a">
                              <h3>BIDIUM coin Lending</h3>
                              <ul class="invest-list">
                                 <li>
                                    <img src="assets/images/icons/deposit.png">
                                    <div class="invest-details">
                                       <h5>Deposit</h5>
                                       <h6>deposit BIDIUM coin  from below options</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/logo-2.png">
                                    <div class="invest-details">
                                       <h5>Buy BIDIUM coin </h5>
                                       <h6>Buy BIDIUM coin  from WDC exchange</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/deal.png">
                                    <div class="invest-details">
                                       <h5>Lend BIDIUM coin </h5>
                                       <h6>Lend or Invest WDC in BIDIUM Lending</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/percentage.png">
                                    <div class="invest-details">
                                       <h5>Trading Profit</h5>
                                       <h6>You can buy WDC at a lower price and selling them at higher price</h6>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="tab-pane" id="tab_b">
                              <h3>BIDIUM coin Trading</h3>
                              <ul class="invest-list">
                                 <li>
                                    <img src="assets/images/icons/deposit.png">
                                    <div class="invest-details">
                                       <h5>Deposit</h5>
                                       <h6>deposit BIDIUM coin  from below options</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/logo-2.png">
                                    <div class="invest-details">
                                       <h5>Buy BIDIUM coin </h5>
                                       <h6>Buy BIDIUM coin  from WDC exchange</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/earn.png">
                                    <div class="invest-details">
                                       <h5>Trading</h5>
                                       <h6>Buy & Sell BIDIUM Coin from WDC exchange</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/money.png">
                                    <div class="invest-details">
                                       <h5>Trading Profit</h5>
                                       <h6>You can buy WDC at a lower price and selling them at higher price</h6>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="tab-pane" id="tab_c">
                              <h3>BIDIUM coin Mining</h3>
                              <ul class="invest-list">
                                 <li>
                                    <img src="assets/images/icons/petroleum.png">
                                    <div class="invest-details">
                                       <h5>Hardware</h5>
                                       <h6>BIDIUM coin can be mined With CPU/GPU</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/businessman.png">
                                    <div class="invest-details">
                                       <h5>Download Miner</h5>
                                       <h6>Download miner for mining BIDIUM Coin</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/auction.png">
                                    <div class="invest-details">
                                       <h5>BIDIUM Coin Mining</h5>
                                       <h6>Start BIDIUM Coin Mining (Solo mining & Pool mining)</h6>
                                    </div>
                                 </li>
                                 <li>
                                    <img src="assets/images/icons/gold-ingots.png">
                                    <div class="invest-details">
                                       <h5>Earn Reward</h5>
                                       <h6>You will get reward to mine (PoW) Block and earn new BIDIUM Coin</h6>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>-->
<!--<section class="choose_bitcoin_area p_100 background-main">
   <div class="container">
      <div class="col-sm-12">
         <div class="text-center">
            <h2 class="title">BIDIUM Coin Ecosystem</h2>
         </div>
      </div>
      <div class="row choose_bit_inner">
         <div class="col-lg-6 col-md-6">
            <div class="choose_botcoin_item">
               <img src="assets/images/icons/ent.png">
               <h4>Entrpreneur</h4>
               <p>Offer you the most convenient method payment when it comes to making payments regarding business expenses including wages, payment for suppliers, for goods and services... all over the word, within second, with an extremely low fee, and free you from complicated banking procedures.</p>
            </div>
         </div>
         <div class="col-lg-6 col-md-6">
            <div class="choose_botcoin_item">
               <img src="assets/images/icons/online-business.png">
               <h4>Online Business And Retails Stores</h4>
               <p>You can boost your sale and speed up your business growth thanks to BIDIUM and our related apps on smart phone. You and your customers cannot ask for more. From this moment, the most advanced yet simple method is in your hand.</p>
            </div>
         </div>
         <div class="col-lg-6 col-md-6">
            <div class="choose_botcoin_item">
               <img src="assets/images/icons/cash.png">
               <h4>NO More Cash</h4>
               <p>There is no more limit to carry foreign exchange to anywhere you go with BIDIUM . No more worry about the exchange rate fees that you may be charged a lot, and many other expenses related to money exchange and transfer. Therefore, save your time, money, and of course, the less cash, the safer</p>
            </div>
         </div>
         <div class="col-lg-6 col-md-6">
            <div class="choose_botcoin_item">
               <img src="assets/images/icons/daily-transaction.png">
               <h4>Other Daily Money Transaction</h4>
               <p>With BIDIUM, just after seconds, you can access your money transferred from anyone anywhere on this planet. Faster, more simple, safer but cheaper.</p>
            </div>
         </div>
      </div>
   </div>
</section>-->
<!-- end echo system -->
<!--<section>
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="text-center">
               <h2 class="title pb-4">Lending Program </h2>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-3 col-sm-3">
            <div class="pricingTable green">
               <h3 class="title"><span>LANDING AMOUNT</span></h3>
               <ul class="pricing-content">
                  <li><i class="fa fa-check"></i> $50 - $5,000</li>
                  <li><i class="fa fa-check"></i> $5,001 - $20,000</li>
                  <li><i class="fa fa-check"></i> $20,001 - $100,000</li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-sm-3">
            <div class="pricingTable green">
               <h3 class="title"><span>GUARANTEE</span></h3>
               <ul class="pricing-content">
                  <li><i class="fa fa-check"></i>01.0% - 1.4%</li>
                  <li><i class="fa fa-check"></i> 1.2% - 1.8%</li>
                  <li><i class="fa fa-check"></i> 1.4% - 2.0%</li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-sm-3">
            <div class="pricingTable green">
               <h3 class="title"><span>UP TO</span></h3>
               <ul class="pricing-content">
                  <li><i class="fa fa-check"></i> 40%</li>
                  <li><i class="fa fa-check"></i> 45%</li>
                  <li><i class="fa fa-check"></i> 50%</li>
               </ul>
            </div>
         </div>
         <div class="col-md-3 col-sm-3">
            <div class="pricingTable green">
               <h3 class="title"><span>DURATION</span></h3>
               <ul class="pricing-content">
                  <li><i class="fa fa-check"></i> 180 days</li>
                  <li><i class="fa fa-check"></i> 150 days</li>
                  <li><i class="fa fa-check"></i> 120 days</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<section id="news">
   <div class="section section-pad">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="text-center">
                  <h2 class="title ">Latest news</h2>
               </div>
            </div>
         </div>
         <div class="gaps size-3x"></div>
         <div class="row text-center">
            <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 res-m-bttm-lg">
               <div class="blog-post abox-shadow shadow round">
                  <div class="post-thumb pt-2"><a href="#"><img src="assets/images/news/1.jpg" alt="post"></a></div>
                  <div class="post-entry">
                     <div class="post-meta"><span>Posted 03 Dec, 2017</span></div>
                     <h5><a href="#">Working Hard to Keep Pace with very heigh Demand</a></h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidi dunt ut labore.</p>
                     <a class="more_btn" href="#">Learn More</a>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 res-m-bttm-lg">
               <div class="blog-post abox-shadow shadow round">
                  <div class="post-thumb pt-2"><a href="#"><img src="assets/images/news/2.jpg" alt="post"></a></div>
                  <div class="post-entry">
                     <div class="post-meta"><span>Posted 03 Dec, 2017</span></div>
                     <h5><a href="#">Black Friday: Bitcoins the biggest deal on from today</a></h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidi dunt ut labore.</p>
                     <a class="more_btn" href="#">Learn More</a>
                  </div>
               </div>
            </div>
            <div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2 res-m-bttm-lg">
               <div class="blog-post abox-shadow shadow round">
                  <div class="post-thumb pt-2"><a href="#"><img src="assets/images/news/3.jpg" alt="post"></a></div>
                  <div class="post-entry">
                     <div class="post-meta"><span>Posted 03 Dec, 2017</span></div>
                     <h5><a href="#">Introducing our new payment services...</a></h5>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incidi dunt ut labore.</p>
                     <a class="more_btn" href="#">Learn More</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>-->
<!-- Investment opportunity Ends-->




  
<!-- Floating Social Media bar Starts -->
<div class="float-sm">
  <div class="fl-fl float-fb">
    <i class="fa fa-facebook"></i>
    <a href="https://www.facebook.com/bidium" target="_blank" class="folw"> Like us!</a>
  </div>
  <div class="fl-fl float-tw">
    <i class="fa fa-twitter"></i>
    <a href="https://twitter.com/bidiumofficial" target="_blank" class="folw">Follow us!</a>
  </div>
  <div class="fl-fl float-gp">
    <i class="fa fa-telegram"></i>
    <a href="https://t.me/Bidiumofficialgroup" target="_blank" class="folw">Connect</a>
  </div>
  
</div>
<!-- Floating Social Media bar Ends -->

<!-- check whitelist modal box-->
<div class="modal fade" id="whitelistmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Check Whitelist</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="login-form col-sm-12">
            <div class="col-sm-12">
              <form id="" class="tab-content active"> 
                <div class="form-group form-row">
                  <input type="text" name="address" value="" placeholder="Enter ETH wallet address" class="form-control" id="erc20" autocomplete="off">
                  <span class="help-block text-danger" id="message">
                        
                      </span>
                </div>
                <div class="text-center">
                <button type="button" onClick="checklstUser()" class="btn btn-login text-center">Check</button>
              </div>
              </form>
            </div>
          </div>
      </div>
      
    </div>
  </div>
</div>
<!-- -->


<!-- Tap on Top -->
<!-- Home slider end -->
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer-scripts'); ?>
<?php if(count(config('common.timerConfigurations.current.data')) > 0): ?>
<script>
   /* _____________________________________
   
   Count down
   _____________________________________ */

   var countDownDate = moment("<?php echo e(config('common.timerConfigurations.current.data.timerDate')); ?>", "YYYY-MM-DD HH:mm:ss").valueOf();
   //var countDownDate = new Date("<?php echo e($setting->ico_start_date); ?>").getTime();

   // Update the count down every 1 second
   var countdownfunction = setInterval(function() {
   
       // Get todays date and time
       var now = new Date().getTime();
       
       // Find the distance between now an the count down date
       var distance = countDownDate - now;
       
       // Time calculations for days, hours, minutes and seconds
       var days = Math.floor(distance / (1000 * 60 * 60 * 24));
       var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
       var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
       var seconds = Math.floor((distance % (1000 * 60)) / 1000);
       
       // Output the result in an element with id="timer"
       document.getElementById("timer").innerHTML = "<span>" + days + "<span class='timer-cal'>days</span></span>" + "<span>" + hours + "<span class='timer-cal'>hours</span></span> "
       + "<span>" + minutes + "<span class='timer-cal'>minutes</span></span> " + "<span>" + seconds + "<span class='timer-cal'>second</span></span> ";
      
       
       // If the count down is over, write some text 
       if (distance < 0) {
         clearInterval(countdownfunction);
         document.getElementById("timer").innerHTML = "EXPIRED";
         location.reload();
      }
   }, 1000);

</script>
<?php endif; ?>
<script>
   $(document).ready(function(){

      // Add smooth scrolling to all links
      $("nav a").on('click', function(event) {

         // Make sure this.hash has a value before overriding default behavior
         if (this.hash !== "") {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
            scrollTop: $(hash).offset().top
            }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
            });
         } // End if
      });
   });
</script>
<script>
   $(document).ready(function() {

      $('.faq .question').on('click', function () {
         $(this).parents('.panel').toggleClass('open').siblings().removeClass('open');
         $(this).siblings('.answer').slideToggle(225);
         $(this).parents('.panel').siblings().find('.answer').slideUp(225);
      });
   
   });
</script>
<script type="text/javascript">
   $(document).ready(function() {
   $('.tabnav-invest li').on('click', function () {
       $('.tabnav-invest li').removeClass("active");
       $(this).addClass("active");
   });
   });
</script>
<script type="text/javascript">
   (function() {
    var selectors = {
        nav: '[data-features-nav]',
        tabs: '[data-features-tabs]',
        active: '.__active'
    }
    var classes = {
        active: '__active'
    }
    $('a', selectors.nav).on('click', function() {
        let $this = $(this)[0];
        $(selectors.active, selectors.nav).removeClass(classes.active);
        $($this).addClass(classes.active);
        $('div', selectors.tabs).removeClass(classes.active);
        $($this.hash, selectors.tabs).addClass(classes.active);
        return false
    });
}());

$(".btn-with-icon").on("click", function() {
    $(".wave-anim").addClass('visible').one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd", function() {
        $(".wave-anim").removeClass('visible');
    });
});
</script>
<script type="text/javascript">
  window.sr = ScrollReveal();

// Customizing a reveal set
sr.reveal('.reveal-0', { duration: 500 ,delay : 100 ,rotate: { x: 0, y: 0, z: 0 }});
</script>
<script type="text/javascript">
window.onload = function() {
        var audioPlayer = document.getElementById("abtvideo");
        audioPlayer.load();
        audioPlayer.play();
    };
    </script>
<!--
<script type="text/javascript">
  $( "#auction" ).mouseover(function() {
      $("#cntr-img-cntnt-display").css("opacity","0");
      $("#exchange-cont").removeClass("showhoverinfo");
      $("#freelance-cont").removeClass("showhoverinfo");
        $("#auction-cont").addClass("showhoverinfo");
  });
  
  $( "#exchange" ).mouseover(function() {
      $("#cntr-img-cntnt-display").css("opacity","0");
      $("#auction-cont").removeClass("showhoverinfo");
      $("#freelance-cont").removeClass("showhoverinfo");
        $("#exchange-cont").addClass("showhoverinfo");
  });
  
  $( "#freelance" ).mouseover(function() {
      $("#cntr-img-cntnt-display").css( "opacity","0" );
        $("#auction-cont").removeClass("showhoverinfo");
        $("#exchange-cont").removeClass("showhoverinfo");
        $( "#freelance-cont" ).addClass("showhoverinfo");
      
  });

</script>
-->

<script type="text/javascript">
  // check whitelist user
  function checklstUser()
    {
      var address = $('#erc20').val();

      $.get("checklstUser/"+address, function(data, status){
        $('#message').empty();
           if(data == 1)
           {
              $('#message').removeClass();
              $('#message').addClass('help-block text-success');
              $('#message').append('<strong> This Address is Whitelisted </strong>')
           }else{
              $('#message').removeClass();
              $('#message').addClass('help-block text-danger');
              $('#message').append('<strong> This Address Not Whitelisted Now </strong>')
           }
      });  
    }

</script>

<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.masterHome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>