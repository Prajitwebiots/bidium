<?php $__env->startSection('title'); ?> Bidium Coin | Profile  <?php $__env->stopSection(); ?>
<?php $__env->startSection('style'); ?>
<style type="text/css">
    .secret{
        color:#000;
    }
.google_2fa {
    border: 1px solid #333 !important;
}
.disabled, *:disabled {
    opacity: .65;
    cursor: not-allowed;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
// check admin or user
$slug = Sentinel::getUser()->roles()->first()->slug;
?>
<div class="dashboard-body">
  <div class="row">
    <div class="col-sm-12">
      <h4 class="page-title">Profile</h4>

      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a <?php if($slug == "2"): ?> href="<?php echo e(url('user/dashboard')); ?>" <?php else: ?> href="<?php echo e(url('admin/dashboard')); ?>" <?php endif; ?>><i class="fa fa-home" aria-hidden="true"></i></a>
        </li>
        <li class="breadcrumb-item"><a <?php if($slug == "2"): ?> href="<?php echo e(url('user/profile')); ?>" <?php else: ?> href="<?php echo e(url('admin/profile')); ?>" <?php endif; ?>>Profile</a>
        </li>
      </ol>
    </div>
    <div class="col-md-3">
      <div class="card">
        <div class="card-body">
          <?php if($errors->has('first_name')): ?>
          <span class="help-block text-danger">
            <strong><?php echo e($errors->first('photo')); ?></strong>
          </span>
          <?php endif; ?>
          <?php if($slug == '1'): ?>
          <form  enctype="multipart/form-data" action="<?php echo e(url('admin/profilePicture')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
            <?php else: ?>
            <form  enctype="multipart/form-data" action="<?php echo e(url('user/profilePicture')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
              <?php endif; ?>
              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
              <div class="profile text-center">
                <div class="p-relative">

                  <img src="<?php echo e(url('/assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" />

                  <div class="image-upload">
                    <label for="file-input">
                      <div class="profile-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div>
                    </label>
                    <input id="file-input" type="file" name="photo" accept="image/x-png,image/gif,image/jpeg" />
                  </div>
                </div>
                <div>
                  <button type="submit" class="btn btn-theme mt-4">Update Profile</button>
                </div>

                <!-- <img src="../assets/images/avtar.png" />-->
                <h3 class="mt-3 mb-4 blue-font"><?php echo e(Sentinel::getUser()->first_name); ?></h3>
                <h4><?php echo e(Sentinel::getUser()->last_name); ?></h4>
                  <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec sagittis mauris. Donec mollis nunc eu est maximus eleifend. In ac malesuada nisl.</p> -->
                </div>
              </form>

           </form>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="card">
      <div class="card-body">
        <ul class="nav nav-pills nav-fill theme-tab">
          <li class="nav-item">
            <a class="nav-link <?php if(!session('validator')): ?> active <?php endif; ?>" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile setting</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="two-fact-tab" data-toggle="tab" href="#two-fact" role="tab" aria-controls="two-fact" aria-selected="false">Two factor Auth</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if(session('validator')): ?> active <?php endif; ?>" id="chg-pwd-tab" data-toggle="tab" href="#chg-pwd" role="tab" aria-controls="chg-pwd" aria-selected="false">Change password</a>
          </li>
          <?php if($slug == "2" && false): ?>
          <li class="nav-item">
            <a class="nav-link " id="chg-pwd-tab" data-toggle="tab" href="#kyc" role="tab" aria-controls="chg-pwd" aria-selected="false">Upload KYC</a>
          </li>
          <?php endif; ?>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade <?php if(!session('validator')): ?>  show active <?php endif; ?>" id="profile" role="tabpanel" aria-labelledby="profile-tab">

            <?php if($errors->any()): ?>
              <div class="alert alert-danger">
                  <ul>
                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <li><?php echo e($error); ?></li>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </ul>
              </div>
             <?php endif; ?>
             <div class="col-md-12">


              <?php if(session('error')): ?><br><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
              <?php if(session('success')): ?><br><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
              <?php if($slug == '1'): ?>
              <form class="form-horizontal theme-form mt-5 row"  action="<?php echo e(url('admin/profile')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
                <?php else: ?>
                <form class="form-horizontal theme-form mt-5 row"  action="<?php echo e(url('user/profile')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
                  <?php endif; ?>
                  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                  <input type="hidden" name="user_id" value="<?php echo e(Sentinel::getUser()->id); ?>">
                  <div class="form-group col-md-6">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" name="first_name" value="<?php echo e(Sentinel::getUser()->first_name); ?>" id="first_name" autocomplete="off">
                    <?php if($errors->has('first_name')): ?>
                    <span class="help-block text-danger">
                      <strong><?php echo e($errors->first('first_name')); ?></strong>
                    </span>
                    <?php endif; ?>

                  </div>

                  <div class="form-group col-md-6">
                    <label for="last_name">Last Name</label>
                    <input type="text" class="form-control" name="last_name" value="<?php echo e(Sentinel::getUser()->last_name); ?>" id="last_name" autocomplete="off">
                    <?php if($errors->has('last_name')): ?>
                    <span class="help-block text-danger">
                      <strong><?php echo e($errors->first('last_name')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                    <div class="form-group col-md-6">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" disabled value="<?php echo e(Sentinel::getUser()->username); ?>" id="username" autocomplete="off">
                        <?php if($errors->has('username')): ?>
                            <span class="help-block text-danger">
                      <strong><?php echo e($errors->first('username')); ?></strong>
                    </span>
                        <?php endif; ?>
                    </div>
                  <div class="form-group col-md-6">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" disabled name="email" disabled  value="<?php echo e(Sentinel::getUser()->email); ?>" id="email" autocomplete="off">
                    <?php if($errors->has('email')): ?>
                    <span class="help-block text-danger">
                      <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="erc_20_address">ERC_20 Address</label>
                    <input type="text" class="form-control"  disabled name="erc_20_address"   value="<?php echo e(Sentinel::getUser()->erc_20_address); ?>" id="erc_20_address" autocomplete="off">
                    <?php if($errors->has('erc_20_address')): ?>
                      <span class="help-block text-danger">
                      <strong><?php echo e($errors->first('erc_20_address')); ?></strong>
                    </span>
                    <?php endif; ?>
                  </div>
                  <div class="form-group col-md-12 text-right">
                    <button type="submit" class="btn btn-theme mt-4">Update</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="tab-pane fade mt-5" id="two-fact" role="tabpanel" aria-labelledby="two-fact-tab">
              <div class="row two-fact">
                <div class="col-md-12 text-center">
                  <h3>Enlable Google Authenticator</h3>
                  <?php if(Sentinel::getUser()->google2fa_enable==1): ?>
                         <button id="otpDisableToggleButton"  class="btn btn-warning mb-4" style="width: 200px;">Disable 2FA</button>
                    <?php else: ?>
                        <button id="otpToggleButton" class="btn btn-theme mb-4" style="width: 200px;">Enable 2FA</button>
                    <?php endif; ?>
                </div>
                <div class="col-md-6 offset-md-3">
                  <ul class="install-step">
                    <li>1.Install Google Authenticator on your phone.</li>
                    <li>2.Open the Google Authenticator app.</li>
                    <li>3.Tab menu, then tab "Set up Account", then "Scan a barcode" or "Enter key provided" is <strong class="colors">3KQD7ED2B5A3CX3M</strong></li>
                    <li>4.Your phone will now be in "scanning" mode. When you are in this mode, scan the barcode below:</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="tab-pane fade <?php if(session('validator')): ?>  show active <?php endif; ?> " id="chg-pwd" role="tabpanel" aria-labelledby="chg-pwd-tab">
              <div class="col-md-12">
                <?php if(session('error')): ?><br><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
                <?php if(session('success')): ?><br><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>
                <?php if($slug == '1'): ?>
                <form class="form-vertical theme-form mt-5 row col-md-12" action="<?php echo e(url('admin/changePassword')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
                  <?php else: ?>
                  <form class="form-vertical theme-form mt-5 row col-md-12" action="<?php echo e(url('user/changePassword')); ?>/<?php echo e(Sentinel::getUser()->id); ?>" method="post">
                    <?php endif; ?>

                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="user_id" value="<?php echo e(Sentinel::getUser()->id); ?>">
                    <div class="form-group col-md-12">
                      <label>Old password</label>
                      <input type="password" name="old_password" class="form-control" autocomplete="off">
                      <?php if($errors->has('old_password')): ?>
                      <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('old_password')); ?></strong>
                      </span>
                      <?php endif; ?>
                    </div>
                    <div class="form-group col-md-12">
                      <label>New password</label>
                      <input type="password" name="new_password" class="form-control" autocomplete="off">
                      <?php if($errors->has('new_password')): ?>
                      <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('new_password')); ?></strong>
                      </span>
                      <?php endif; ?>
                    </div>
                    <div class="form-group col-md-12">
                      <label>Retype password</label>
                      <input type="password" name="confirm_password" class="form-control" autocomplete="off">
                      <?php if($errors->has('confirm_password')): ?>
                      <span class="help-block text-danger">
                        <strong><?php echo e($errors->first('confirm_password')); ?></strong>
                      </span>
                      <?php endif; ?>
                    </div>
                    <div class="form-group col-md-12 text-right">
                      <button type="submit" class="btn btn-theme mt-4">Change</button>
                    </div>
                  </form>
                </div>
              </div>


        <?php if($slug == "2"): ?> 
              <div class="tab-pane fade mt-5" id="kyc" role="tabpanel" aria-labelledby="two-fact-tab">
                <div class="row two-fact">
                  <div class="col-md-12">

                  <?php if(session('error')): ?><br><br><div class="alert alert-danger"><?php echo e(session('error')); ?></div><?php endif; ?>
                  <?php if(session('success')): ?><br><br><div class="alert alert-success"><?php echo e(session('success')); ?></div><?php endif; ?>

                    <form class="form-horizontal theme-form mt-5 row"  action="<?php echo e(url('kyc-form-upload')); ?>" method="post" enctype= "multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="user_id" value="<?php echo e(Sentinel::getUser()->id); ?>">

                    <div class="container">
                      <?php

$userData = Sentinel::getUser();

?>

                    <?php if($userData->kyc_nat_id_front && $userData->kyc_nat_id_back && $userData->kyc_selfie_id): ?>
                      <?php if($userData->kyc_status == 0): ?>
                        <div class="col-md-6">
                          <div class="form-group">
                              <h3><span class="badge badge-warning">Kyc Status is pending !</span></h3>
                          </div>
                        </div>
                        <?php elseif($userData->kyc_status == 1): ?>
                        <div class="col-md-6">
                          <div class="form-group">
                              <h3><span class="badge badge-success">Kyc Status is Approved !</span></h3>
                          </div>
                        </div>
                        <?php elseif($userData->kyc_status == 2): ?>
                        <div class="col-md-6">
                          <div class="form-group">
                              <h3><span class="badge badge-danger">Kyc Status is Rejected !</span></h3>
                          </div>
                        </div>
                        <?php endif; ?>
                    
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="form-group">
                                <label>National ID front picture</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            <img style="width: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/<?php echo e($userData->kyc_nat_id_front); ?>">
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                                <label>National ID back picture</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                          <img style="width: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/<?php echo e($userData->kyc_nat_id_back); ?>">
                                        </span>
                                    </span>
                                </div>
                            </div>
                          </div>
                          <div class="col-sm-4">
                            <div class="form-group">
                                <label>Selfie with National ID</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                          <img style="width: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/<?php echo e($userData->kyc_selfie_id); ?>">
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                      </div>
                         <?php else: ?>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label>National ID front picture</label>
                              <div class="input-group">
                                  <span class="input-group-btn">
                                      <span class="btn btn-default btn-file">
                                          Browse… <input type="file" id="first_id_proof" name="first_id_proof">
                                      </span>
                                  </span>
                              </div>
                              <div class="text-center" style="text-align: center;">
                                Example
                                  <!-- i class="fa fa-long-arrow-down" style="font-size:48px;"></i -->
                            </div>
                            <div class="text-center">
                               <img  style="height: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/front.png ">
                             </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label>National ID back picture</label>
                              <div class="input-group pb-4">
                                  <span class="input-group-btn">
                                      <span class="btn btn-default btn-file">
                                          Browse… <input type="file" id="second_id_proof" name="second_id_proof">
                                      </span>
                                  </span>
                              </div>
                            <div class="text-center pb-1" style="text-align: center;">
                                Example
                                  <!-- i class="fa fa-long-arrow-down" style="font-size:48px;"></i -->
                            </div>
                            <div class="text-center">
                               <img  style="height: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/back.png ">
                             </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                        
                          <div class="form-group">
                              <label>Selfie with National ID</label>
                              <div class="input-group-btn pb-4">
                                  <span class="input-group-btn">
                                      <span class="btn btn-default btn-file">
                                          Browse… <input type="file" id="third_id_proof" name="third_id_proof">
                                      </span>
                                  </span>
                              </div>
                            <div class="text-center" style="text-align: center;">
                                Example
                                  <!-- i class="fa fa-long-arrow-down" style="font-size:48px;"></i -->
                            </div>
                            <div class="text-center">
                               <img  style="height: 200px;" src="<?php echo e(url('assets/images/user/kyc')); ?>/id_selfie.png ">
                             </div>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group">
                            <div class="input-group pb-4">
                                  <span class="input-group-btn">
                                      <span class="btn btn-default btn-file">
                                <input type="submit" name="" value="Submit">
                                      </span>
                                  </span>
                              </div>
                          </div>
                      </div>
          <?php endif; ?>

                    </div>
                  </form>
                  </div>
                </div>
              </div>

<?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--2fa enable disable popup-->
<div id="qrModal" class="modal modal-styled fade in modals-body ">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
         <h4 class="text-center w-100">Disable Google 2FA</h4>
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
        </button>
      </div>
      <form id="gauth-formnew" inspfaactive="true" action="<?php echo e(url('2fa/save')); ?>" method="post" class="">
        <div class="modal-body theme-form">
          <div id="alert-msg-enable"></div>
          <input type="hidden" id="token" name="_token" value="<?php echo e(csrf_token()); ?>">
          <div>
            <p class="text-center mb-0"><strong>Scan the QR code:</strong></p>
            <div class="qrcode text-center" >

            </div>
            <p class="text-center"><strong>or enter this code manually:</strong></p>
            <h3 class="text-center secret"></h3>
            <input type="hidden" name="secret_key" id="secret">
          </div>

                        <!-- <div id="google_auth_msg_enable"></div>
                        <input type="hidden" id="token_dis" name="_token" value="<?php echo e(csrf_token()); ?>">

                        <div class="row form-group" id="match-otp-2fa">
                            <lable>Enter OTP </lable>
                            <input type="number" name="google_2fa_otp" id="google_2fa_otp" class="form-control" onkeyup="checkOTP()" placeholder="Enter OTP that you get in your mobile" row="100" />
                          </div> -->

                          <div class="row margin12" id="match-otp-2fa_enable">

                            <div class="col-md-12">
                              <div class="form-group token_error text-center">
                                <label for="" style="color:  black;">Authenticator Code</label>
                                <input type="number" class="form-control aucode google_2fa" placeholder="input your 6-digit Authenticator code " id="google_2fa_otp_enable" name="totp" onkeyup="checkOTPEnable()">
                              </div>
                            </div>
<!--
                            <div class="col-md-4">
                                  <span class="labeltext"> Authenticator Code</span>
                            </div>
                            <div class="col-md-7">
                               <div class="" >

                                    <input type="number" class="form-control aucode" placeholder="input your 6-digit Authenticator code " id="google_2fa_otp_enable" name="totp" onkeyup="checkOTPEnable()">

                                </div>
                              </div> -->
                            </div>

                            <div class="text-center">
                              <code>If enabled then each time you login or make a withdrawal at Decentralend you will be prompted to complete the 2-Step Verification which can only be finished by having access to your smartphone.</code>
                            </div>


                          </div>
                          <div class="modal-footer text-center">
                            <button type="button" class="btn btn-theme mt-4" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-theme mt-4" id="enable-2fa" >Yes Enable Google 2FA Authenticator</button>
                          </div>
                        </form>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div>

                  <div id="qrmatch" class="modal modal-styled fade in modals-body">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h4 class="text-center w-100">Disable Google 2FA</h4>
                          <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                        </div>
                        <form id="gauth-form" class=" theme-form" inspfaactive="true" action="<?php echo e(url('2fa/disable')); ?>" method="get">
                          <div class="modal-body">
                            <div id="google_auth_msg"></div>
                            <input type="hidden" id="token_dis" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="row" id="match-otp-2fa">
                              <div class="col-12">
                              <div class="form-group text-center">
                                <label >Enter OTP </label>
                                <input type="number" name="google_2fa_otp" id="google_2fa_otp" class="form-control google_2fa" onkeyup="checkOTP()" placeholder="Enter OTP that you get in your mobile" row="100" />
                              </div>
                            </div>


                            </div>
                            <div class="col-12">
                              <div class="form-group text-center">
                                 <button type="submit" class="btn btn-theme mt-4" id="disable-2fa" >Yes Disable Google 2FA Authenticator</button>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer text-center">
                            <button type="button" class="btn button-close btn-theme" data-dismiss="modal">Close</button>
                          </div>
                        </form>
                      </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                  </div>
                  <!--popup end-->
                  <?php $__env->stopSection(); ?>

                  <?php $__env->startSection('script'); ?>

                    <script type="application/javascript" >
                      $('#otpToggleButton').on('click', function () {

                        "<?php echo e(session()->put('2fa:user:id', Sentinel::getUser()->id)); ?>"

                        $("#enable-2fa").prop('disabled', true);
                        $("#match-otp-2fa_enable").show();
                        $.ajax({
                          type: 'get',
                          url:"<?php echo e(url('2fa/enable')); ?>",
                          success: function (responseData) {
                            console.log(responseData);
                            $('.qrcode').empty();
                            $('.secret').empty();
                            //$('.qrcode').append('<img src="'https://chart.googleapis.com/', 'chart', 'chs='.$size.'x'.$size.'&chld=M|0&cht=qr&chl="+responseData['imgurl']+'">');
                            $(".qrcode").append('<img src="https://chart.googleapis.com/chart?chs=200x200&&chld=M|0&cht=qr&chl='+responseData['imgurl']+'">');
                            $('.secret').text(responseData['secret']);
                            $('#secret').val(responseData['secret']);
                          },
                          error: function (responseData) {
                            console.log(responseData);
                            return false;
                          }
                        });
                        $('#qrModal').modal('show');
                      });
                      $('#otpDisableToggleButton').on('click', function () {
                        <?php Session::forget('2fa:user:id');?>
                                "<?php echo e(session()->put('2fa:user:id', Sentinel::getUser()->id)); ?>"
                        $("#disable-2fa").prop('disabled', true);
                        $("#google_auth_msg").html('');
                        $("#match-otp-2fa").show();
                        $('#qrmatch').modal('show');
                      });

                      $('#submitGauth').on('click', function () {
                        var code = $('#otpCode').val();
                        var token = $('#token').val();
                        $.ajax({
                          type: 'post',
                          url:"<?php echo e(url('2fa/validate')); ?>",
                          data: "code="+code+'&_token='+token,
                          success: function (responseData) {
                            console.log(responseData);
                          },
                          error: function (responseData) {
                            console.log(responseData);
                            return false;
                          }
                        });
                        $('#qrModal').modal('show');
                      })

                      function checkOTP()
                      {
                        $("#google_auth_msg").html('');
                        var token = $('#token_dis').val();
                        var code= $("#google_2fa_otp").val();
                        if(code.length == 6)
                        {
                          $.ajax({
                            type: 'post',
                            url:"<?php echo e(url('2fa/validate-disabletime')); ?>",
                            data: "totp="+code+'&_token='+token,
                            success: function (responseData) {
                              if(responseData==1)
                              {
                                $("#match-otp-2fa").hide();
                                $("#google_auth_msg").html("<div class='alert alert-success'>OTP match successfully</div>");
                                $("#google_2fa_otp").val('');
                                $("#disable-2fa").prop('disabled', false);
                              }
                            },
                            error: function (responseData) {
                              $("#google_auth_msg").html("<div class='alert alert-danger'>Nice try but OTP not match, Please try again.</div>");
                              console.log(responseData);
                              return false;
                            }
                          });
                        }
                      }
                      function checkOTPEnable()
                      {
                        $("#alert-msg-enable").html('');

                        var code= $("#google_2fa_otp_enable").val();
                        var token = "<?php echo e(csrf_token()); ?>";

                        if(code.length == 6)
                        {

                          $.ajax({
                            type: 'post',
                            url:"<?php echo e(url('2fa/validate-enabletime')); ?>",
                            data: "totp="+code+"&_token="+token,
                            success: function (responseData) {
                              console.log(responseData);
                              if(responseData==1)
                              {
                                $("#match-otp-2fa_enable").hide();
                                $("#alert-msg-enable").html("<div class='alert alert-success'>OTP match successfully</div>");
                                $("#google_2fa_otp_enable").val('');
                                $("#enable-2fa").prop('disabled', false);
                              }
                            },
                            error: function (responseData) {
                              $("#alert-msg-enable").html("<div class='alert alert-danger'>Nice try but OTP not match, Please try again.</div>");
                              console.log(responseData);
                              return false;
                            }
                          });
                        }
                        else {
                            $("#alert-msg-enable").html("<div class='alert alert-danger'>Enter 6 digit only, Please try again.</div>");
                            console.log(responseData);
                            return false;

                        }
                      }
                    </script>
      <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>