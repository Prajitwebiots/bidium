<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="BIDIUM coin">
  <meta name="keywords" content="BIDIUM coin">
  <meta name="author" content="BIDIUM coin">
  <link rel="icon" href="<?php echo e(asset('assets/images/favicon.png')); ?>" type="image/x-icon"/>
  <link rel="shortcut icon" href="<?php echo e(asset('assets/images/favicon.png')); ?>" type="image/x-icon"/>
  <title><?php echo $__env->yieldContent('title'); ?></title>
    <?php echo $__env->make('layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </head>
  <body>
    <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="dashboard-wrapper" style="margin-top: 50px !important;">
      <?php echo $__env->make('layouts.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldContent('content'); ?>
    </div>
    <?php echo $__env->make('layouts.footerScript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </body>
  
</html>

