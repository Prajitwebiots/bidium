<!-- Tap on Top -->

<!-- latest jquery-->
<script src="<?php echo e(asset('assets/js/jquery-3.2.1.min.js')); ?>" type="text/javascript"></script>
<!--Jarallax JS-->
<script src="<?php echo e(asset('assets/js/jarallax.min.js')); ?>"></script>
<!--OWL Carousel JS -->
<script src="<?php echo e(asset('assets/js/owl.carousel.js')); ?>"></script>
<!--Scroll Reveal JS-->
<script src="<?php echo e(asset('assets/js/scrollreveal.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/modernizr-custom.js')); ?>"></script>
<!-- popper js-->
<script src="<?php echo e(asset('assets/js/popper.min.js')); ?>" type="text/javascript"></script>
<!-- Bootstrap js-->
<script src="<?php echo e(asset('assets/js/bootstrap.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/js/jquery.dataTables.min.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(URL::asset('assets/js/sweetalert.min.js')); ?>" type="text/javascript"></script>
<?php echo $__env->yieldContent('bottom_script'); ?>
<!-- Counter js-->
<script src="<?php echo e(asset('assets/js/jquery.waypoints.min.js')); ?>" type="text/javascript"></script>
<!-- <script src="<?php echo e(asset('assets/js/jquery.counterup.js')); ?>" type="text/javascript"></script> -->
<!-- Theme js-->
<script src="<?php echo e(asset('assets/js/script.js')); ?>" type="text/javascript"></script>
<script src="<?php echo e(asset('assets/js/chart.js')); ?>" type="text/javascript"></script>


<!-- Use for Datpicker with time in laravel..  -->
<script src="<?php echo e(url('assets/js')); ?>/moment-with-locales.min.js"></script>
<script src="<?php echo e(url('assets/js')); ?>/tempusdominus-bootstrap-4.js"></script>
<!-- end script  -->


<?php echo $__env->yieldContent('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {

        // Copy Link js starts
        (function() {
            // click events
            document.body.addEventListener('click', copy, true);
            // event handler
            function copy(e) {
                // find target element
                var
                    t = e.target,
                    c = t.dataset.copytarget,
                    inp = (c ? document.querySelector(c) : null);
                // is element selectable?
                if (inp) {
                    // select text
                    inp.select();
                    try {
                        // copy text
                        document.execCommand('copy');
                        inp.blur();

                        // copied animation
                        t.classList.add('copied');
                        setTimeout(function() { t.classList.remove('copied'); }, 1500);
                    } catch (err) {
                        alert('please press Ctrl/Cmd+C to copy');
                    }
                }
            }
        })();
        // Copy Link js ends


        //menu left toggle

        $('.toggle-nav').click(function() {
            // alert('done');
            $this = $(this);
            $nav = $('.nice-nav');
            //$nav.fadeToggle("fast", function() {
            //  $nav.slideLeft('250');
            //  });

            $nav.toggleClass('open');

        });

         $(document).ready(function(){

            var widthw = $( window ).width();

            if(widthw < 768){
                $('.nice-nav').addClass("open");  
            }

         });

        $('.body-part').click(function() {
            $nav.addClass('open');
        });
        //  $nav.addClass('open');

        //drop down menu
        $submenu = $('.child-menu-ul');
        $('.child-menu .toggle-right').on('click', function(e) {

            $(".toggle-right").removeClass("rotate");


            e.preventDefault();
            $this = $(this);
            $parent = $this.parent().next();
            // $parent.addClass('active');
            $tar = $('.child-menu-ul');
            if (!$parent.hasClass('active')) {
                $tar.removeClass('active').slideUp('fast');
                $parent.addClass('active').slideDown('fast');
                $(this).addClass("rotate");


            } else {
                $parent.removeClass('active').slideUp('fast');
                $(".toggle-right").removeClass("rotate");
            }

        });

    });

   /* $(document).ready(function() {
        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });
    });*/


$(window).scroll(function(){
    if($(window).scrollTop() > 100 )
    {
        $(".header-bottom").addClass("bg-white");
    }
    else{
        $(".header-bottom").removeClass("bg-white");
    }
});
</script>
<script type="text/javascript">
    $('#homenavbar li').click(function() {
    $('#homenavbar li').removeClass("active");
    $(this).addClass("active");

});
</script>
