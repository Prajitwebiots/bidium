'<?php
// check admin or user
$slug = Sentinel::getUser()->roles()->first()->slug;
?>
<div class="nice-nav" style="top: 64px !important;">
   <div class="user-info">
       <div class="media">
           <img class="mr-3 align-self-center" src="<?php echo e(url('/assets/images/user')); ?>/<?php echo e(Sentinel::getUser()->profile); ?>" alt="Generic placeholder image">
           <div class="media-body">
                  <h5><?php echo e(Sentinel::getUser()->first_name); ?></h5>
                 <h6><?php echo e(Sentinel::getUser()->last_name); ?></h6>
           </div>
       </div>
   </div>
  <?php if($slug == 1): ?>
   <ul>
       <li>
           <a href="<?php echo e(url('admin/dashboard')); ?>" class="<?php echo e((Request::is('admin/dashboard') ? 'active ' : '')); ?>">Dashboard</a>
       </li>
        <li>
           <a href="<?php echo e(url('admin/profile')); ?>" class="<?php echo e((Request::is('admin/profile') ? 'active ' : '')); ?>">Profile</a>
       </li>
       <li class="child-menu">
               <a href="#"><span>Users</span> <span class="fa fa-angle-right toggle-right"></span>
               </a>
               <ul class="child-menu-ul">
                  <li>
                    <a href="<?php echo e(url('admin/users')); ?>" >Manage Users</a>
                  </li>
                  <li>
                    <a href="<?php echo e(url('admin/invRequest')); ?>" >Investment Request  <?php if($header_data['inv'] != 0): ?> <span class="badge badge-warning" style="float:right;font-size: 18px;border-radius: 16px;margin: -6px 0px 0px 0px;color:  #fff;padding-top: 3px;width:  28px;"><?php echo e($header_data['inv']); ?>

               </span> <?php endif; ?></a>
                  </li>
                  <li>
                      <a href="<?php echo e(url('admin/kyclist')); ?>">User KYC List</a>
                  </li>
                  
               </ul>
       </li>


       <li class="child-menu">
               <a href="#"><span>Users Transaction</span> <span class="fa fa-angle-right toggle-right"></span>
               </a>
               <ul class="child-menu-ul">
                  <li>
                     <a href="<?php echo e(url('admin-buytokentransaction')); ?>">Buy Token  </a>
                  </li>
                  <li>
                     <a href="<?php echo e(url('admin-deposittransaction')); ?>">Deposit</a>
                  </li>
                  <li>
                     <a href="<?php echo e(url('admin-withdrawaltransaction')); ?>">Withdrawal</a>
                  </li>
       <!--            <li>
                     <a href="<?php echo e(url('admin-tokentransaction')); ?>">Transfer Token </a>
                  </li> -->
               </ul>
       </li>
              <li>
               <a href="<?php echo e(url('admin-withdrawal')); ?>" class="<?php echo e((Request::is('admin-withdrawal') ? 'active ' : '')); ?>">Withdrawal Request 
                <?php if($header_data['count'] != 0): ?> <span class="badge badge-warning" style="float:right;font-size: 18px;border-radius: 16px;margin: -6px 0px 0px 0px;color:  #fff;padding-top: 3px;width:  28px;"><?php echo e($header_data['count']); ?>

               </span> <?php endif; ?>
               </a>
       </li>
       <li>
         <a href="<?php echo e(url('admin-setting')); ?>" class="<?php echo e((Request::is('admin-setting') ? 'active ' : '')); ?>">ICO Settings</a>
       </li>
       <li>
         <a href="<?php echo e(url('admin-rates')); ?>" class="<?php echo e((Request::is('admin-rates') ? 'active ' : '')); ?>">Phase Settings</a>
       </li>


   </ul>
  <?php else: ?>
   <ul>
       <li>
           <a href="<?php echo e(url('user/dashboard')); ?>" class="<?php echo e((Request::is('user/dashboard') ? 'active ' : '')); ?>">Dashboard</a>
       </li>
       <li>
           <a href="<?php echo e(url('user/profile')); ?>" class="<?php echo e((Request::is('user/profile') ? 'active ' : '')); ?>">Profile</a>
       </li>

       <?php if(config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5): ?>
       <li class="child-menu">
          <a href="<?php echo e(url('user-walletInfo')); ?>" class="<?php echo e((Request::is('user-walletInfo') ? 'active ' : '')); ?>"><span>Deposit</span>
          </a>
       </li>
       <?php endif; ?>

       <?php if(config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5): ?>
       <li class="child-menu">
          <a href="<?php echo e(url('user-ico')); ?>" class="<?php echo e((Request::is('user-ico') ? 'active ' : '')); ?>"><span>Buy Token</span>
              <!--<span class="fa fa-angle-right toggle-right"></span>-->
          </a>
          <!--<ul class="child-menu-ul">

            <li>
              <a href="<?php echo e(url('user-ico')); ?>">Buy Token</a>
            </li>
                 <li>
              <a href="<?php echo e(url('ico-info')); ?>" class="">ICO information</a>
            </li>
          </ul>-->
       </li>
       <?php endif; ?>

       <?php if(config('common.timerConfigurations.current.groupId') == 2 || config('common.timerConfigurations.current.groupId') == 5 || config('common.timerConfigurations.current.groupId') == 0): ?>
       <li class="child-menu">
           <a href="#"><span>Transaction</span> <span class="fa fa-angle-right toggle-right"></span>
           </a>
           <ul class="child-menu-ul">
              <li>
                 <a href="<?php echo e(url('buytokentransaction')); ?>">Buy Token History </a>
              </li>
              <li>
                 <a href="<?php echo e(url('deposittransaction')); ?>">Deposit History</a>
              </li>
              <!--<li>
                 <a href="<?php echo e(url('withdrawaltransaction')); ?>">Withdrawal</a>
              </li>-->
             <!--  <li>
                 <a href="<?php echo e(url('tokentransaction')); ?>">Transfer Token </a>
              </li> -->

           </ul>
       </li>
       <?php endif; ?>

        <li>
           <a href="<?php echo e(url('user-referral')); ?>" class="<?php echo e((Request::is('user-referral') ? 'active ' : '')); ?>">Referral List</a>
        </li>
        <li class="d-md-none d-lg-block">
            <a href="<?php echo e(url('logout')); ?>">Signout</a>
        </li>
   </ul>
  <?php endif; ?>
</div>