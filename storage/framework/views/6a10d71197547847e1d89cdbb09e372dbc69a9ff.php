<!-- Header Start-->
<div class="header">
<!--<div class="header-top">
   <div class="container">
      <div class="row">
         <div class="col-lg-8">
            <ul class="header-contact">
               <li><i class="fa fa-clock-o"></i>Mon - Fri: 9AM - 7PM</li>
               <li><i class="fa fa-address-book"></i>123 XYZ United Kingdom, UK 12352</li>
            </ul>
         </div>
         <div class="col-lg-4 text-right">
            <div class="header-social">
               <a href="#"><i class="fa fa-facebook"></i></a>
               <a href="#"><i class="fa fa-twitter"></i></a>
               <a href="#"><i class="fa fa-google-plus"></i></a>
               <a href="#"><i class="fa fa-instagram"></i></a>
            </div>
         </div>
      </div>
   </div>
</div>-->
<div class="header-bottom fixed-top">
   <div class="container br-dark">
      <div class="row">
         <div class="col-sm-12">
            <nav class="navbar navbar-expand-lg navbar-light ">
               <a class="navbar-brand" href="#"><img src="assets/images/logo.png" /></a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav ml-auto" id="homenavbar">
                     <li class=" nav-item active">
                        <a class="nav-link" href="#">Home</a>
                     </li>
                     <li class=" nav-item">
                        <a class="nav-link" href="#about-us">About us</a>
                     </li>
                     <li class=" nav-item">
                        <a class="nav-link" href="#ICO">ICO</a>
                     </li>
                     <li class=" nav-item">
                        <a class="nav-link" href="#team">Team</a>
                     </li>
                     <li class=" nav-item">
                        <a class="nav-link" href="#road-map">Roadmap</a>
                     </li>
                     <li class=" nav-item">
                        <a class="nav-link" href="#documents">Whitepaper</a>
                     </li>
                  </ul>
                   <?php if(Sentinel::check()): ?>
                           <?php if(Sentinel::getUser()->roles()->first()->slug == 1): ?>
                                 <a  href="<?php echo e(url('admin/dashboard')); ?>" class="btn btn-join my-2 my-sm-0" >My Account</a>
                           <?php else: ?>
                                 <a  href="<?php echo e(url('user/dashboard')); ?>" class="btn btn-join my-2 my-sm-0" >My Account</a>
                           <?php endif; ?>
                           <?php else: ?>
                        <a  href="<?php echo e(url('/login')); ?>" class="btn btn-join my-2 my-sm-0" >Login</a>
                        <?php endif; ?>
               </div>
            </nav>
         </div>
      </div>
   </div>
</div>
<!-- Header End-->
