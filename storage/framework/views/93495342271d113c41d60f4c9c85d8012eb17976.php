<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Advance combination of auction platform , freelancing platform and crypto-fiat-crypto trading/exchange platform.">
    <meta name="keywords" content="Exchange platform, Bidium, freelancing Platform, Best ICO ">
    <meta name="author" content="Bidium ">
    <link rel="icon" href="<?php echo e(url('assets/images/favicon.png')); ?>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo e(url('assets/images/favicon.png')); ?>" type="image/x-icon"/>
    <title><?php echo $__env->yieldContent('title'); ?></title>
    <?php echo $__env->make('layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if(config('common.adCodes.analytics.google')): ?>
      <?php echo $__env->make('layouts.ad-codes.google-analytics', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
  </head>
  <body>
    <!-- Loader Start-->
     
    <div class="loader-wrapper">
      <div class="loader">
        
      </div>
  </div> 
    <!-- Loader Ends-->
    <?php echo $__env->make('layouts.headerHome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('layouts.footerScript', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.footerHome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldPushContent('footer-scripts'); ?>
  </body>
</html>
<script type="text/javascript">
  $(window).on('load', function() {
      $('.loader-wrapper').fadeOut('slow');
      $('.loader-wrapper').remove('slow');
    });

</script>