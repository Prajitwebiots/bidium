<!-- dashboard start -->
 <div class="dashboard-header">
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-4">
                 <a class="navbar-brand-2" href="/"><img src="<?php echo e(asset('assets/images/logo-white.png')); ?>" /></a>
                 <div class="dashboard-sidebar">
                     <a href="#" class="toggle-nav">|||</a>
                 </div>
             </div>
             <div class="col-md-8 dashboard-curruncy d-none d-md-block d-lg-block">
                 <ul class="rating-info">
                     <li><img src="<?php echo e(asset('assets/images/coin/btc.png')); ?>" alt="BTC">1 BTC = $ <span class="btcusd"><?php echo e($header_data['BTC']); ?></span></li>
                     <li><img src="<?php echo e(asset('assets/images/coin/eth.png')); ?>" alt="ETH">1 ETH = $ <span class="ethusd"><?php echo e($header_data['ETH']); ?></span></li>
                     <li><img src="<?php echo e(asset('assets/images/coin/bidium.png')); ?>" />1 BIDIUM = $ <?php echo e($header_data['rate']); ?></li>
                 </ul>
                 <a href="<?php echo e(url('logout')); ?>" class="btn btn-logout my-2 my-sm-0 ml-3">Signout</a>
             </div>
         </div>
     </div>
 </div>

